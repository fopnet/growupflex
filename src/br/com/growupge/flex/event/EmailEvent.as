 package br.com.growupge.flex.event
{
	import br.com.growupge.flex.valueobject.ComandoDTO;
	import br.com.growupge.flex.valueobject.EmailDTO;
	
	public class EmailEvent extends BaseEvent {

		public var email:EmailDTO;
		
		public function EmailEvent(email:EmailDTO, type:String, bubbles:Boolean=true, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
			this.email = email;			
		}
		
		/* Envio de emails */
		public static const EMAIL_ENVIANDO:String = "enviandoEmail";
		public static const EMAIL_ENVIADO:String = "enviadoEmail";
		public static const EMAIL_ERROR:String = "envioEmailError";
	}	
}
 package br.com.growupge.flex.event
{
	import br.com.growupge.flex.constantes.GCS;
	import br.com.growupge.flex.valueobject.ComandoDTO;
	
	public class ConectarEvent extends BaseEvent {

		public function ConectarEvent ( cmd : ComandoDTO , type:String, bubbles:Boolean=true, cancelable:Boolean=false) {
			super( type, bubbles , cancelable );
			this.comando = cmd;
		}

		/* Conexão */		
		public static const CONECTAR : String = "conectar";
		public static const DESCONECTAR : String = "desconectar";
		
		/* User Manager */		
    	public static const PROCURAR_SESSION:String = GCS.PROCURAR_SESSION;
    	public static const TROCAR_SENHA:String = GCS.TROCAR_SENHA;
    	public static const TROCAR_SENHA_SUCESSO:String = "TrocarSenhaSucessoEvent";		
		
	}	
}
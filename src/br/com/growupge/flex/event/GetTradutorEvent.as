 package br.com.growupge.flex.event
{
	import com.adobe.cairngorm.control.CairngormEvent;
	import br.com.growupge.flex.controller.TradutorControl;
	
	public class GetTradutorEvent extends CairngormEvent {
		public var sigla : String;
		public function GetTradutorEvent (sigla : String) {
			super( TradutorControl.EVENT_GETTRADUTOR );
			this.sigla = sigla;
		}
		
	}	
}
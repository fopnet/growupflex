 package br.com.growupge.flex.event
{
    import br.com.growupge.flex.constantes.GCS;
    import br.com.growupge.flex.event.BaseEvent;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    
	public class ExecutarComandoEvent extends BaseEvent {
		
		public function ExecutarComandoEvent ( comando : ComandoDTO, permissao:String = null ) {
			permissao = (permissao == null)? comando.permissao : permissao;
			super( permissao );
			this.comando = comando;
			this.comando.SID = model.sessaoDTO.sid;
		}
		
		/*CRUD */
    	public static const COMANDO_GENERICO:String = 'GENERIC_EVENT_COMMAND';
    	public static const EXPORTAR_FATURA:String = GCS.EXPORTAR_FATURA;
    	public static const PROCURAR:String = GCS.PROCURAR;
    	public static const BUSCAR_TODAS:String = GCS.BUSCAR_TODAS;
    	public static const BUSCAR_TOTAL:String = GCS.BUSCAR_TOTAL;
		public static const EVENT_ADICIONAR : String = "CRUD_ADICIONAR";
		public static const EVENT_ATUALIZAR : String = "CRUD_ATUALIZAR";
		public static const EVENT_EXCLUIR : String = "CRUD_EXCLUIR";    	
		public static const EVENT_COPIAR : String = "CRUD_COPIAR";    	

		/*Pedido*/
    	public static const PROCURAR_PEDIDO:String = GCS.PROCURAR_PEDIDO;
    	public static const NOVO_PEDIDO:String = GCS.CRIAR_NIP;
		
		/*CRUD Grupo Relacionamento*/
    	public static const BUSCAR_LISTA_ORIGEM:String = 'BuscarListaOrigemEvent';
    	public static const BUSCAR_LISTA_DESTINO:String = 'BuscarListaDestinoEvent';
    	public static const ADICIONAR_RELACIONAMENTO : String = "CRUD_ADICIONAR_RELACIONAMENTO";
		public static const ATUALIZAR_RELACIONAMENTO : String = "CRUD_ATUALIZAR_RELACIONAMENTO";
		public static const EXCLUIR_RELACIONAMENTO : String = "CRUD_EXCLUIR_RELACIONAMENTO";
		
		/* User Manager */
    	public static const CRIAR_USUARIO:String = GCS.ADICIONAR_USUARIO;
    	public static const USUARIO_CRIADO_SUCESSO:String = "UsuarioCriadoSucessoEvent"
    	public static const RECRIAR_SENHA:String = GCS.RECRIAR_SENHA;
    	public static const RECRIAR_SENHA_SUCESSO:String = "ReCriarSenhaSucessoEvent";
		
	}	
}
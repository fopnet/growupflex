package br.com.growupge.flex.event
{
    import br.com.growupge.flex.model.SigemModelLocator;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    
    import com.adobe.cairngorm.control.CairngormEvent;
    
    import flash.events.EventDispatcher;
    
    public class BaseEvent extends CairngormEvent {
        public var model : SigemModelLocator = SigemModelLocator.getInstance();

        public var comando : ComandoDTO;
        public var resultadoDisplayObject:EventDispatcher;
        public var indice:Number;
        public var eventTargetResult:BaseEvent;
        public var eventTargetFault:BaseEvent;
        public var result:Object;
        
        public function BaseEvent (type:String, bubbles:Boolean=true, cancelable:Boolean=false) {
			super( type, bubbles, cancelable );
		}
    }
}


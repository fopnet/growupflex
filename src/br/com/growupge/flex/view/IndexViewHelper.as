package br.com.growupge.flex.view
{
    import br.com.growupge.flex.model.Linguagem;
    import br.com.growupge.flex.model.SigemModelLocator;
    import br.com.growupge.flex.model.TradutorHelper;
    import br.com.growupge.flex.valueobject.SessaoDTO;
    import br.com.growupge.flex.valueobject.UsuarioDTO;
    
    import flash.net.SharedObject;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;
    import flash.utils.setTimeout;
    
    import mx.containers.TitleWindow;
    import mx.controls.Label;
    import mx.core.Application;
    import mx.managers.PopUpManager;
    
    public class IndexViewHelper {
        private var model:SigemModelLocator = SigemModelLocator.getInstance();
        private var helper : TradutorHelper = TradutorHelper.getInstance();
        
        public function showSigemApp(usr:UsuarioDTO):void {
            model.appState = usr.perfil.codigo == 'U9' ? SigemModelLocator.VAREJO_VIEW : SigemModelLocator.SIGEM_APP_VIEW;
            model.usuarioState = "usuarioConectado";
    
        }

        public function closeSigemApp(sessao:SessaoDTO):void{
            //model.appState = SigemModelLocator.CONECTAR_VIEW;
            model.usuarioState = "";
            model.sessaoDTO = new SessaoDTO();
			model = null;			
			
			var window:TitleWindow = new TitleWindow();
 			var msg:Label = new Label();
			msg.text =  "Desconectando..."; //sessao.mensagemRetorno;
			msg.styleName = "textColor";
			window.addChild(msg);
			window.title = "Aguarde...";
    		window.styleName = "window";
    		window.minWidth = 300;
    		
			PopUpManager.addPopUp(window, Application.application.getChildByName('appView'), true);
            PopUpManager.centerPopUp(window);
			setTimeout(reloadApp, 300, "index.html");
		
            /* recarrega application Sigem */
/*             try {
                var module:ModuleLoaderWithProgress = mx.core.Application.application.sigemApp.sigemAdminModuleApp;
                //mx.core.Application.application.sigemApp.tabApp.selectedIndex = 0;
                //mx.core.Application.application.portalView.conectarView.tabNavigatorConectar.selectedIndex = 0;
                module.unloadModule();
                //module.loadModule();
            } catch (e:Error){} */
        }
        
        public function aplicarConfigUsuario(usuario : UsuarioDTO) : void {
            if (usuario.idioma != helper.getLinguagem().nome){
                helper.setLinguagem(new Linguagem(usuario.idioma));
                helper.getTradutor(usuario.idioma);
            }
			
			if (model.isVeiculosSwf()) {
				//BrowserManager.getInstance().setFragment("panel=dashboard");
				model.addSidToSharedObject();
				
				reloadApp("../GrowUpFlex/index.html");
				return;
			}
			
			showSigemApp(usuario);
			
            model.max_registros_retornados_paginacao = usuario.qtdRegistros;
			
        }
		
		private function reloadApp(page:String):void {
			var u:URLRequest = new URLRequest(page);
			navigateToURL(u ,"_self");
		}  
        
    }
}
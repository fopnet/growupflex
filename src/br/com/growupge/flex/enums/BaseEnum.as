package br.com.growupge.flex.enums
{
	import com.adobe.cairngorm.vo.IValueObject;
	import br.com.growupge.flex.valueobject.Abstract;
	
	public class BaseEnum extends Abstract implements IValueObject 
	{
		
		public function BaseEnum(name:String=null)
		{
			super(getAccess());
			this._name = name;
		}
		
		public function equals(value:BaseEnum):Boolean
		{
			return value != null && value.name == this.name;
		}
		
		public function toString():String
		{
			return name.toString();
		}
		
		/* Name */
		
		private var _name:String;
		
		[Bindable]
		public function get name():String
		{
			return _name;
		} 
		
		public function set name(value:String):void
		{
			this._name = value;
		}
		
		/* Descricao */
		private var _descricao:String;
		
		/*[Transient]*/
		public function get descricao():String
		{
			return _descricao;
		} 
		
		public function set descricao(value:String):void
		{
			_descricao = value;
		}		
		
	}

}
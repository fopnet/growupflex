package br.com.growupge.flex.enums
{
	public final class Perfil {
    	public static const ADMINISTRADOR:String = "A";
    	public static const EMPRESA:String = "E";
    	public static const USUARIO_AVANCADO:String = "U0";
    	public static const COMUM:String = "U9";
	}
}
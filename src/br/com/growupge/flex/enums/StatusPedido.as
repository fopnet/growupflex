package br.com.growupge.flex.enums
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;
	
	[Bindable]
	[RemoteClass(alias="br.com.growupge.dto.StatusPedido")]
	public class StatusPedido  extends BaseEnum /* implements IValueObject  , IExternalizable*/
	{
		public static const PENDENTE:StatusPedido 	= new StatusPedido("PENDENTE",  "PD", "Pendente");
		public static const A_FATURAR:StatusPedido 	= new StatusPedido("A_FATURAR", "AF", "A faturar");
		public static const CONCLUIDO:StatusPedido 	= new StatusPedido("CONCLUIDO", "PG", "Concluído");
		public static const FATURADO:StatusPedido 	= new StatusPedido("FATURADO",  "FA", "Faturado");
		public static const ENVIADO:StatusPedido 	= new StatusPedido("ENVIADO",   "EV", "Enviado");
		public static const EXPIRADO:StatusPedido 	= new StatusPedido("EXPIRADO",  "EX", "Expirado");
		public static const CANCELADO:StatusPedido 	= new StatusPedido("CANCELADO", "CL", "Cancelado");
		
		private var _tipo:String;
		
		[ArrayElementType("br.com.growupge.dto.StatusPedido")]
		private static const tipos:Array = [PENDENTE, A_FATURAR, CONCLUIDO, FATURADO, ENVIADO, EXPIRADO, CANCELADO];
		
/*		private static const Self:EnumClass = enumOf(StatusPedido);
		public static const values:Array = Self.values;*/
		
		public function StatusPedido(name:String=null, tipo:String=null,desc:String= null)
		{
			super(name);
			this._tipo = tipo;
			super.descricao = desc;
		}
		
		public function get tipo():String {
			return _tipo;
		}
		
	    /*
		 * Somente para finalidades de serializacao 
		 */
		public function set tipo(value:String):void {
			_tipo = value;
		}
		
		public static function valueOf(value:String):StatusPedido
		{
			for each (var tipo:StatusPedido in tipos)
			{
				if (tipo.name == value)
					return tipo;
			}
			return null;
		} 
		
		public function isPendente():Boolean { 
			return (_tipo == A_FATURAR._tipo || _tipo == PENDENTE._tipo); 
		}
		
		public function readExternal(input:IDataInput):void { 
			name = input.readUTF(); // discard the name 
		}
		
		public function writeExternal(output:IDataOutput):void {
			output.writeUTF(name); 
		}
		
	}
}
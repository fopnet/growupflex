package br.com.growupge.flex.command
{
	import br.com.growupge.flex.componentes.MensagemErroSigem;
	import br.com.growupge.flex.delegate.SigemDelegate;
	import br.com.growupge.flex.event.ConectarEvent;
	import br.com.growupge.flex.model.SigemModelLocator;
	import br.com.growupge.flex.valueobject.SessaoDTO;
	import br.com.growupge.flex.view.IndexViewHelper;
	
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class ConectarCommand implements ICommand , IResponder {
		protected var model : SigemModelLocator = SigemModelLocator.getInstance();
	   
/* 	    public function ConectarCommand(): void {
	    	super(this.result , this.fault);
	    } */
		public function execute( event : CairngormEvent ) : void {
			// Aqui chama o Delegate (opcionalmente, qnd envolver complexidade ou chamadas remotas)
			// Caso a implementação se resuma ao Command, basta chamar : this.onResult(event);	      
			var delegate:SigemDelegate = new SigemDelegate(this);
			
			// AMF0 CursorManager.setBusyCursor();
			delegate.executarComando(ConectarEvent(event).comando);
		}
		   
		public function result( data : Object ) : void {
			 var event:ResultEvent = data as ResultEvent;
			 model.sessaoDTO = event.result as SessaoDTO;
			 
			// AMF0
			//CursorManager.removeBusyCursor();			
			//model.sessaoDTO = ConvertDTO.toSessaoDTO(data);
			 
			var app:IndexViewHelper = new IndexViewHelper();
			app.aplicarConfigUsuario(model.sessaoDTO.usuario);
		}
				
		public function fault( info : Object ) : void {
			var event:FaultEvent = info as FaultEvent;
			//CursorManager.removeBusyCursor();
			var mensagem : MensagemErroSigem = MensagemErroSigem.getInstance();
			mensagem.exibir(event.fault);
		}
	}
}
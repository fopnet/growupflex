package br.com.growupge.flex.command
{
	import mx.rpc.events.ResultEvent;
	
	import br.com.growupge.flex.valueobject.SessaoDTO;
	
	public class ProcurarSessionCommand extends ConectarCommand {

		override public function result(data:Object):void {
			var event:ResultEvent = data as ResultEvent;
			model.sessaoDTO = event.result as SessaoDTO;
			if ( model.sessaoDTO != null) 
				super.result(data);
			
		}
	}
}
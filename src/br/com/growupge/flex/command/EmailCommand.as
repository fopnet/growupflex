package br.com.growupge.flex.command
{
	import br.com.growupge.flex.constantes.ImageLibrary;
	import br.com.growupge.flex.event.BaseEvent;
	import com.adobe.cairngorm.control.CairngormEvent;
	import mx.controls.Alert;
	import mx.managers.CursorManager;
	import br.com.growupge.flex.model.TradutorHelper;
	import br.com.growupge.flex.valueobject.EmailDTO;
	import br.com.growupge.flex.delegate.EmailDelegate;
	import br.com.growupge.flex.event.EmailEvent;
	import mx.core.UIComponent;
	import mx.rpc.events.ResultEvent;
	
	
	public class EmailCommand extends BaseCommand {
	    
	   	public override function result( data : Object ) : void {
	   		var event:ResultEvent = data as ResultEvent;	
			super.result(data);							
			/* super.dispararEventoTargetResult(event);  Nao precisa pq já faz no super.result*/
		}
	                    
        public override function execute( event : CairngormEvent ) : void {
        	// CursorManager.setBusyCursor();
			var delegate:EmailDelegate = new EmailDelegate(this); 
    		delegate.enviarEmail( EmailEvent(event).email );
    		
    		this.evento = event;
 		}
		   		
	}
}
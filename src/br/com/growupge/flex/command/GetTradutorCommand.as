package br.com.growupge.flex.command
{
	import br.com.growupge.flex.componentes.MensagemErroSigem;
	import br.com.growupge.flex.delegate.TradutorDelegate;
	import br.com.growupge.flex.event.GetTradutorEvent;
	import br.com.growupge.flex.model.SigemModelLocator;
	import br.com.growupge.flex.model.TradutorHelper;
	
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class GetTradutorCommand implements ICommand , IResponder {
		private var model : SigemModelLocator = SigemModelLocator.getInstance();
        private var helper : TradutorHelper = TradutorHelper.getInstance();
	    
/* 	    public function GetTradutorCommand() :void {
	    	super(this.result , this.fault);
	    }     */
    
		public function execute( event : CairngormEvent ) : void {
			var delegate:TradutorDelegate = new TradutorDelegate(this);
			delegate.getTradutor(GetTradutorEvent(event).sigla);
		}
		   
		public function result( data : Object ) : void {	
		    var event:ResultEvent = data as ResultEvent;	
			helper.setTradutor( event.result as Object );
		}
				
		public function fault( info : Object ) : void {
		    var event:FaultEvent = info as FaultEvent;
		    var mensagem : MensagemErroSigem = MensagemErroSigem.getInstance();
			mensagem.exibir(event.fault);
		}
	}
}
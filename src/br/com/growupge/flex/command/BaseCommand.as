package br.com.growupge.flex.command
{
    import br.com.growupge.flex.componentes.MensagemErroSigem;
    import br.com.growupge.flex.delegate.SigemDelegate;
    import br.com.growupge.flex.event.BaseEvent;
    import br.com.growupge.flex.model.SigemModelLocator;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    import br.com.growupge.flex.valueobject.MensagemRetornoDTO;
    
    import com.adobe.cairngorm.commands.ICommand;
    import com.adobe.cairngorm.control.CairngormEvent;
    
    import flash.events.Event;
    
    import mx.rpc.IResponder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    public class BaseCommand implements ICommand, IResponder {
        public var evento:Event;
		
        [Deprecated('Variaveis não pertencem ao comando')]
        public function get indice():Number {
        	return BaseEvent(evento).indice;
        }
        [Deprecated('Variaveis não pertencem ao comando')]
        public function get comando():ComandoDTO {
        	return BaseEvent(evento).comando;
        }
        [Deprecated('Variaveis não pertencem ao comando')]
        public function get resultado():* {
        	return BaseEvent(evento).resultadoDisplayObject;
        }
        
/*     	public function BaseCommand(): void {
	    	super(this.result , this.fault);
	    } */
	                    
        public function execute( event : CairngormEvent ) : void {
        	//AMF0 CursorManager.setBusyCursor();
			var delegate:SigemDelegate = new SigemDelegate(this); 
    		delegate.executarComando(BaseEvent(event).comando);

    		this.evento = event;
 		}
		   
		public function result( data : Object ) : void {
			var event:ResultEvent = data as ResultEvent;			
			//AMF0 CursorManager.removeBusyCursor();
			this.dispararEventoTargetResult(event);
		}
				
		public function fault( info : Object ) : void {
			var event:FaultEvent = info as FaultEvent;
			//AMF0 CursorManager.removeBusyCursor();
			var mensagem : MensagemErroSigem = MensagemErroSigem.getInstance();
			mensagem.exibir(event.fault);

			this.dispararEventoTargetFault(event);
		}
		
		public function dispararEventoTargetFault(result:FaultEvent):void {
			var faultEvent:BaseEvent = BaseEvent(this.evento).eventTargetFault;
		    if (faultEvent != null) {
		    	faultEvent.result = result.fault;
		    	faultEvent.data   = BaseEvent(evento).data;
		    	BaseEvent(this.evento).resultadoDisplayObject.dispatchEvent( faultEvent );
		    }
		}
		public function dispararEventoTargetResult(result:ResultEvent):void {
			var targetEvent:BaseEvent = BaseEvent(this.evento).eventTargetResult;	    
		    if (targetEvent != null) {
		    	targetEvent.result = result.result;
		    	targetEvent.data   = BaseEvent(evento).data;
		    	BaseEvent(this.evento).resultadoDisplayObject.dispatchEvent( targetEvent );
		    } else {
				var dto:MensagemRetornoDTO = result.result as MensagemRetornoDTO; 
				if (dto != null)
					SigemModelLocator.getInstance().mensagemRetorno = dto.mensagemRetorno;
			}
		}
        
    }
}
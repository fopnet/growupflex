package br.com.growupge.flex.command
{
	import br.com.growupge.flex.valueobject.SessaoDTO;
	import br.com.growupge.flex.valueobject.ComandoDTO;
	
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;

	import br.com.growupge.flex.delegate.SigemDelegate;
	import br.com.growupge.flex.view.IndexViewHelper;
	import br.com.growupge.flex.model.SigemModelLocator;
	import br.com.growupge.flex.componentes.MensagemErroSigem;
	
	import mx.controls.Alert;
	import mx.managers.CursorManager;
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import br.com.growupge.flex.event.ConectarEvent;
	
	public class DesconectarCommand implements ICommand , IResponder  {
		private var model : SigemModelLocator = SigemModelLocator.getInstance();
	   
/* 	   public function DesconectarCommand(): void {
	    	super(this.result , this.fault);
	    } */
	    
		public function execute( event : CairngormEvent ) : void {
			var delegate:SigemDelegate = new SigemDelegate(this); 
			CursorManager.setBusyCursor();			
			delegate.executarComando(ConectarEvent(event).comando);
		}
		   
		public function result( data : Object ) : void {			
			CursorManager.removeBusyCursor();
			var event:ResultEvent = data as ResultEvent;
			var app:IndexViewHelper = new IndexViewHelper();
			
			//AMF0 app.closeSigemApp(ConvertDTO.toSessaoDTO(data));
			app.closeSigemApp(event.result as SessaoDTO);
		}
				
		public function fault( info : Object ) : void {
			CursorManager.removeBusyCursor();
			var event:FaultEvent = info as FaultEvent;
			var mensagem : MensagemErroSigem = MensagemErroSigem.getInstance();
			mensagem.exibir(event.fault);
		}
	}
}
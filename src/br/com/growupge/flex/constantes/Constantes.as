package br.com.growupge.flex.constantes
{
    import br.com.growupge.flex.valueobject.CombustivelDTO;
    
    import mx.collections.ArrayCollection;
    
    public class Constantes
    {
    	static public var GATEWAY:String;
    	static public const FACADE_COMMAND:String 	= "br.com.growupge.facade.GrowUpFacade.executarComando";
    	static public const FACADE_EMAIL_COMMAND:String 	= "br.com.growupge.facade.GrowUpFacade.enviarEmail";
    	static public const TRADUTOR_COMMAND:String = "br.com.growupge.tradutores.Tradutor.getInstance";
    	//static public const ENDERECO:String 		= "Rua Belfort Roxo, nº 40 sala 201 – Copacabana Tel.: (21)2541-3441";
    	static public const ENDERECO:String 		=  "www.growupge.com.br";// "Rua Fernandes Guimarães, 80 casa III – Botafogo Tel.: (21)9967-9697";
		public static var NOME_EMPRESA:String = "Grow Up Gestão Empresarial";
		
        static public const LISTA_FORMA_AUTENTICACAO: Array = ['SIST','LDAP'];
        
        static public const INICIALMENTE_COM:String = 'inicialmente';
        static public const EM_QUALQUER_LUGAR:String = 'qualquer';
        static public const EXATAMENTE_IGUAL:String = 'exatamente';
        static public const EXPRESSAO_REGULAR:String = 'expressaoRegular';

        static public const SIM:String = 'S';
        static public const NAO:String = 'N';

		public static function getLISTA_CONBUTIVEIS():ArrayCollection {
			var lista:ArrayCollection = new ArrayCollection();
/*  			lista.addItem(new CombustivelDTO('GNV','Gás Natural Veicular'));
			lista.addItem(new CombustivelDTO('GSO', 'Gasolina'));
			lista.addItem(new CombustivelDTO('ALC', 'Álcool'));
			lista.addItem(new CombustivelDTO('FLX', 'Flex'));
			lista.addItem(new CombustivelDTO('FGV', 'Flex + Gás'));
			lista.addItem(new CombustivelDTO('GGV', 'Gasolina + Gás'));
			lista.addItem(new CombustivelDTO('AGV', 'Álcool + Gás'));
			lista.addItem(new CombustivelDTO('TEF', 'Tetra Fuel')); */
 			return lista;
		}

		/*
		 static public const STATUS_PENDENTE:Object = {codigo:'PD', descricao:'Pendente'};
		 static public const STATUS_CONCLUIDO:Object = {codigo:'PG', descricao:'Concluído'};
		 static public const STATUS_ENVIADO:Object = {codigo:'EV', descricao:'Enviado'};
		 static public const STATUS_A_FATURAR:Object = {codigo:'AF', descricao:'A faturar'};
		 static public const STATUS_FATURADO:Object = {codigo:'FA', descricao:'Faturado'};
		 
         static public const LISTA_STATUS: ArrayCollection =  new ArrayCollection(
                                                                        [
                                                                        STATUS_PENDENTE,
																		STATUS_A_FATURAR,
																		STATUS_FATURADO,
                                                                        STATUS_CONCLUIDO,
                                                                       	STATUS_ENVIADO
                                                                        ]); */

        static public const LISTA_COMBUTIVEIS: ArrayCollection =  new ArrayCollection(
                                                                        [
                                                                        new CombustivelDTO('GNV', 'Gás Natural Veicular'),
                                                                        new CombustivelDTO('GSO', 'Gasolina'),
                                                                        new CombustivelDTO('ALC', 'Álcool'),
                                                                        new CombustivelDTO('FLX', 'Flex'),
																		new CombustivelDTO('DIE', 'Diesel'),
                                                                        new CombustivelDTO('FGV', 'Flex + Gás'),
                                                                        new CombustivelDTO('GGV', 'Gasolina + Gás'),
                                                                        new CombustivelDTO('AGV', 'Álcool + Gás'),
                                                                        new CombustivelDTO('TEF', 'Tetra Fuel'),
																		new CombustivelDTO('CNE', 'Não encontrado')
                                                                        ]);

        static public const LISTA_EMPRESAS: ArrayCollection =  new ArrayCollection(
                                                                        [
                                                                        null,
                                                                        {codigo:1,
                                                                        	descricao:'GROW UP GESTAO RH S/C LTDA'},
                                                                        {codigo:2,
                                                                        	descricao:'GROW UP SISTEMAS DE INFORMAÇÃO S/C LTDA'},
                                                                        {codigo:3,
                                                                        	descricao:'GROW UP ENGENHARIA DE TRANSITO S/C LTDA'}
                                                                        ]);
                                                                        
        static public const LISTA_TIPO_SERVICO: ArrayCollection =  new ArrayCollection(
                                                                        [
                                                                        {codigo:'S',
                                                                        	descricao:'Consulta Simples'},
                                                                        {codigo:'C',
                                                                        	descricao:'Consulta Completa'},
                                                                        {codigo:'V',
                                                                        	descricao:'Comunicação de Venda'},
                                                                        {codigo:'P',
                                                                        	descricao:'Processo Administrativo'}
                                                                        ]);
                                                                 
        static public const LISTA_TIPO_USUARIO: ArrayCollection = new ArrayCollection(
                                                                        [
                                                                        {codigo:'U',
                                                                        	descricaoBR:'Comum',
                                                                        	descricaoEN:'Common'},
                                                                        {codigo:'S',
                                                                        	descricaoBR:'Sistema',
                                                                        	descricaoEN:'System'},
                                                                        {codigo:'A',
                                                                        	descricaoBR:'Administrador',
                                                                        	descricaoEN:'Administrator'},
                                                                        {codigo:'E',
                                                                        	descricaoBR:'Externo',
                                                                        	descricaoEN:'Extern'}
                                                                        ]);
        static public const LISTA_IDIOMAS: Array = ['BR','EN'];                                                                        

		static public const LISTA_TIPOS_VEICULOS:ArrayCollection =  new ArrayCollection(['Automóvel', 'Moto', 'Caminhão', 'Caminhonete' ]);        
		
		static public const LISTA_CATEGORIA_VEICULOS:ArrayCollection =  new ArrayCollection(['Particular', 'Aluguel', 'Carga']);                                                            
		
		static public const LISTA_CORES:ArrayCollection =  new ArrayCollection(['Branco', 
																				'Amarelo',
																				'Azul',
																				'Verde',
																				'Prata',
																				'Vermelho',
																				'Preto',
																				'Cinza',
																				'Rosa',
																				'Bege',
																				'Laranja'
																				]);         
		
		static public const LISTA_SRF:ArrayCollection =  new ArrayCollection(['Sem ocorrência','Roubo e furto', 
																				'Recuperação',
																				'Liberação'
																				]);  
		static public const REGEX_LOGIN:String = "[A-Z\.0-9_]";
		static public const REGEX_EMAIL:String = "[a-z@\.0-9_]";
 
    }
}
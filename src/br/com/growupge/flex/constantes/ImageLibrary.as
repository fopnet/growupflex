package br.com.growupge.flex.constantes
{
	import flash.media.Sound;

    public class ImageLibrary {
        
        /*
        * Portal Imagens
        */
        [Embed(source="/_assets/fundo_header.png")]
        static public const GROWUP_HEADER_BACKGROUND:Class;

        [Embed(source="/_assets/logo_menor.png")]
        static public const GROWUP_LOGO:Class;

        [Embed(source="/_assets/background.png")]
        static public const BACKGROUND:Class;
        
        [Embed(source="/_assets/icones/usuario_login.png")]
        static public const USUARIO_LOGIN_ICON:Class;
        [Embed(source="/_assets/icones/ico_conectar.png")]
        static public const CONECTAR_BUTTON_ICON:Class;
        [Embed(source="/_assets/icones/ico_recriar_senha.png")]
        static public const RECRIAR_SENHA_BUTTON_ICON:Class;
        
        [Embed(source="/_assets/icones/aba_acessos.png")]
        static public const ABA_ACESSOS_ICON:Class;
        [Embed(source="/_assets/icones/aba_arquivos.png")]
        static public const ABA_ARQUIVOS_ICON:Class;
        [Embed(source="/_assets/icones/aba_documentos.png")]
        static public const ABA_DOCUMENTOS_ICON:Class;
        [Embed(source="/_assets/icones/aba_projetos.png")]
        static public const ABA_PROJETOS_ICON:Class;
        [Embed(source="/_assets/icones/aba_unidades.png")]
        static public const ABA_UNIDADES_ICON:Class;
        [Embed(source="/_assets/icones/aba_usuario.png")]
        static public const ABA_USUARIO_ICON:Class;
        /* *********** */
        
        /* Bullets */
        [Embed(source="/_assets/icones/bulletwarning.png")]
        static public const BULLET_WARNING:Class;
        [Embed(source="/_assets/icones/bulletcritical.png")]
        static public const BULLET_ERROR:Class;
        [Embed(source="/_assets/icones/bulletcheck.png")]
        static public const BULLET_CHECK:Class;
        
        /* Bandeiras Idioma*/
        [Embed(source="/_assets/icones/ico_br.png")]
        static public const BR_ICON:Class;
        [Embed(source="/_assets/icones/ico_en.png")]
        static public const EN_ICON:Class;
        [Embed(source="/_assets/icones/ico_es.png")]
        static public const ES_ICON:Class;
        [Embed(source="/_assets/icones/ico_it.png")]
        static public const OT_ICON:Class;
        
        [Embed(source="/_assets/icones/icoxp/24/flagbrasil.png")]
        static public const BR_24_ICON:Class;
		[Embed(source="/_assets/icones/icoxp/24/flageua.png")]
        static public const EN_24_ICON:Class;
        /* **** **** */
        
        /* Conexão */
		[Embed(source="/_assets/icones/icoxp/24/Arreter.png")]
        static public const DESCONECTAR_ICON:Class;
		[Embed(source="/_assets/icones/icoxp/24/UserAccounts01.png")]
        static public const OPCOES_USUARIO:Class;
        [Embed(source="/_assets/icones/ico_usuario.png")]
        static public const USUARIO_ICON:Class;

        /* CRUD */
        [Embed(source="/_assets/icones/exportXLS.png")]
        static public const EXPORT_XLS_ICON:Class;
        [Embed(source="/_assets/icones/import1.png")]
        static public const IMPORT_DATA:Class;
        [Embed(source="/_assets/icones/preferences.png")]
        static public const TRAMITACAO:Class;
        [Embed(source="/_assets/icones/notebook_preferences.png")]
        static public const TRAMITACAO_CONCLUIR:Class;
        
        [Embed(source="/_assets/icones/ico_bt_lupa.png")]
        static public const SELECIONAR_ICON:Class;
        [Embed(source="/_assets/icones/ico_ad_novo.png")]
        static public const NOVO_ICON:Class;
        [Embed(source="/_assets/icones/ico_editar.png")]
        static public const EDITAR_ICON:Class;
        [Embed(source="/_assets/icones/ico_visualizar.png")]
        static public const VISUALIZAR_ICON:Class;
        [Embed(source="/_assets/icones/icoxp/24/element_previous.png")]
        static public const REMOVER_ICON:Class;
        [Embed(source="/_assets/icones/document_delete.png")]
        static public const EXCLUIR_ICON:Class;
        [Embed(source="/_assets/icones/ico_historico.png")]
        static public const HISTORICO_ICON:Class;
        [Embed(source="/_assets/icones/copiar.png")]
        static public const COPIAR_ICON:Class;
        [Embed(source="/_assets/icones/icoxp/24/element_next.png")]
        static public const ADICIONAR_ICON:Class;
        [Embed(source="/_assets/icones/ico_bt_salvar.png")]
        static public const SALVAR_ICON:Class;
        [Embed(source="/_assets/icones/ico_bt_cancelar.png")]
        static public const CANCELAR_ICON:Class;
        [Embed(source="/_assets/icones/ico_bt_limpar.png")]
        static public const LIMPAR_ICON:Class;
        
        /* Paginacao */
        [Embed(source="/_assets/icones/ico_pg_anterior.png")]
        static public const VOLTAR_ICON:Class;
        [Embed(source="/_assets/icones/ico_pg_proxima.png")]
        static public const AVANCAR_ICON:Class;
        [Embed(source="/_assets/icones/ico_pg_primeira.png")]
        static public const PRIMEIRA_PAGINA_ICON:Class;
        [Embed(source="/_assets/icones/ico_pg_ultima.png")]
        static public const ULTIMA_PAGINA_ICON:Class;

        /* Menu Admin */
        [Embed(source="/_assets/icones/ico_seta_right.png")]
        static public const ACCORD_SETA_OFF_ICON:Class;
        [Embed(source="/_assets/icones/ico_seta_down.png")]
        static public const ACCORD_SETA_ON_ICON:Class;
        
        /* Mensagens Sistema*/
        [Embed(source="/_assets/icones/icoxp/48/Information.png")]
        static public const INFORMACAO_ICON_G:Class;
        [Embed(source="/_assets/icones/icoxp/48/Interrogation.png")]
        static public const INTERROGACAO_ICON_G:Class;
        
        /* Modulos */
        [Embed(source="/_assets/icones/administracao.png")]
        static public const ADMIN_MODULO_ICON:Class;

		/* Contato */
        [Embed(source="/_assets/icones/icoxp/48/mail.png")]
        static public const CONTATO_MAIL:Class;
		
		/* Uploader */
		[Embed(source="/_assets/fileUploaderClasses/cross.png")]
		static public const error:Class; 
		
		[Embed(source="/_assets/fileUploaderClasses/page_white_add.png")]
		static public const pending:Class;
		
		[Embed(source="/_assets/fileUploaderClasses/tick.png")]
		static public const completed:Class;
		
		[Embed(source="/_assets/fileUploaderClasses/page_white_get.png")]
		static public const ramloading:Class;
		
		[Embed(source="/_assets/fileUploaderClasses/page_white_key.png")]
		static public const checksumming:Class;
		
		[Embed(source="/_assets/fileUploaderClasses/page_white_go.png")] //  arrow_right.png
		static public const uploading:Class; 
		
		[Embed(source="/_assets/fileUploaderClasses/excluir.png")]
		static public const excluir:Class; 
		
		[Embed(source="/_assets/fileUploaderClasses/disk_up.png")]
		static public const disk_up:Class; 

		[Embed(source="/_assets/fileUploaderClasses/hide.png")]
		static public const show:Class; 

		[Embed(source="/_assets/fileUploaderClasses/hide.png")]
		static public const hide:Class; 

		[Embed(source="/_assets/fileUploaderClasses/add.png")]
		static public const add:Class; 

		[Embed(source="/_assets/fileUploaderClasses/delete.png")]
		static public const deleteImg :Class; 

    }
}
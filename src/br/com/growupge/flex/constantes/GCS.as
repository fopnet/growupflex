package br.com.growupge.flex.constantes
{
    /**
     *
     * Classe de Constantes para o Gerenciador de Comandos
     *
     * @author Fred
     *
     */
    public class GCS {
    static public const MAPA_USUARIO: String = "Usuario";
    static public const MAPA_USER: String = "User";
    static public const MAPA_SESSAO: String = "Sessao";
    static public const MAPA_MENSAGEM: String = "Mensagem";
    static public const MAPA_EMPRESA: String = "Empresa";
    static public const MAPA_CLIENTE: String = "Cliente";
    static public const MAPA_PEDIDO_VEICULO: String = "PedidoVeiculo";
    static public const MAPA_PERMISSAO: String = "Permissao";
    static public const MAPA_PERFIL: String = "Perfil";
    static public const MAPA_MARCA: String = "Marca";
    static public const MAPA_ARQUIVO: String = "Arquivo";
    static public const MAPA_MODELO: String = "Modelo";
    static public const MAPA_SERVICO: String = "Servico";
    static public const MAPA_UF: String = "UnidadeFederativa";
    static public const MAPA_PROPRIETARIO: String = "Proprietario";
    static public const MAPA_TRAMITACAO: String = "Tramitacao";
    static public const MAPA_VEICULO: String = "Veiculo";
    
    
    // Permissoes Genéricas
    static public const CONECTAR: String = "CONECTAR";
    static public const DESCONECTAR: String = "DESCONECTAR";
	static public const PROCURAR_SESSION:String = "PROCURAR_SESSAO";
	
    static public const PROCURAR: String = "PROCURAR";
    static public const BUSCAR_TODAS: String = "BUSCAR_TODAS";
    static public const BUSCAR_PERFIL_USUARIO: String = "BUSCAR_PERFIL_USUARIO";
    static public const BUSCAR_CLIENTES: String = "BUSCAR_CLIENTES";
    static public const BUSCAR_EMPRESAS: String = "BUSCAR_EMPRESAS";
    static public const BUSCAR_TOTAL: String = "BUSCAR_TOTAL"; 
	
	//Mensagem
	static public const ADICIONAR_MENSAGEM: String = "CRIAR_MENSAGEM";
	static public const ATUALIZAR_MENSAGEM: String = "ALTERAR_MENSAGEM";
	static public const EXCLUIR_MENSAGEM: String = "EXCLUIR_MENSAGEM";
	
	//Usuario
	static public const ADICIONAR_USUARIO: String = "CRIAR_USUARIO";
	static public const ATUALIZAR_USUARIO: String = "ALTERAR_USUARIO";
	static public const EXCLUIR_USUARIO: String = "EXCLUIR_USUARIO";
	static public const TROCAR_SENHA: String = "TROCAR_SENHA";
	static public const RECRIAR_SENHA: String = "RECRIAR_SENHA";
	
	//Empresa
	static public const ADICIONAR_EMPRESA: String = "CRIAR_EMPRESA";
	static public const ATUALIZAR_EMPRESA: String = "ALTERAR_EMPRESA";
	static public const EXCLUIR_EMPRESA: String = "EXCLUIR_EMPRESA";
	
	static public const ADICIONAR_EMPRESA_ITEM: String = "ADICIONAR_EMPRESA_ITEM";
	static public const ATUALIZAR_EMPRESA_ITEM: String = "ALTERAR_EMPRESA_ITEM";
	static public const EXCLUIR_EMPRESA_ITEM: String = "REMOVER_EMPRESA_ITEM";

	//Cliente
	static public const ADICIONAR_CLIENTE: String = "CRIAR_CLIENTE";
	static public const ATUALIZAR_CLIENTE: String = "ALTERAR_CLIENTE";
	static public const EXCLUIR_CLIENTE: String = "EXCLUIR_CLIENTE";

	//Pedido Veículo
	static public const ADICIONAR_PEDIDO_VEICULO: String = "CRIAR_PEDIDO_VEICULO";
	static public const ATUALIZAR_PEDIDO_VEICULO: String = "ALTERAR_PEDIDO_VEICULO";
	static public const EXCLUIR_PEDIDO_VEICULO: String = "EXCLUIR_PEDIDO_VEICULO";
	static public const EXPORTAR_FATURA: String = "EXPORTAR_FATURA";
	static public const CONCLUIR_PEDIDO: String = "CONCLUIR_PEDIDO";
	static public const CONCLUIR_PEDIDO_ENVIAR: String = "CONCLUIR_PEDIDO_ENVIAR";
	static public const REMOVER_ANEXO: String = "REMOVER_ANEXO";
	
	//Permissao
	static public const ADICIONAR_PERMISSAO: String = "CRIAR_PERMISSAO";
	static public const ATUALIZAR_PERMISSAO: String = "ALTERAR_PERMISSAO";
	static public const EXCLUIR_PERMISSAO: String = "EXCLUIR_PERMISSAO";

	//Perfil
	static public const ADICIONAR_PERFIL: String = "CRIAR_PERFIL";
	static public const ATUALIZAR_PERFIL: String = "ALTERAR_PERFIL";
	static public const EXCLUIR_PERFIL: String = "EXCLUIR_PERFIL";

	static public const ADICIONAR_PERFIL_ITEM: String = "ADICIONAR_PERFIL_PERMISSAO";
	static public const ATUALIZAR_PERFIL_ITEM: String = "ALTERAR_PERFIL_PERMISSAO";
	static public const EXCLUIR_PERFIL_ITEM: String = "REMOVER_PERFIL_PERMISSAO";

	//Marca
	static public const ADICIONAR_MARCA: String = "CRIAR_MARCA";
	static public const ATUALIZAR_MARCA: String = "ALTERAR_MARCA";
	static public const EXCLUIR_MARCA: String = "EXCLUIR_MARCA";

	//Arquivo
	static public const ADICIONAR_ARQUIVO: String = "CRIAR_ARQUIVO";
	static public const ATUALIZAR_ARQUIVO: String = "ALTERAR_ARQUIVO";
	static public const EXCLUIR_ARQUIVO: String = "EXCLUIR_ARQUIVO";

	//Modelo
	static public const ADICIONAR_MODELO: String = "CRIAR_MODELO";
	static public const ATUALIZAR_MODELO: String = "ALTERAR_MODELO";
	static public const EXCLUIR_MODELO: String = "EXCLUIR_MODELO";

	//Serviço
	static public const ADICIONAR_SERVICO: String = "CRIAR_SERVICO";
	static public const ATUALIZAR_SERVICO: String = "ALTERAR_SERVICO";
	static public const EXCLUIR_SERVICO: String = "EXCLUIR_SERVICO";

	//Proprietário
	static public const ADICIONAR_PROPRIETARIO: String = "CRIAR_PROPRIETARIO";
	static public const ATUALIZAR_PROPRIETARIO: String = "ALTERAR_PROPRIETARIO";
	static public const EXCLUIR_PROPRIETARIO: String = "EXCLUIR_PROPRIETARIO";

	//Tramitacao
	static public const ADICIONAR_TRAMITACAO: String = "CRIAR_TRAMITACAO";
	static public const ATUALIZAR_TRAMITACAO: String = "ALTERAR_TRAMITACAO";
	static public const EXCLUIR_TRAMITACAO: String = "EXCLUIR_TRAMITACAO";
	static public const CONCLUIR_TRAMITACAO: String = "CONCLUIR_TRAMITACAO";

	//Veiculo
	static public const ADICIONAR_VEICULO : String = "CRIAR_VEICULO";
	static public const ATUALIZAR_VEICULO: String = "ALTERAR_VEICULO";
	static public const EXCLUIR_VEICULO: String = "EXCLUIR_VEICULO";
	static public var PROCURAR_PEDIDO:String = "PROCURAR_PEDIDO";
	static public var CRIAR_NIP:String = "CRIAR_NIP";
	static public var IMPORTAR_DADOS:String = "IMPORTAR_DADOS";

	
    }
}
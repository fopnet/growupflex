package br.com.growupge.flex.model
{
	
	import com.adobe.cairngorm.model.ModelLocator;
	
	import flash.net.SharedObject;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.formatters.DateFormatter;
	
	import br.com.growupge.flex.valueobject.SessaoDTO;
	import br.com.growupge.flex.valueobject.UsuarioDTO;

	[Bindable]
	public class SigemModelLocator implements ModelLocator {		
		public static var instance: SigemModelLocator;
		
		public static function getInstance():SigemModelLocator {
			if (instance == null) {
				instance = new SigemModelLocator();
			}
			return instance;
		}
		
		public function SigemModelLocator() {
			if (instance != null) {
				throw new Error("Somente pode haver uma instância de ModelLocator");
			}
      		sessaoDTO.usuario = new UsuarioDTO();		
		}
		
		private function getSwfName():String {
			var swfName:String = Application.application.loaderInfo.url;
			var matches:Array = swfName.match(/(?i)\b\w+(.swf)\b/);
			if (matches.length > 0)
				return matches[0].toString().toLowerCase();
			
			return '';
		}
		
		public function isVeiculosSwf():Boolean {
			return VEICULOS_SWF == getSwfName();
		}
		
		/**
		 * Instancia objetos do Modelo
		 * */

		/* Modo Debug */
        public var isDebug:Boolean = false;
        
        /* CONEXAO */
		public var sessaoDTO: SessaoDTO = new SessaoDTO();
		public var isConectarHabilitado : Boolean = false;
        
        /* Tradutor - Multilinguagem*/
        public var linguagem : Linguagem = new Linguagem();
        
        /*ESTADOS DA APLICACAO*/
        public var appState : int;
        public var usuarioState:String; //Usado no SigemHeader
		public static const CONECTAR_VIEW : int = 0;
		public static const SIGEM_APP_VIEW : int = 1;
//		public static const ADMIN_VIEW : int = 2;
		public static const VAREJO_VIEW : int = 2;
		
		public static var VEICULOS_SWF:String = 'veiculos.swf'
		public static var PORTAL_SWF:String = 'index.swf'
        
        /*RETORNO BackEnd*/
        private var _mensagemRetorno:String = '';
		
		public function get mensagemRetorno() :String {
			return _mensagemRetorno;
		}
		public function set mensagemRetorno(value:String):void {
			_mensagemRetorno = '';
			_mensagemRetorno  = value;
		}

        /* Formatters */
        public var dataFormatter:DateFormatter = new DateFormatter();
        
        // Providers
        public var dataProviderUsuario : ArrayCollection ;
        public var config:XML;
       
        /* CRUD */
	    public var max_registros_retornados_paginacao:uint;
		  
		public function addSidToSharedObject():void
		{
			try {
				var shared:SharedObject = SharedObject.getLocal("sid","/GrowUpFlex");
				shared.data.sid = sessaoDTO.sid;
			} catch (err:Error) { 
				// evitando Error #2134: Cannot create SharedObject.
				trace (err);
				//Alert.show(err);
			}
		}
		
		public function getSidFromSharedObject():void
		{
			try {
				var shared:SharedObject = SharedObject.getLocal("sid","/GrowUpFlex");
				sessaoDTO.sid = shared.data.sid as String;
				shared.clear();
			} catch (err:Error) { 
				// evitando Error #2134: Cannot create SharedObject.
				trace (err);
				//Alert.show(err);
			}
			
		}
	}
}
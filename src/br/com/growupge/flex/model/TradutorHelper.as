package br.com.growupge.flex.model
{
    import br.com.growupge.flex.event.GetTradutorEvent;
    
    import com.adobe.cairngorm.control.CairngormEventDispatcher;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    
    import mx.formatters.SwitchSymbolFormatter;
    import mx.rpc.events.ResultEvent;
    import mx.utils.StringUtil;
    
    public class TradutorHelper extends EventDispatcher {
        private static var instance : TradutorHelper;
        private static var model : SigemModelLocator = SigemModelLocator.getInstance();
        
        public static function getInstance() : TradutorHelper {
    		if ( instance == null ) {
    			instance = new TradutorHelper();
    		}				
    		return instance;
       	}
    
       	public function TradutorHelper() {	
       		if ( instance != null ) {
    			throw new Error(
    			        "Usar TradutorHelper.getInstance()" );
       		}
       		instance = this;	   		
       	}
       	
       	/**
       	 * Define um dicionário para a linguagem a ser utilizada
       	 * Este método é invocado pelo GetTradutorCommand ao retornar a requisição ao BackEnd
       	 * */
       	public function setTradutor (dicionario:Object) : void{
       	    model.linguagem.dicionario = dicionario;
       	    dispatchEvent( new Event( "mudarLinguagem" ) );
       	}
       	
       	/**
          * Dispara Evento solicitando Dicionário para a linguagem desejada
          **/
        public function getTradutor(sigla : String) : void {
            var event : GetTradutorEvent = new GetTradutorEvent(sigla);
            CairngormEventDispatcher.getInstance().dispatchEvent( event );
        }
        
        public function setLinguagem(linguagem:Linguagem) : void {
            model.linguagem = linguagem;
        }
        
        public function getLinguagem() :Linguagem {
            return model.linguagem;
        }
        
        public function getIdiomaUsuario():String {
            return model.sessaoDTO.usuario.idioma;
        }
        
        /**
        * Metodo a ser invocado em cada texto a ser traduzido na aplicação
        * Ao ocorrer o Evento "mudarLinguagem"(novo dicionario) os texto são atualizados
        * */
        [Bindable(event="mudarLinguagem")]
       	public function getTexto(chave:String, ... params:Array) : String {
            var texto:String = model.linguagem.getTexto(chave);
			
			for (var i:uint=0;i<params.length;i++) {
				texto = StringUtil.substitute(texto, params);	
			}
				
        	return texto;
        }
        /**
        * Metodo a ser invocado em cada Array a ser traduzido na aplicação
        * Ao ocorrer o Evento "mudarLinguagem"(novo dicionario) os textos são atualizados
        * */
        [Bindable(event="mudarLinguagem")]
       	public function getArrayTexto(chave:String) : Array {
            var array:Array = String(model.linguagem.getTexto(chave)).split(',');
        	return array;
        }        
    }
}
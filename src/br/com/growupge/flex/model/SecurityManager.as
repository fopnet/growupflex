package br.com.growupge.flex.model
{
    import br.com.growupge.flex.componentes.crud.ComandosCRUDEvent;
    import br.com.growupge.flex.valueobject.Parameter;
    import br.com.growupge.flex.valueobject.PerfilDTO;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    import br.com.growupge.flex.constantes.GCS;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    
    import com.adobe.cairngorm.control.CairngormEventDispatcher;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import mx.collections.ArrayCollection;
    
    [Event(name="permissoesAtualizadas")]
    [Event(name="usuarioChanged")]
    
    public class SecurityManager extends EventDispatcher {
        private static var instance : SecurityManager;
        public static const PERMISSOES_ATUALIZADAS:String = "permissoesAtualizadas";
        public static const USUARIO_CHANGED:String = "usuarioChanged";
        private var permissoes:ArrayCollection = new ArrayCollection();
        private var _context:PerfilDTO;
        
        public static function getInstance() : SecurityManager {
    		if ( instance == null ) {
    			instance = new SecurityManager();
    			instance.context = SigemModelLocator.getInstance().sessaoDTO.usuario.perfil;
    		}				
    		return instance;
       	}
    
       	public function SecurityManager() {	
       		if ( instance != null ) {
    			throw new Error(
    			        "Usar SecurityManager.getInstance()" );
       		}
       		instance = this;	   		
       		this.addEventListener(ComandosCRUDEvent.RESULTADO, resultadoGetPermissoes);
       	}

        [Bindable(event="contextChanged")]
        public function set context(value:PerfilDTO):void {
            this._context = value;
            setPermissoes(new ArrayCollection());
            getPermissoes(this._context);
            dispatchEvent( new Event( "contextChanged" ) );
        }
        public function get listaPermissoes():Array{
            return this.permissoes.toArray();
        }
        public function get context():PerfilDTO{
            return this._context;
        }
       	
       	public function reload():void{
       	    setPermissoes(new ArrayCollection());
       	    this.getPermissoes(context);
       	}
       	/**
       	 * Define um dicionário das permissoes
       	 * Este método é invocado pelo BuscarTodasPermissoesUsuarioCommand ao retornar a requisição ao BackEnd
       	 * */
       	public function setPermissoes ( value:ArrayCollection ) : void{
       	    permissoes = value;
       	    dispatchEvent( new Event( "permissoesAtualizadas" ) );
       	}
       	
       	/**
          * Dispara Evento para Back End solicitando permissoes neste contexto
          **/
        private function getPermissoes(contexto:PerfilDTO) : void {
            if (contexto) {
                var comando:ComandoDTO = new ComandoDTO();
                comando.mapaGCS = GCS.MAPA_PERMISSAO;
                comando.permissao = GCS.BUSCAR_TODAS;
                comando.parametrosLista.addItem(contexto);
                
                var event : ExecutarComandoEvent= new ExecutarComandoEvent(comando, ExecutarComandoEvent.COMANDO_GENERICO);
                event.eventTargetResult = new ComandosCRUDEvent( ComandosCRUDEvent.RESULTADO );
                event.resultadoDisplayObject = this;
                CairngormEventDispatcher.getInstance().dispatchEvent( event );
            }
        }
        private function resultadoGetPermissoes(event:ComandosCRUDEvent):void {
            this.setPermissoes(event.result as ArrayCollection);
        }
        /**
        * Metodo a ser invocado em cada controle a ser utilizado
        * Ao ocorrer o Evento "permissoesAtualizadas" os controles são atualizados
        * */
        [Bindable(event="permissoesAtualizadas")]
       	public function getPermissao(chave:String) : Boolean {
        	return this.buscaPermissao(chave);
        }
        [Bindable(event="permissoesAtualizadas")]
       	public function getArrayPermissao(chaves:Array) : Boolean {
       	    var possuiPermissao:Boolean;
       	    for each (var chave:String in chaves){
       	        possuiPermissao = this.buscaPermissao(chave);
       	        if (possuiPermissao) break;
       	    }
        	return possuiPermissao;
        }
        
        private function buscaPermissao(chave:String):Boolean {
            var possuiPermissao:Boolean =  permissoes.contains(chave);
            return possuiPermissao;
        }
    }
}
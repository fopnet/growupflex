package br.com.growupge.flex.model
{
      
    public class Linguagem {
        public var dicionario : Object;
        private var _nome : String ;
        public var sigla : String;
        
        /**
        * Constantes dos dicionários disponíveis no BackEnd
        * */
        public static const BR : String = "BR";
        public static const EN : String = "EN";
        public static const ES : String = "ES";
        public static const OT : String = "OT";
        
        public function Linguagem(sigla:String = BR){
                this.sigla = sigla;
        }
        
        public function get nome() : String{
                return getTexto(sigla);
        }
                                               	
    	public function getTexto(chave:String) : String {
	        if ( this.dicionario == null || this.dicionario[chave] == null) {
        	        return "?" + chave;
	        } else 
				return this.dicionario[chave] as String;
	    }
    }
}
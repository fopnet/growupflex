package br.com.growupge.flex.util
{
	import mx.utils.StringUtil;
	
	public class StringUtilExt extends StringUtil
	{
		private static var CARACTERES_ACENTUADOS:Array = ["Ç","C","ç","c","ü","u","û","u","ú","u","ù","u","é","e","ê","e","ë","e","è","e","â","a","ä","a","à","a","å","a","á","a","ï","i","î","i","ì","i","í","i","Ä","A","Å","A","É","E","æ","ae","Æ","Ae","ô","o","ö","o","ò","o","ó","o","Ü","U","ÿ","Y","Ö","O","ñ","n","Ñ","N"]; 

		public function StringUtilExt()
		{
			super();
		}
		
		public static function trimOrNull(str:String):String {
			var valor:String = trim(str);
			return valor.length == 0  ? null : valor; 
		}
		
		public static function removerAcentos(str:String):String {
			var retorno:String = "";
			for (var idx:int=0; idx <str.length; idx++) {
				var char:String  = str.charAt(idx);
				var idxFound:int = CARACTERES_ACENTUADOS.indexOf( char );
				if (idxFound >=0) 
					str = str.concat( CARACTERES_ACENTUADOS[ idxFound + 1] );
				else 
					str = str.concat( char );
			}
			
			return retorno;
		}
		
		public static function isNumber(s: String) : Boolean {
			return s != null && s.match(/[0-9]+.?[0-9]+,?[0-9]+/);
		}
	}
}
package br.com.growupge.flex.util.effects
{
	
import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.filters.BlurFilter;
import flash.geom.Point;

import mx.containers.Canvas;
import mx.controls.Image;
import mx.core.Container;
import mx.core.ScrollPolicy;
import mx.core.UIComponent;
import mx.effects.Move;
import mx.effects.Parallel;
import mx.effects.Resize;
import mx.events.EffectEvent;
import mx.managers.PopUpManager;
	
[Event("moveInEnd")]
[Event("moveOutEnd")]
	
public class MoveAwayFX extends EventDispatcher
{

	public static const MOVE_IN_END:String = "moveInEnd";
	public static const MOVE_OUT_END:String = "moveOutEnd";
	
	private var isPlaying:Boolean = false;
	
	public function moveOut():void
	{
		var move:Move = new Move();
		var fromPoint:Point = getGlobalPoint(affectedTarget);
		move.xFrom = fromPoint.x;
		move.yFrom = fromPoint.y;
		var toPoint:Point = getGlobalPoint(destinationTarget);
		move.xTo = toPoint.x;
		move.yTo = toPoint.y;
		
		var resize:Resize = new Resize();
		resize.widthTo = destinationTarget.width;
		resize.heightTo = destinationTarget.height;
		
		var moveAndResize:Parallel = new Parallel();
		moveAndResize.duration = duration;
		moveAndResize.addChild(move);
		moveAndResize.addChild(resize);
				
		moveAndResize.addEventListener
		(
			EffectEvent.EFFECT_END, 
			function():void 
			{
				snapshotPopUp.visible = false;
				dispatchEvent(new Event(MoveAwayFX.MOVE_OUT_END));
				isPlaying = false;
			}
		);
		
		
		if (!isPlaying)
		{
			affectedTarget.visible = false;
			createSnapshotPopup(affectedTarget);
			moveAndResize.play([snapshotPopUp]);
			isPlaying = true;
		}
		
	}
	
	public function moveIn():void
	{
		var move:Move = new Move();
		var fromPoint:Point = getGlobalPoint(destinationTarget);
		move.xFrom = fromPoint.x;
		move.yFrom = fromPoint.y;
		var toPoint:Point = getGlobalPoint(originTarget);
		move.xTo = toPoint.x;
		move.yTo = toPoint.y;
		
		var resize:Resize = new Resize();
		resize.widthFrom = destinationTarget.width;
		resize.heightFrom = destinationTarget.height;
		resize.widthTo = originTarget.width;
		resize.heightTo = originTarget.height;
		
		var moveAndResize:Parallel = new Parallel();
		moveAndResize.duration = duration;
		moveAndResize.addChild(move);
		moveAndResize.addChild(resize);
				
		moveAndResize.addEventListener
		(
			EffectEvent.EFFECT_END, 
			function():void 
			{
				removePopup();
				affectedTarget.visible = true;
				//affectedTarget.verticalScroll = 0;
				dispatchEvent(new Event(MoveAwayFX.MOVE_IN_END));
				isPlaying = false;
			}
		);
		
		
		if (!isPlaying)
		{
			createSnapshotPopup(affectedTarget);
			snapshotPopUp.visible = true;
			moveAndResize.play([snapshotPopUp]);
			isPlaying = true;
		}
		
	}
	
	private function getGlobalPoint(target:UIComponent):Point
	{
		var parentContainer:Container = getParentContainer(target);
		
		if (parentContainer)
		{
			return parentContainer.contentToGlobal(new Point(target.x, target.y));
		}
		
		return new Point(target.x, target.y);
		
	}	
	
	private function getParentContainer(target:UIComponent):Container
	{
		var parentContainer:Object = target.parent;
		while ( parentContainer && !(parentContainer is Container) )
		{
			if (parentContainer)
			{
				parentContainer = parentContainer.parent;
			}
		}		
		return Container(parentContainer);
	}

	private var snapshotPopUp:Canvas;
	
	private function createSnapshotPopup(target:UIComponent):void
	{
		var popUpParent:DisplayObject = mx.core.Application.application as DisplayObject;
		snapshotPopUp = PopUpManager.createPopUp(popUpParent, Canvas) as Canvas;
		snapshotPopUp.horizontalScrollPolicy = ScrollPolicy.OFF;
		snapshotPopUp.verticalScrollPolicy = ScrollPolicy.OFF;
		snapshotPopUp.setActualSize(target.width, target.height);
		snapshotPopUp.filters = [new BlurFilter(20, 20)];
		
		var snapShot:Image = Snapshot.getSnapshot(target);
		snapshotPopUp.addChild(snapShot);
	}
	
	private function removePopup():void
	{
		PopUpManager.removePopUp(snapshotPopUp);
	}

	//--------------------------------------------------------------------------
	//
	//  Properties
	//
	//--------------------------------------------------------------------------

	//----------------------------------
	//  affectedTarget
	//----------------------------------

	private var _affectedTarget:UIComponent;
	
	[Bindable]
	
	public function get affectedTarget():UIComponent
	{
		return _affectedTarget;
	}
	
	public function set affectedTarget(value:UIComponent):void
	{
		_affectedTarget = value;
	}
	
	
	//----------------------------------
	//  originTarget
	//----------------------------------
	
	private var _originTarget:UIComponent;
	
	[Bindable]
	
	public function get originTarget():UIComponent
	{
		return _originTarget == null ? _affectedTarget : _originTarget;
	}
	
	public function set originTarget(value:UIComponent):void
	{
		_originTarget = value;
	}
	
	
	//----------------------------------
	//  destinationTarget
	//----------------------------------
	
	private var _destinationTarget:UIComponent;
	
	[Bindable]
	
	public function get destinationTarget():UIComponent
	{
		return _destinationTarget;
	}
	
	public function set destinationTarget(value:UIComponent):void
	{
		_destinationTarget = value;
	}
	
	//----------------------------------
	//  destinationTarget
	//----------------------------------
	
	private var _duration:int = 500;
	
	[Bindable]
	
	public function get duration():int
	{
		return _duration;
	}
	
	public function set duration(value:int):void
	{
		_duration = value;
	}


}

}
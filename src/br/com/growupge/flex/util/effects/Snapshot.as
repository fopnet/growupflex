package br.com.growupge.flex.util.effects
{
	
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Matrix;

import mx.controls.Image;
import mx.core.UIComponent;
	
public class Snapshot
{
	
	public static function getSnapshot(target:UIComponent):Image
	{
		var bitmapData:BitmapData = Snapshot.getBitmapData(target);
		var image:Image = new Image();
		image.source = new Bitmap(bitmapData);
		return image;
	}
	
	private static function getBitmapData(target:UIComponent):BitmapData
	{
		var bitmapData:BitmapData = new BitmapData(target.width, target.height, true, 0);
		var matrix:Matrix = new Matrix();
		bitmapData.draw(target, matrix);
		return bitmapData;
	}
	
}

}
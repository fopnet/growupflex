package br.com.growupge.flex.itemRenderer
{
    import mx.controls.Button;
    import mx.core.IDataRenderer;
    import mx.controls.ButtonLabelPlacement;
    import mx.containers.accordionClasses.AccordionHeader;

    public class AccordionHeaderMenu extends AccordionHeader implements IDataRenderer
    {
        public function AccordionHeaderMenu()
        {
            super();
            this.labelPlacement = ButtonLabelPlacement.LEFT;
            setStyle("paddingRight", 8)
        }
        
        /* public function get data():Object {
            return super.data;
        }
        
        public function set data(value:Object):void {
            super.data = value;
        } */
        
    }
}
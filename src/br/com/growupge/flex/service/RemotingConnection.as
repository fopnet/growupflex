package br.com.growupge.flex.service
{
	import flash.net.NetConnection;
	import flash.net.ObjectEncoding;
	import mx.core.MXMLObjectAdapter;
	import br.com.growupge.flex.constantes.Constantes;

	public class RemotingConnection extends NetConnection
	{
		public  static var instance: RemotingConnection;
    	
	    public function RemotingConnection(){
	    	if (instance != null) {
				throw new Error("Somente pode haver uma instância de RemotingConnection");
			}
            objectEncoding = ObjectEncoding.AMF0;
            connect(Constantes.GATEWAY);
        }
            
		public static function getInstance():RemotingConnection {
			if (instance == null) {
				instance = new RemotingConnection();
			}
			return instance;
		}
		
		
	}
}
package br.com.growupge.flex.formatters
{
    import mx.core.UIComponent;
    import flash.events.Event;
    import mx.formatters.DateFormatter;
    import br.com.growupge.flex.model.TradutorHelper;
    import mx.resources.Locale;

	public class DataFormatter extends DateFormatter implements IFormatterSigem {
		
		public var source:UIComponent;
		public var property:String;
		
  		public static const millisecondsPerMinute:int = 1000 * 60;
		public static const millisecondsPerHour:int = 1000 * 60 * 60;
		public static const millisecondsPerDay:int = 1000 * 60 * 60 * 24;
		
		public function DataFormatter(){
			super();
			this.formatString = TradutorHelper.getInstance().getTexto('formatDateTimeString');
		}
		
		/**
		 * Método formata a data de acordo com o TimeZone local
		 **/
        override public function format(value:Object):String {
	        if (value != null) {
	        	var date:Date;
	        	if (value is Date) {
			        date = value as Date;
	        	} else {
	        		date = new Date( value );
	        	}
		        //date.setTime( date.getTime() - (date.getTimezoneOffset() - 60) * millisecondsPerMinute);
		        return super.format(date.toLocaleString());
	        } else {
	        	return null;
	        }
        }
        
        public function doFormat(event:Event):void {
            this.source[property] = this.format(this.source[property]);
        }
 }

}
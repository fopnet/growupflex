
package br.com.growupge.flex.formatters
{
	import mx.formatters.Formatter;
	import flash.events.Event;
	import mx.core.UIComponent;

	public class CPFFormatter extends Formatter implements IFormatterSigem {
		
		public var formattedString:String = "";
		public var source:UIComponent;
		public var property:String;
		
		public function CPFFormatter() {
			super();
		}
		
		/**
		 * Método utilizado para aplicar o formato "000.000.000-00" ao campo CPF.
		**/
		override public function format(value:Object):String {
			var cpf:String;
			
  			if (value != null) {
  				
				cpf = String(value);
  				    
  				if(cpf.length == 11) {
  					formattedString = cpf.substr(0,3) + "." + 
  									  cpf.substr(3,3) + "." + 
  									  cpf.substr(6,3)  + "-" + 
  									  cpf.substr(9,2);
  				}else if (cpf.length == 14){
  					cpf = cpf;
  				}else {
  				    cpf = cpf.replace(".","").replace(".","").replace(".","").replace("-","");
  				    formattedString = cpf;
  				}
            }
            
			return formattedString;
	
        }
        
        public function doFormat(event:Event):void {
            this.source[property] = this.format(this.source[property]);
        }
	}
}
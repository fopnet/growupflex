package br.com.growupge.flex.formatters
{
	import mx.formatters.Formatter;
	import flash.events.Event;
	import mx.core.UIComponent;
	
	public class CNPJFormatter extends Formatter implements IFormatterSigem {
		public var formattedString:String = "";
		public var source:UIComponent;
		public var property:String;
		
		public function CNPJFormatter(){
			super();
		}
	
		/**
		 * Método utilizado para aplicar o formato "00.000.000/0000-00" ao campo CNPJ.
		**/
	    override public function format(value:Object):String {
			var cnpj:String;
			
  			if (value != null) {
  				
				cnpj = String(value);
  				    
  				if(cnpj.length ==14) {
  					formattedString = cnpj.substr(0,2) + "." + 
  									  cnpj.substr(2,3) + "." + 
  									  cnpj.substr(5,3)  + "/" + 
  									  cnpj.substr(8,4) + "-" + 
  									  cnpj.substr(12,2);
  				}else if (cnpj.length == 18){
  					cnpj = cnpj;
  				}else {
  				    cnpj = cnpj.replace("-","").replace("/","").replace(".","").replace(".","");
  				    formattedString = cnpj;
  				}
            }
            
			return formattedString;
	
        }
        
        public function doFormat(event:Event):void {
            this.source[property] = this.format(this.source[property]);
        }
 	}
        
}
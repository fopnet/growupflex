package br.com.growupge.flex.formatters
{
   import mx.formatters.Formatter;
    import flash.events.Event;
    import mx.utils.StringUtil;
    import mx.controls.TextInput;
    import flash.events.KeyboardEvent;
    import mx.effects.easing.Back;

	public class PlacaFormatter extends Formatter	implements IFormatterSigem {
		
		public var source:TextInput;
		public var property:String;
		
		public function PlacaFormatter(){
			super();
		}
		
		/**
		 * Método utilizado para aplicar o formato #####-### ao campo CEP.
		 **/
        override public function format(value:Object):String {
			var placa:String;
			var idxSperator:int = -1; 
			
  			if (value != null) {
	
				placa = String(value);
				idxSperator = placa.indexOf("-");

				/* Formatação de Letras Confere */
				if (placa.match(/^[A-Z]{3}$/) != null) {
					if (idxSperator != 3 && idxSperator > 0) {
						placa = placa.replace("-","");
						placa = placa.slice(0,3).concat("-").concat(placa.substring(4));
					} else {
						placa = placa.concat("-");
						TextInput(this.source).setSelection(placa.length,placa.length);
					}
				} else	if (idxSperator != 3) {
					// Ainda digitando letras o quebrando a formatação de letras
					if (placa.length <= 3) {
						placa = placa.replace("-","");
					} else {
						placa = placa.substr(0,3).concat("-").concat(placa.substring(3));
					}
				}
					
            }
			return placa;
        }
	                    
 	                    
        private function validate(value:String):Boolean {
        	var pattern:RegExp;
        	if (value.substring(0,3).match(/^[A-Z]{1,3}$/) == null ) {
        		 return false;
        	} else if (value.length > 3 && value.substring(4).match(/^\d{1,4}$/) == null) {
        		return false;
        	}
        	return true;
        }
        
        private function cancelarTecla(value:String):String {
        	var placa:String = value ? String(value): "";
        	var parte:String;
        	var idx:int = -1;

        	if (placa.substring(0,3).match(/^[A-Z]{1,3}$/) == null) {
				parte = placa.substring(0,3);
        		idx = parte.search(/[^A-Z]/);
        		placa = placa.replace(placa.charAt(idx),'');
        		placa = placa.replace('-','');
        		
        		return placa;
        	} else if (placa.substr(4).match(/^[A-Z]{1,3}-\d{1,4}$/) == null) {
        		placa = placa.replace('-','');
       			parte = placa.substring(3);
        		idx = parte.search(/[^0-9]/);
	    		parte = parte.replace(parte.charAt(idx),'');
	    		if (placa.charAt(3) != '-') {
       				placa = placa.substr(0,3).concat("-").concat(parte);
       			}
    		
	    		return placa;
        	}

        	return placa;
        }
        
        public function doFormat(event:Event):void {
        	var placa:String = this.source[property];
        	
        	if (validate(placa)) {
        		placa = this.format(placa);
        	} else {
        		placa = this.cancelarTecla(placa);
        	}
        	
        	this.source[property] = placa.substr(0,8);
		 }
	}

}

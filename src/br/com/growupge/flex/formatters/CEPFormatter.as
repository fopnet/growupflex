package br.com.growupge.flex.formatters
{
    import mx.formatters.Formatter;
    import mx.core.UIComponent;
    import flash.events.Event;
    import mx.utils.StringUtil;

	public class CEPFormatter extends Formatter	implements IFormatterSigem {
		
		public var formattedString:String = "";
		public var source:UIComponent;
		public var property:String;
		
		private var i:int = 0; //contador para percorrer String de cep.
		
		public function CEPFormatter(){
			super();
		}
		
		/**
		 * Método utilizado para aplicar o formato #####-### ao campo CEP.
		 **/
        override public function format(value:Object):String {
			var cep:String;
			
  			if (value != null) {
  				
				cep = String(value);
				
				if (cep.replace("-","").length ==8) {
					cep = cep.replace("-","");
  					formattedString = cep.substr(0,5) + "-" + cep.substr(5,3);
  				}else if ((cep.length == 8) && (cep.indexOf("-") != -1)){
  					cep = formattedString.replace("-","");
  					cep = cep.substr(0,7);
  					formattedString = cep;
  				}else{
  					cep = cep.replace("-","");
  					formattedString =cep;
  				}
            
            }
			return formattedString;
	
        }
        
        public function doFormat(event:Event):void {
            this.source[property] = this.format(this.source[property]);
        }
 }

}
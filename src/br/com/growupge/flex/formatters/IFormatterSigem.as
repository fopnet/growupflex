package br.com.growupge.flex.formatters
{
    import flash.events.Event;
    
    public interface IFormatterSigem  {
        function doFormat(event:Event):void;
    }
}
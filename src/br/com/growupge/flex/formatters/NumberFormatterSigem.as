package br.com.growupge.flex.formatters
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	import mx.controls.TextInput;
	import mx.core.UIComponent;
	import mx.formatters.NumberFormatter;

	public class NumberFormatterSigem extends NumberFormatter implements IFormatterSigem {
		public var source:UIComponent;
		public var property:String;
		private var anterior:String = new String;
			
		public function NumberFormatterSigem() {
			super();
			this.precision = 3;
			this.decimalSeparatorTo = ",";
			this.thousandsSeparatorTo = ".";
		}

		public function focusOut(event:Event):void {
	        var valor:String = this.source[property].toString();
	        valor = valor.replace(",", ".");
	        
	        var r:Number = parseFloat(valor);
	        this.useThousandsSeparator = true;
	        var result:String = null;
	        
	        if(r == 0) {
	        	result = r.toString();
	        	
	        } else {
	        	result = this.format(r.toString());
	        }
	        
	        if(result.charAt(0) == ",") {
	        	result = "0" + result;
	        }
	        
	        this.source[property] = result;
		}
		
		public function focusIn(event:Event):void {
			var valor:String = this.source[property].toString();
			valor = valor.replace(".", "").replace(".", "").replace(".", "");
			this.source[property] = valor;
			
			var size:Number =  valor.length;
	        this.source['selectionBeginIndex'] = size;
	        this.source['selectionEndIndex'] = size;
		}

		public function doFormat(event:Event):void {
			if (event['data'] == this.source){
				var valor:String = this.source[property].toString();
				valor = valor.replace(".", "").replace(".", "").replace(".", "");
		        var virgulaAntes:Number = valor.indexOf(",");
		        var erro:String = valor.substring(virgulaAntes + 1);
		        var virgulaDepois:Number = erro.lastIndexOf(",");
		        
		        if(virgulaAntes != -1) {
		        	var limite:String = valor.substring(virgulaAntes + 1);
		        	
		        	if((limite.length > 3) || (virgulaDepois != -1)) {
		        		valor = this.anterior;
		        	}
		        }
		        
		        valor = valor.replace(",", ".");
		        var numero:Number = parseFloat(valor);
		        var result:String = null;
		        		        
		        if((numero.toString().length == 10) && (virgulaAntes == -1)) {
		        	result = this.anterior + "," + valor.charAt(9);
		        
		        } else if((valor.charAt(0) == '0') && (virgulaAntes == -1)) {
		        	result = numero.toString();
		        	
		        } else if((valor.length == 12) && (virgulaAntes == -1)) {
		        	var caracterAntes:String = valor.substring(0, 9);
		        	var caracterDepois:String = valor.substr(9, 12);
		        	result = caracterAntes + "," + caracterDepois;
		        	
		        } else {
		        	result = valor.replace(".", ",");
		        }
		        
		        this.anterior = result;
		        this.useThousandsSeparator = false;
		        this.source[property] = result;
		        
		        var size:Number =  result.length;
		        this.source['selectionBeginIndex'] = size;
		        this.source['selectionEndIndex'] = size;
	  		}
  		}
  		
		public function moverSeta(event:KeyboardEvent):void {
	        if (event.charCode == 0) {
	        	TextInput(event.currentTarget).selectionBeginIndex = TextInput(event.currentTarget).length;
	            TextInput(event.currentTarget).selectionEndIndex = TextInput(event.currentTarget).length;
	        }
		} 
	}
}
package br.com.growupge.flex.admin.command
{
    import mx.controls.Alert;
    import mx.rpc.events.ResultEvent;
    
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.constantes.ImageLibrary;
    import br.com.growupge.flex.model.TradutorHelper;
    import br.com.growupge.flex.valueobject.SenhaDTO;
    
	public class TrocarSenhaCommand extends BaseCommand {
		[Bindable]
        private var tradutor : TradutorHelper = TradutorHelper.getInstance();		   
        
		public override function result( data : Object ) : void {
			super.result(data);			
			var event:ResultEvent = data as ResultEvent;

			//AMF0 var senha :SenhaDTO = ConvertDTO.toSenhaDTO(data);
			var senha :SenhaDTO = event.result as SenhaDTO;
		    Alert.show(senha.mensagemRetorno, senha.codMensagem ,0,null,null,ImageLibrary.INFORMACAO_ICON_G);
		}		
	
	}
}
package br.com.growupge.flex.admin.command
{
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.model.SigemModelLocator;
    
    import mx.rpc.events.ResultEvent;
    import mx.collections.ArrayCollection;
    import br.com.growupge.flex.componentes.crud.DataGridCRUD;
    import mx.controls.DataGrid;
    import br.com.growupge.flex.valueobject.MensagemRetornoDTO;
    
			
	public class AtualizarCRUDCommand extends BaseCommand {
		private var model : SigemModelLocator = SigemModelLocator.getInstance();	
		
		public override function result( data : Object ) : void {
			super.result(data);	
			var event:ResultEvent = data as ResultEvent;						
			var dto:MensagemRetornoDTO = (data as ResultEvent).result as MensagemRetornoDTO;
			
			model.mensagemRetorno = dto.mensagemRetorno;
			//AMF0 model.mensagemRetorno = data.mensagemRetorno;

			var dataProvider:ArrayCollection = ArrayCollection(DataGridCRUD(this.resultado).dataProvider);
        	var idx: int = DataGrid(this.resultado).selectedIndex ;
            dataProvider.setItemAt( dto , idx );
            DataGridCRUD(this.resultado).selectedIndex = dataProvider.getItemIndex(dto);
           
		}
				
	}
}
package br.com.growupge.flex.admin.command
{
    import br.com.growupge.flex.admin.model.AdminModelLocator;
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.componentes.crud.ComandosCRUDEvent;
    import br.com.growupge.flex.model.SigemModelLocator;
    
    import mx.collections.ArrayCollection;
    import mx.controls.DataGrid;
    import mx.core.Application;
    import mx.rpc.events.ResultEvent;
    
	
	public class BuscarListaDestinoCommand extends BaseCommand {
		private var adminModel : AdminModelLocator = AdminModelLocator.getInstance();   
		private var model : SigemModelLocator = SigemModelLocator.getInstance();	
		   
		public override function result( data : Object ) : void {
			super.result(data);							
			var event:ResultEvent = data as ResultEvent;
	        //AMF0 DataGrid(this.resultado).dataProvider = new ArrayCollection(data as Array);		
	        DataGrid(this.resultado).dataProvider = event.result as ArrayCollection;		
			//var e:ComandosCRUDEvent = new ComandosCRUDEvent(ComandosCRUDEvent.RESULTADO,true, true);				
			//Application.application.dispatchEvent(e);

		}
		
	}
}
package br.com.growupge.flex.admin.command
{
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.componentes.crud.ComandosCRUDEvent;
    import br.com.growupge.flex.model.SigemModelLocator;
    
    import mx.core.Application;
    import mx.rpc.events.ResultEvent;
    import mx.controls.DataGrid;
    import mx.collections.ArrayCollection;
    import br.com.growupge.flex.componentes.crud.DataGridCRUD;
    import br.com.growupge.flex.valueobject.MensagemRetornoDTO;
    
	public class AdicionarCRUDRelacionamentoCommand extends BaseCommand {
	    private var model : SigemModelLocator = SigemModelLocator.getInstance();	   
		   
		public override function result( data : Object ) : void {		
			super.result(data);	
			var dto:MensagemRetornoDTO = (data as ResultEvent).result as MensagemRetornoDTO;
			
			var dataGrid:DataGridCRUD = DataGridCRUD(this.resultado);
			var dataProvider:ArrayCollection = ArrayCollection(this.resultado.dataProvider);
            
            var sortFieldName:String = dataGrid.sortFieldName;
            var posicao:int;
            var item:*;
            
            try {
                if (dto[sortFieldName] <= dataProvider.length){
                    for each ( item in dataProvider){
                        if ( item[sortFieldName] == dto[sortFieldName]) {
                            posicao = dataProvider.getItemIndex(item);
                            break;
                        }
                    } 
    
                    for (var i:int = posicao; i < dataProvider.length; i++) {
                        item = dataProvider.getItemAt(i);
                        item[sortFieldName] = item[sortFieldName] + 1;
                    }
                }
            } catch (e:Error) {}
            
			dataProvider.addItem(dto);
			dataGrid.selectedIndex = dataProvider.getItemIndex(dto);
            dataGrid.maxVerticalScrollPosition = dataProvider.length;
			dataGrid.scrollToIndex(dataProvider.getItemIndex(dto));
                        
			model.mensagemRetorno = '';
			model.mensagemRetorno = dto.mensagemRetorno;
		}		
	
	}
}
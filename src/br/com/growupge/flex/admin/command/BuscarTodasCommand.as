package br.com.growupge.flex.admin.command
{
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.componentes.crud.ComandosCRUDEvent;
    
    import mx.collections.ArrayCollection;
    import mx.core.Application;
    import mx.rpc.events.ResultEvent;
    import br.com.growupge.flex.componentes.crud.DataGridCRUD;
    import br.com.growupge.flex.constantes.ImageLibrary;
    import br.com.growupge.flex.model.TradutorHelper;
    import mx.controls.Alert;
    
	
	public class BuscarTodasCommand extends BaseCommand {
        private var tradutor:TradutorHelper = TradutorHelper.getInstance();

		public override function result( data : Object ) : void {
			super.result(data);							
			var event:ResultEvent = data as ResultEvent;
			data = event.result;
			// Modo antigo
/* 			adminModel.dataProvider = new ArrayCollection(data as Array);
			var e:ComandosCRUDEvent = new ComandosCRUDEvent(ComandosCRUDEvent.RESULTADO,true, true);				
			Application.application.dispatchEvent(e);

 */
 			//Modo novo
 			if (this.resultado as DataGridCRUD) {
				DataGridCRUD(this.resultado).dataProvider = data as ArrayCollection;
	
	            // TODO Isso é por causa do bug do flex, se não trocar o valor ele nao atribui
				var tempTotalDeRegistros:uint = DataGridCRUD(this.resultado).totalDeRegistros;
				DataGridCRUD(this.resultado).totalDeRegistros = 0;
				DataGridCRUD(this.resultado).totalDeRegistros = tempTotalDeRegistros;
				if(data.length ==0){
				    Alert.show(tradutor.getTexto('INT-0006'),"",0,null,null,ImageLibrary.INFORMACAO_ICON_G);
				}
 			}

		}
		
	}
}
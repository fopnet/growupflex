package br.com.growupge.flex.admin.command
{
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.componentes.crud.DataGridCRUD;
    import br.com.growupge.flex.model.SigemModelLocator;
    
    import mx.collections.ArrayCollection;
    import mx.controls.DataGrid;
    import mx.core.Application;
    import mx.rpc.events.ResultEvent;
    import br.com.growupge.flex.componentes.crud.ComandosCRUDEvent;
    import br.com.growupge.flex.valueobject.MensagemRetornoDTO;
    
	
	public class ExcluirCRUDRelacionamentoCommand extends BaseCommand {
        private var model : SigemModelLocator = SigemModelLocator.getInstance();	
		   
		public override function result( data : Object ) : void {	
			super.result(data);	

			var dto:MensagemRetornoDTO = (data as ResultEvent).result as MensagemRetornoDTO;	
			model.mensagemRetorno = '';
			model.mensagemRetorno = dto.mensagemRetorno;
								
			var dataGrid:DataGridCRUD = DataGridCRUD(this.resultado);
			var dataProvider:ArrayCollection = ArrayCollection(this.resultado.dataProvider);
			var itemSeguinte : * ;
			
    		var sortFieldName:String = dataGrid.sortFieldName;
            var posicao:int;
            var item:*;
            
            var itemParaExcluir:* = dataProvider.getItemAt(this.indice);
            
            try {
                if (dto[sortFieldName] <= dataProvider.length){
                    for each ( item in dataProvider){
                        if ( item[sortFieldName] == data[sortFieldName]) {
                            posicao = dataProvider.getItemIndex(item)+1;
                            break;
                        }
                    } 
    
                    for (var i:int = posicao; i < dataProvider.length; i++) {
                        item = dataProvider.getItemAt(i);
                        item[sortFieldName] = item[sortFieldName] - 1;
                    }
                }			
    			
    			if ((this.indice + 1) < dataProvider.length) {
    			    itemSeguinte = dataProvider.getItemAt(this.indice + 1);
    			} else {
    			    itemSeguinte = dataProvider.getItemAt( (this.indice == 0)? this.indice : this.indice-1 );
    			}
            } catch (e:Error){}
            
			dataGrid.selectedIndex = dataProvider.getItemIndex(itemSeguinte) ;
			//dataProvider.removeItemAt(this.indice);
			dataProvider.removeItemAt(dataProvider.getItemIndex(itemParaExcluir));
			

			//?var e:ComandosCRUDEvent = new ComandosCRUDEvent(ComandosCRUDEvent.RESULTADO);				
			//?Application.application.dispatchEvent(e);
		}

	}
}
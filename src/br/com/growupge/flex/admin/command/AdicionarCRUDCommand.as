package br.com.growupge.flex.admin.command
{
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.model.SigemModelLocator;
    
    import mx.rpc.events.ResultEvent;
    import br.com.growupge.flex.componentes.crud.DataGridCRUD;
    import mx.collections.ArrayCollection;
    import br.com.growupge.flex.valueobject.MensagemRetornoDTO;
    
	public class AdicionarCRUDCommand extends BaseCommand {
	    private var model : SigemModelLocator = SigemModelLocator.getInstance();	   
		   
		public override function result( data : Object ) : void {
			super.result(data);			
			var dto:MensagemRetornoDTO = (data as ResultEvent).result as MensagemRetornoDTO;
			
			var dataGrid:DataGridCRUD = DataGridCRUD(this.resultado);
			var dataProvider:ArrayCollection ;

    	    dataProvider = ArrayCollection(dataGrid.dataProvider);
    	    dataProvider.addItem(dto);
            
			dataGrid.selectedIndex = dataProvider.getItemIndex(dto);
            dataGrid.maxVerticalScrollPosition = dataProvider.length;
			dataGrid.scrollToIndex(dataProvider.getItemIndex(dto));
			model.mensagemRetorno = dto.mensagemRetorno;
 		}		
	
	}
}
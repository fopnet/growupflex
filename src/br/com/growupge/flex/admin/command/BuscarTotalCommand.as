package br.com.growupge.flex.admin.command
{
    import mx.rpc.events.ResultEvent;
    
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.componentes.crud.DataGridCRUD;
    
	
	public class BuscarTotalCommand extends BaseCommand {
		
		public override function result( data : Object ) : void {
			super.result(data);
			var event:ResultEvent = data as ResultEvent;							
			// var event:ResultEvent = data as ResultEvent;
			DataGridCRUD(this.resultado).totalDeRegistros = Number(event.result) ;
		}
		
	}
}
package br.com.growupge.flex.admin.command
{
    import mx.collections.ArrayCollection;
    import mx.rpc.events.ResultEvent;
    
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.componentes.crud.DataGridCRUD;
    import br.com.growupge.flex.model.SigemModelLocator;
    import br.com.growupge.flex.valueobject.MensagemRetornoDTO;
    
	
	public class ExcluirCRUDCommand extends BaseCommand {
        private var model : SigemModelLocator = SigemModelLocator.getInstance();	
		   
		public override function result( data : Object ) : void {
			super.result(data);	
			var dto:MensagemRetornoDTO = (data as ResultEvent).result as MensagemRetornoDTO;						
			model.mensagemRetorno = dto.mensagemRetorno;
			
			/*
			var event:ResultEvent = data as ResultEvent;
			adminModel.dataProvider.removeItemAt(adminModel.indiceExcluirItemDataProvider);
			model.mensagemRetorno = data.mensagemRetorno;
			var e:ComandosCRUDEvent = new ComandosCRUDEvent(ComandosCRUDEvent.RESULTADO);				
			Application.application.dispatchEvent(e);
			*/
			
			var dataGrid:DataGridCRUD = DataGridCRUD(this.resultado);
			var dataProvider:ArrayCollection = ArrayCollection(this.resultado.dataProvider);
			var itemSeguinte : * ;
			
			if ((this.indice + 1) < dataProvider.length) {
			    itemSeguinte = dataProvider.getItemAt(this.indice + 1);
			} else {
			    itemSeguinte = dataProvider.getItemAt( (this.indice == 0)? this.indice : this.indice-1 );
			}

			dataGrid.selectedIndex = dataProvider.getItemIndex(itemSeguinte) ;
			dataProvider.removeItemAt(this.indice);
			dataGrid.totalDeRegistros = dataGrid.totalDeRegistros - 1;
		}

	}
}
package br.com.growupge.flex.admin.command
{
    import br.com.growupge.flex.command.BaseCommand;
    import br.com.growupge.flex.componentes.crud.DataGridCRUD;
    import br.com.growupge.flex.model.SigemModelLocator;
    
    import mx.collections.ArrayCollection;
    import mx.controls.DataGrid;
    import mx.rpc.events.ResultEvent;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    import br.com.growupge.flex.valueobject.MensagemRetornoDTO;
    
			
	public class AtualizarCRUDRelacionamentoCommand extends BaseCommand {
		private var model : SigemModelLocator = SigemModelLocator.getInstance();	
				
		public override function result( data : Object ) : void {
			super.result(data);							
			var dto:MensagemRetornoDTO = (data as ResultEvent).result as MensagemRetornoDTO;
			model.mensagemRetorno = '';
			model.mensagemRetorno = dto.mensagemRetorno;
            
            try {
                var posicaoAnterior:int = ExecutarComandoEvent(this.evento).indice +1;
                
                /* ArrayCollection(
                        DataGridCRUD(this.resultado).dataProvider)
                        .setItemAt(event.result, DataGrid(this.resultado).selectedIndex); */
                    
                    var dataProvider:ArrayCollection = ArrayCollection(DataGridCRUD(this.resultado).dataProvider);
                    var sortFieldName:String = DataGridCRUD(this.resultado).sortFieldName;
                    var itemAtual:* = dataProvider.getItemAt(DataGrid(this.resultado).selectedIndex);
                    
                    if (sortFieldName == 'ordem' && dto[sortFieldName]!=posicaoAnterior){            
                        if (dto[sortFieldName] < posicaoAnterior){
                            var itemAnterior:* = dataProvider.getItemAt(DataGrid(this.resultado).selectedIndex-1); 
                            itemAnterior[sortFieldName] = itemAnterior[sortFieldName] + 1;
                        }else{
                            var itemSeguinte:* = dataProvider.getItemAt(DataGrid(this.resultado).selectedIndex+1); 
                            itemSeguinte[sortFieldName] = itemSeguinte[sortFieldName] - 1;
                        }
                        dataProvider.removeItemAt(dataProvider.getItemIndex(itemAtual));
                        dataProvider.addItem(dto);
                        DataGridCRUD(this.resultado).selectedIndex = dataProvider.getItemIndex(dto);
                    } else {
                        ArrayCollection(
                            DataGridCRUD(this.resultado).dataProvider)
                            .setItemAt(dto, DataGrid(this.resultado).selectedIndex); 
                        DataGridCRUD(this.resultado).selectedIndex = 1;
                        DataGridCRUD(this.resultado).selectedIndex = dataProvider.getItemIndex(dto);
                    }

            } catch (e:Error) {}
		}
				
	}
}
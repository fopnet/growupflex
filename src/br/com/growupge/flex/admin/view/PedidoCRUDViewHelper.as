package br.com.growupge.flex.admin.view
{
    import br.com.growupge.flex.admin.event.BuscarTotalEvent;
    import br.com.growupge.flex.componentes.crud.CRUDViewHelper;
    import br.com.growupge.flex.componentes.crud.ComandosCRUDEvent;
    import br.com.growupge.flex.constantes.GCS;
    import br.com.growupge.flex.event.BaseEvent;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    import br.com.growupge.flex.valueobject.ConvertDTO;
    import br.com.growupge.flex.valueobject.PedidoVeiculoDTO;
    
    import com.adobe.cairngorm.control.CairngormEventDispatcher;

    public class PedidoCRUDViewHelper extends CRUDViewHelper
    {
		/* Permissoes Crud */
		public var permissaoImportar:String;
		
        public function PedidoCRUDViewHelper(dto:* = null)
        {
            super(dto);
        }
        
        public override function consultar(event:ComandosCRUDEvent):void {
            this.comando.permissao = permissaoBuscar;
            this.comando.parametrosLista.removeAll();    
            

            if (event.data != null) {          
	            //AMF0 this._dto = ConvertDTO.clone(event.data, this.instanciaDTO);
	            this._dto = ConvertDTO.cloneObject(event.data);
                comando.parametrosLista.addItem(this._dto);
            }
			
            if (event.pagina == 0){
                var eTotal : BuscarTotalEvent = new BuscarTotalEvent(mapaGCS);
                eTotal.resultadoDisplayObject = event.resultadoDisplayObject;
				eTotal.comando.parametrosLista.addAll( comando.parametrosLista );
                CairngormEventDispatcher.getInstance().dispatchEvent( eTotal );
            }

            // Ordem inicial para quantidade de registros retornados
            this.comando.parametrosLista.addItem( event.pagina );
            
			var e : ExecutarComandoEvent= new ExecutarComandoEvent(comando);
            e.resultadoDisplayObject = event.resultadoDisplayObject;
            CairngormEventDispatcher.getInstance().dispatchEvent( e );  
	                        
        }
		
		override public function editar(event:ComandosCRUDEvent):void{
			var paramDTO:PedidoVeiculoDTO = ConvertDTO.cloneObject(event.data) as PedidoVeiculoDTO;
			
			this.comando.permissao = GCS.PROCURAR_PEDIDO;
			this.comando.parametrosLista.removeAll();
			this.comando.parametrosLista.addItem(paramDTO);
			
			var e : ExecutarComandoEvent = new ExecutarComandoEvent(comando, ExecutarComandoEvent.PROCURAR);
			e.resultadoDisplayObject = event.resultadoDisplayObject;
			e.eventTargetResult = new ComandosCRUDEvent(ComandosCRUDEvent.RESULTADO_PROCURAR);
			CairngormEventDispatcher.getInstance().dispatchEvent( e );
			
		}
		
		override public function novo(event:ComandosCRUDEvent):void
		{
			this.comando.permissao = GCS.CRIAR_NIP;
			this.comando.parametrosLista.removeAll();
			
			var e : ExecutarComandoEvent = new ExecutarComandoEvent(comando, ExecutarComandoEvent.COMANDO_GENERICO);
			e.resultadoDisplayObject = event.resultadoDisplayObject;
			e.eventTargetResult = new ComandosCRUDEvent(ComandosCRUDEvent.RESULTADO_PROCURAR);
			CairngormEventDispatcher.getInstance().dispatchEvent( e );
		}
		
		
		public function importar(targetEvent:BaseEvent):void {
			var cmd:ComandoDTO  = new ComandoDTO();
			cmd.mapaGCS = super.mapaGCS;
			cmd.permissao = permissaoImportar;
			cmd.parametrosLista.addItem(dto);//pedidoVeiculoDTO do form
			cmd.parametrosLista.addItem(targetEvent.data.fileName);
			cmd.parametrosLista.addItem(targetEvent.data.attachmentName);
			
			var e:ExecutarComandoEvent = new ExecutarComandoEvent(cmd, ExecutarComandoEvent.COMANDO_GENERICO);
			e.resultadoDisplayObject = targetEvent.resultadoDisplayObject;
			e.eventTargetResult = targetEvent; 
			CairngormEventDispatcher.getInstance().dispatchEvent(e);	
			
		}
		
	}
}
package br.com.growupge.flex.admin.view
{
    import com.adobe.cairngorm.control.CairngormEventDispatcher;
    
    import br.com.growupge.flex.admin.event.BuscarTotalEvent;
    import br.com.growupge.flex.componentes.crud.CRUDViewHelper;
    import br.com.growupge.flex.componentes.crud.ComandosCRUDEvent;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    import br.com.growupge.flex.valueobject.Parameter;

    public class UsuarioCRUDViewHelper extends CRUDViewHelper
    {
        public function UsuarioCRUDViewHelper(dto:*=null)
        {
            super(dto);
        }
        
        public override function consultar(event:ComandosCRUDEvent):void {
            comando.permissao = permissaoBuscar;
            comando.parametrosLista.removeAll();    
            var parameters:Array = event.data as Array;
            
            if (parameters.length > 0) {
		        this.dto = this.instanciaDTO;
	            
	            for each(var param:Parameter in parameters) {
		            Parameter.setValor( this.dto, param );
	            }
            	
                comando.parametrosLista.addItem(this.dto);
            }
            
			if (event.pagina == 0){
                var eTotal : BuscarTotalEvent = new BuscarTotalEvent(mapaGCS);
				eTotal.comando.parametrosLista.addAll( comando.parametrosLista );
                eTotal.resultadoDisplayObject = event.resultadoDisplayObject;
                CairngormEventDispatcher.getInstance().dispatchEvent( eTotal );
            }
                        
            
			var e : ExecutarComandoEvent= new ExecutarComandoEvent(comando);
            e.resultadoDisplayObject = event.resultadoDisplayObject;
            CairngormEventDispatcher.getInstance().dispatchEvent( e );  
        }
    }
}
package br.com.growupge.flex.admin.view
{
	import br.com.growupge.flex.componentes.crud.CRUDViewHelper;
	import br.com.growupge.flex.componentes.crud.ComandosCRUDEvent;
	import br.com.growupge.flex.event.BaseEvent;
	import br.com.growupge.flex.event.ExecutarComandoEvent;
	import br.com.growupge.flex.valueobject.ComandoDTO;
	
	import com.adobe.cairngorm.control.CairngormEventDispatcher;
	
	public class VeiculoCrudViewHelper extends CRUDViewHelper
	{
		/* Permissoes Crud */
		public var permissaoImportar:String;
		
		public function VeiculoCrudViewHelper(dto:*=null) {
			super(dto);
		}
		
		public function importar(targetEvent:BaseEvent):void {
			var cmd:ComandoDTO  = new ComandoDTO();
			cmd.mapaGCS = super.mapaGCS;
			cmd.permissao = permissaoImportar;
			cmd.parametrosLista.addItem(dto);//veiculoDTO do form
			cmd.parametrosLista.addItem(targetEvent.data);

			var e:ExecutarComandoEvent = new ExecutarComandoEvent(cmd, ExecutarComandoEvent.COMANDO_GENERICO);
			e.resultadoDisplayObject = targetEvent.resultadoDisplayObject;
			e.eventTargetResult = targetEvent; 
			CairngormEventDispatcher.getInstance().dispatchEvent(e);	
			
		}
	}
}
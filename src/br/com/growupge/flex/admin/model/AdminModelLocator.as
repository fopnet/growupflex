package br.com.growupge.flex.admin.model
{
	import com.adobe.cairngorm.model.ModelLocator;
	import mx.collections.ArrayCollection;
	
	[Bindable]
	public class AdminModelLocator implements ModelLocator	{
		public static var instance: AdminModelLocator;
	
		public static function getInstance(): AdminModelLocator {
			if (instance == null) {
				instance = new AdminModelLocator();
			}
			return instance;
		}
		
		public function AdminModelLocator() {
			if (instance != null) {
				throw new Error("Somente pode haver uma instância de AdminModelLocator");
			}
		}
		
		public var dataProvider : ArrayCollection = new ArrayCollection();
		public var indiceExcluirItemDataProvider : int;
		
		public var mensagemRetorno:String = '';
		        
	}
}
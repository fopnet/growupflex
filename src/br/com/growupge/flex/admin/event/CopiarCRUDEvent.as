 package br.com.growupge.flex.admin.event
{
    import br.com.growupge.flex.constantes.GCS;
    import br.com.growupge.flex.event.BaseEvent;
    import br.com.growupge.flex.model.SigemModelLocator;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    
    import com.adobe.cairngorm.control.CairngormEvent;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
	
	public class CopiarCRUDEvent extends BaseEvent {
	        
		public function CopiarCRUDEvent ( comando : ComandoDTO) {
			super( ExecutarComandoEvent.EVENT_COPIAR);
			
			this.comando = comando;
			this.comando.SID = model.sessaoDTO.sid;
		}
	}	
}
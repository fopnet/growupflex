package br.com.growupge.flex.admin.event
{
    import flash.events.Event;
    import mx.core.Container;
    import mx.containers.ViewStack;

    public class ViewStackAdminEvent extends Event    {
        
        public static const SELECIONAR_VIEW:String = 'selecionarView';
        public static const OPEN_VIEW:String = 'openView';
        
        public var view:Container;
        public var viewStack:ViewStack;
        [Bindable]
        public var label:String;
        public var index:int;
        public var idView:String;
        
        public function ViewStackAdminEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false){
            super(type, bubbles, cancelable);
        }
        
    }
}
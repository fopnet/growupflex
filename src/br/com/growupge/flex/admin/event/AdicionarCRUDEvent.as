 package br.com.growupge.flex.admin.event
{
    import br.com.growupge.flex.event.BaseEvent;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    import br.com.growupge.flex.valueobject.ComandoDTO;
	
	public class AdicionarCRUDEvent extends BaseEvent {
	        
		public function AdicionarCRUDEvent ( comando : ComandoDTO) {
			super( ExecutarComandoEvent.EVENT_ADICIONAR );
			
			this.comando = comando;
			this.comando.SID = model.sessaoDTO.sid;
		}
	}	
}
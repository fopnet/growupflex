 package br.com.growupge.flex.admin.event
{
    import br.com.growupge.flex.event.BaseEvent;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    
  
	public class AtualizarCRUDEvent extends BaseEvent {
	
		public function AtualizarCRUDEvent ( comando : ComandoDTO) {
			super( ExecutarComandoEvent.EVENT_ATUALIZAR );
			this.comando = comando;
			this.comando.SID = model.sessaoDTO.sid;
		}
	}	
}
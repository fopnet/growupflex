 package br.com.growupge.flex.admin.event
{
    import br.com.growupge.flex.constantes.GCS;
    import br.com.growupge.flex.event.BaseEvent;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    
	public class BuscarTotalEvent extends BaseEvent {

		public function BuscarTotalEvent ( mapa : String) {
			super( ExecutarComandoEvent.BUSCAR_TOTAL );
			this.comando = new ComandoDTO();
			this.comando.mapaGCS = mapa;
			this.comando.SID = model.sessaoDTO.sid;
            this.comando.permissao = GCS.BUSCAR_TOTAL;
		}
	}	
}
package br.com.growupge.flex.validators
{
	import mx.validators.Validator;
	import br.com.growupge.flex.model.TradutorHelper;
	import mx.events.ValidationResultEvent;
	import mx.utils.StringUtil;
	import mx.validators.ValidationResult;

	public class CPFValidator extends Validator {
		
		[Bindable]
        private var tradutor:TradutorHelper = TradutorHelper.getInstance();  
             
        [Bindable]
        public var formatoMensagemErro:String;
                     
        public function CPFValidator() {
			super();
            loadTexts(new Event(Event.INIT));
            this.addEventListener(ValidationResultEvent.INVALID,loadTexts);
            this.addEventListener(ValidationResultEvent.VALID,loadTexts);
      	}
       
        private function loadTexts(event:Event):void{
            this.requiredFieldError = tradutor.getTexto('requiredFieldError');
            this.formatoMensagemErro= tradutor.getTexto('formatoCpfErro');
        }
       /**
       * Método utilizado para validação de CPF
       */
       	private static function validarCPF(validator:CPFValidator, 
       									   value:Object,
       									   subField : String = "") :Array { 
       			
		    var results:Array = [];
			var soma: int = 0;
			var formatoValido:Boolean = false;
			
			//CPF com caracter em branco, CPF inválido
			var cpf: String = value != null ? StringUtil.trim(String(value)) : "";
			
			cpf = cpf.replace(".", "").replace(".", "").replace(".", "").replace("-","");
			
			if ( !isNaN(Number(cpf)) && StringUtil.trim(cpf).length == 11 ) {
                var i:int;
                
				for ( i = 0; i < 9; i++) {
					soma += (10 - i) * (cpf.charCodeAt(i) - String("0").charCodeAt(0));					
				}				
				soma = 11 - (soma % 11);
				
				if (soma > 9) {
					soma = 0;
				}
				if (soma == (cpf.charCodeAt(9) - String("0").charCodeAt(0))) {
					soma = 0;
					for (i = 0; i < 10; i++) {
						soma += (11 - i) * (cpf.charCodeAt(i) - String("0").charCodeAt(0));
					}
					
					soma = 11 - (soma % 11);
					
					if (soma > 9) {
						soma = 0;
					}
					if (soma == (cpf.charCodeAt(10) - String("0").charCodeAt(0))) {
						formatoValido = true;
					}
				}
				
			}
			
            if( !formatoValido ){    
    		    results.push(new ValidationResult( true, subField, "formatoCpfErro", validator.formatoMensagemErro));
    			return results;  
			}
			return results;	
       }

       	override protected function doValidation(value:Object):Array {
			var results:Array = super.doValidation(value);
    		
    		var val:String = value ? String(value) : "";
    		if (results.length > 0 || ((val.length == 0) && !required)){
    			return results;
    		} else {
    		    return CPFValidator.validarCPF(this, value, null);
    		} 
			return results;
	   }
	}
}
package br.com.growupge.flex.validators
{
    import mx.validators.NumberValidator;
    import br.com.growupge.flex.model.TradutorHelper;
    import mx.events.ValidationResultEvent;

    public class NumberValidatorSigem extends NumberValidator
    {
    	
    	[Bindable]
        public var tradutor:TradutorHelper = TradutorHelper.getInstance();
    	
        public function NumberValidatorSigem() {
        	super();
        	loadTexts(new Event(Event.INIT));
        	this.addEventListener(ValidationResultEvent.INVALID,loadTexts);
            this.addEventListener(ValidationResultEvent.VALID,loadTexts);
        }
        
        private function loadTexts(event:Event):void {
        	this.lowerThanMinError = tradutor.getTexto('lowerThanMinError');
        	this.requiredFieldError = tradutor.getTexto('requiredFieldError');
        }
    }
}
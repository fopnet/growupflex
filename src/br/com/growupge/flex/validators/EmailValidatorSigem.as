package br.com.growupge.flex.validators
{
    import br.com.growupge.flex.model.TradutorHelper;
    import mx.validators.EmailValidator;


    public class EmailValidatorSigem extends EmailValidator
    {
        [Bindable]
        private var tradutor:TradutorHelper = TradutorHelper.getInstance();       
        public function EmailValidatorSigem() {
            super();
            //TODO Traduzir Mensagens
            this.missingAtSignError = tradutor.getTexto('missingAtSignError');
            this.requiredFieldError = tradutor.getTexto('requiredFieldError');
        }
        
    }
}
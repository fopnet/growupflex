package br.com.growupge.flex.validators
{
    import mx.validators.StringValidator;
    import br.com.growupge.flex.model.TradutorHelper;
    import mx.events.ValidationResultEvent;
    import mx.utils.StringUtil;
    import mx.validators.ValidationResult;


    public class AlgumNumeroValidator extends StringValidator
    {
        [Bindable]
        private var tradutor:TradutorHelper = TradutorHelper.getInstance();
		[Bindable]
        public var formatoMensagemErro:String;       
                 
        public function AlgumNumeroValidator() {
            super();
            loadTexts(new Event(Event.INIT));
            this.addEventListener(ValidationResultEvent.INVALID,loadTexts);
            this.addEventListener(ValidationResultEvent.VALID,loadTexts);            
        }
        
        private function loadTexts(event:Event):void{
            this.tooLongError = tradutor.getTexto('tooLongError');
            this.tooShortError = tradutor.getTexto('tooShortError');
            this.formatoMensagemErro = tradutor.getTexto('formatoAlgumNumeroErro');
        }
		override protected function doValidation(value:Object):Array {
			var results:Array = super.doValidation(value);
    		
    		var val:String = value ? String(value) : "";
    		if (results.length > 0 || ((val.length == 0) && !required)){
    			return results;
    		} else {
    		    return AlgumNumeroValidator.validarAlgumNumero(this, value, null);
    		} 
			return results;
	   }
	   
	   private static function validarAlgumNumero(validator:AlgumNumeroValidator, 
       									   value:Object,
       									   subField : String = "") :Array { 
       		var results:Array = [];	
			var pwd: String = value != null ? StringUtil.trim(String(value)) : "";
						
            if( pwd.length > 0 && pwd..match(/[0-9]+/) == null ){    
    		    results.push(new ValidationResult( true, subField, "formatoAlgumNumeroErro", validator.formatoMensagemErro));
    			return results;  
			}
			return results;	
       }               
        
    }
}
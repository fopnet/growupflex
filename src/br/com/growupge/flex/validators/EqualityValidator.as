package br.com.growupge.flex.validators
{
    import br.com.growupge.flex.model.TradutorHelper;
    
    import flash.display.DisplayObject;
    import flash.events.Event;
    
    import mx.events.ValidationResultEvent;
    import mx.validators.ValidationResult;
    import mx.validators.Validator;

    public class EqualityValidator extends Validator
    {
        public var target : DisplayObject;
        private var results:Array;
        private var tradutor : TradutorHelper = TradutorHelper.getInstance();
        public var notEqual :String = '';
        
        public function EqualityValidator() {
            super();
            loadTexts(new Event(Event.INIT));
            this.addEventListener(ValidationResultEvent.INVALID,loadTexts);
            this.addEventListener(ValidationResultEvent.VALID,loadTexts);
        }
        private function loadTexts(event:Event):void{
            this.requiredFieldError = this.requiredFieldError = tradutor.getTexto('requiredFieldError');
            this.notEqual = tradutor.getTexto('INT-0011');
        }
        override protected function doValidation(value:Object):Array {
            results = new Array();
            results = super.doValidation(value);
            
            if (results.length > 0 || ((value.length == 0) && !required)) {
            	return results;
            } else if (source[property] != target[property]){
                results.push( new ValidationResult(true, 'target', 'notEqual',tradutor.getTexto('INT-0011') ));
            }
            return results;
        }
        
    }
}
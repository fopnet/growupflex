package br.com.growupge.flex.validators
{
    import flash.events.Event;
    
    import mx.events.ValidationResultEvent;
    import mx.validators.StringValidator;
    
    import br.com.growupge.flex.model.TradutorHelper;


    public class StringValidatorSigem extends StringValidator
    {
        [Bindable]
        private var tradutor:TradutorHelper = TradutorHelper.getInstance();       
        public function StringValidatorSigem() {
            super();
            loadTexts(new Event(Event.INIT));
            this.addEventListener(ValidationResultEvent.INVALID,loadTexts);
            this.addEventListener(ValidationResultEvent.VALID,loadTexts);            
        }
        
        private function loadTexts(event:Event):void{
            this.requiredFieldError = tradutor.getTexto('requiredFieldError');
            this.tooLongError = tradutor.getTexto('tooLongError');
            this.tooShortError = tradutor.getTexto('tooShortError');
        }        
        
    }
}
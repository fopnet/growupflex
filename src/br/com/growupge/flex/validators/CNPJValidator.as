package br.com.growupge.flex.validators
{
	import br.com.growupge.flex.model.TradutorHelper;
	
	import mx.charts.chartClasses.ChartBase;
	import mx.events.ValidationResultEvent;
	import mx.utils.StringUtil;
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	public class CNPJValidator extends 	Validator{
		[Bindable]
        private var tradutor:TradutorHelper = TradutorHelper.getInstance();  
             
        [Bindable]
        public var formatoMensagemErro:String;
                     
        public function CNPJValidator() {
			super();
            loadTexts(new Event(Event.INIT));
            this.addEventListener(ValidationResultEvent.INVALID,loadTexts);
            this.addEventListener(ValidationResultEvent.VALID,loadTexts);
      	}
       
        private function loadTexts(event:Event):void{
            this.requiredFieldError = tradutor.getTexto('requiredFieldError');
            this.formatoMensagemErro= tradutor.getTexto('formatoCnpjErro');
        }
       /**
       * Método utilizado para validação de CNPJ
       */
       	private static function validarCNPJ(validator: CNPJValidator, 
       								value:Object,
       								subField : String = "") :Array { 
       			
		    var results:Array = [];
			var soma: int = 0;
			var formatoValido:Boolean = false;
			//CNPJ com caracter em branco, cnpj inválido
			var cnpj: String = value != null ? StringUtil.trim(String(value)) : "";
			
			cnpj = cnpj.replace("/", "").replace("-", "").replace(".", "").replace(".","");
			
			if ( !isNaN(Number(cnpj)) && StringUtil.trim(cnpj).length == 14 ) {
                var i:int;
                var j:int;
				for ( i = 0, j = 5; i < 12; i++) {
					soma += j-- * (cnpj.charCodeAt(i) - String("0").charCodeAt(0));
					if (j < 2) {
						j = 9;
					}
				}
	
				soma = 11 - (soma % 11);
				if (soma > 9) {
					soma = 0;
				}
				if (soma == (cnpj.charCodeAt(12) - String("0").charCodeAt(0))) {
					soma = 0;
					for (i = 0, j = 6; i < 13; i++) {
						soma += j-- * (cnpj.charCodeAt(i) - String("0").charCodeAt(0));
						if (j < 2) {
							j = 9;
						}
					}
					soma = 11 - (soma % 11);
					if (soma > 9) {
						soma = 0;
					}
					if (soma == (cnpj.charCodeAt(13) - String("0").charCodeAt(0))) {
						formatoValido = true;
					}
				}
				
			}
			
            if( !formatoValido ){    
    		    results.push(new ValidationResult( true, subField, "formatoCnpjErro", validator.formatoMensagemErro));
    			return results;  
			}
			return results;	
       }

       	override protected function doValidation(value:Object):Array {
			var results:Array = super.doValidation(value);
    		
    		var val:String = value ? String(value) : "";
    		if (results.length > 0 || ((val.length == 0) && !required)){
    			return results;
    		} else {
    		    return CNPJValidator.validarCNPJ(this, value, null);
    		} 
			return results;
	   }
	}
}
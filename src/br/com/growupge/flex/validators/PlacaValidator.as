package br.com.growupge.flex.validators 
{	
    import br.com.growupge.flex.model.TradutorHelper;
    
    import mx.events.ValidationResultEvent;
    import mx.validators.ValidationResult;
    import mx.validators.Validator;
    import mx.utils.StringUtil;
	
	public class PlacaValidator extends Validator {
		[Bindable]
        private var tradutor:TradutorHelper = TradutorHelper.getInstance(); 
        
        [Bindable]
        public var formatoPlacaErro:String;
             
        public function PlacaValidator() {
             super();
             loadTexts(new Event(Event.INIT));
             this.addEventListener(ValidationResultEvent.INVALID,loadTexts);
             this.addEventListener(ValidationResultEvent.VALID,loadTexts);
        }
        
        private function loadTexts(event:Event):void{
            this.requiredFieldError = tradutor.getTexto('requiredFieldError');
            this.formatoPlacaErro= tradutor.getTexto('formatoPlacaErro');
        }     
        
		override protected function doValidation(value:Object):Array  {
	    	var results:Array = super.doValidation(value);
			
			var val:String = value ? String(value) : "";
			if (results.length > 0 || ((val.length == 0) && !required)) {
				return results;
	        } else {
			    return validatePlaca(this, value, null);
	        }
         }
         
         /**
		 * Método utilizado para validar o campo CEP.
		 **/
   		 public static function validatePlaca(validator:PlacaValidator,
										   value:Object,
										   baseField:String):Array
	    {
	        var results:Array = [];

			var placa:String = value != null ? StringUtil.trim(String(value)) : "";
			//placa = placa.replace("-","");

			var alfa:String = placa.substr(0,3);
			var numero:String = placa.substr(3,4);
			var pattern:RegExp = /^[A-Z]{3}-\d{4}$/;			
			if ( placa.match(pattern) == null) {
  			    results.push(new ValidationResult(
    			true, baseField, "formatoPlacaErro", validator.formatoPlacaErro));
			}
            
			return results;						
	    }

	 }
}
package br.com.growupge.flex.validators 
{	
    import br.com.growupge.flex.model.TradutorHelper;
    
    import mx.events.ValidationResultEvent;
    import mx.validators.ValidationResult;
    import mx.validators.Validator;
    import mx.utils.StringUtil;
	
	public class CEPValidator extends Validator {
		[Bindable]
        private var tradutor:TradutorHelper = TradutorHelper.getInstance(); 
        
        [Bindable]
        public var formatoCEPErro:String;
             
        public function CEPValidator() {
             super();
             loadTexts(new Event(Event.INIT));
             this.addEventListener(ValidationResultEvent.INVALID,loadTexts);
             this.addEventListener(ValidationResultEvent.VALID,loadTexts);
        }
        
        private function loadTexts(event:Event):void{
            this.requiredFieldError = tradutor.getTexto('requiredFieldError');
            this.formatoCEPErro= tradutor.getTexto('formatoCEPErro');
        }     
        
		override protected function doValidation(value:Object):Array  {
	    	var results:Array = super.doValidation(value);
			
			var val:String = value ? String(value) : "";
			if (results.length > 0 || ((val.length == 0) && !required)) {
				return results;
	        } else {
			    return validateZipCode(this, value, null);
	        }
         }
         
         /**
		 * Método utilizado para validar o campo CEP.
		 **/
   		 public static function validateZipCode(validator:CEPValidator,
										   value:Object,
										   baseField:String):Array
	    {
	        var results:Array = [];

			var cep:String = value != null ? StringUtil.trim(String(value)) : "";
			
			cep = cep.replace("-","");
			
			if ( !isNaN(Number(cep)) && StringUtil.trim(cep).length == 8 ) {
				cep = cep;
			} else {
  			    results.push(new ValidationResult(
    			true, baseField, "formatoCEPErro",	validator.formatoCEPErro));
			}
            
			return results;						
	    }

	 }
}
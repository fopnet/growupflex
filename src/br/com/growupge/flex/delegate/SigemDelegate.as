package br.com.growupge.flex.delegate
{
	import com.adobe.cairngorm.business.ServiceLocator;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.remoting.RemoteObject;
	
	import br.com.growupge.flex.valueobject.ComandoDTO;
	
	public class SigemDelegate {
		private var service : RemoteObject ; ///RemotingConnection = RemotingConnection.getInstance();
		private var responder : IResponder; //Responder
				
		public function SigemDelegate ( responder : IResponder ) {
			this.service = ServiceLocator.getInstance().getRemoteObject( "GrowUpFacade" );
			this.responder = responder;
		}
		
		/**
		 * Executa comandos remotos
		 * */
/* 		public function executarComando( comando : ComandoDTO ): void {
			comando.set_Parametros(comando.parametrosLista.toArray());
			this.service.call( Constantes.FACADE_COMMAND, responder , comando );
		}
		 */
		/**
		 * Executa comandos remotos
		 * */
		public function executarComando( comando : ComandoDTO ): void {
			var token : AsyncToken = service.executarComando( comando );
			token.addResponder( responder );
		}
		
	}
}

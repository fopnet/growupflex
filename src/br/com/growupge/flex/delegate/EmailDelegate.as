package br.com.growupge.flex.delegate
{
	import com.adobe.cairngorm.business.ServiceLocator;
	import mx.rpc.AsyncToken;
	import br.com.growupge.flex.valueobject.EmailDTO;
	import mx.rpc.IResponder;
	
	public class EmailDelegate {
		private var service : Object;//RemotingConnection = RemotingConnection.getInstance();
		private var responder : IResponder;
				
		public function EmailDelegate ( responder : IResponder ) {
			this.service = ServiceLocator.getInstance().getRemoteObject( "GrowUpMail" );
			this.responder = responder;
		}
		
		/**
		 * Executa comandos remotos
		 * */
		public function enviarEmail( email : EmailDTO ): void {
			//this.service.call( Constantes.FACADE_EMAIL_COMMAND, responder , email );
			var token : AsyncToken = service.enviarEmail(email);
			token.addResponder( responder );
		}
	}
}

package br.com.growupge.flex.delegate
{
	import com.adobe.cairngorm.business.ServiceLocator;
	import mx.rpc.IResponder;
	import mx.rpc.AsyncToken;
	import br.com.growupge.flex.constantes.Constantes;
	
	
	public class TradutorDelegate {
		private var responder : IResponder;
		private var service : Object;
				
		public function TradutorDelegate( responder : IResponder ) {
			this.service = ServiceLocator.getInstance().getRemoteObject( "GrowUpTradutor" );
			this.responder = responder;
		}
		
		/**
		 * Solicita Tradutor
		 * */
		public function getTradutor(chave:String): void {
 			var token : AsyncToken = service.getInstance(chave);
			token.addResponder( responder );
			//this.service.call( Constantes.TRADUTOR_COMMAND , responder , chave );
		}
		
		
	}
}

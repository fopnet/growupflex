package br.com.growupge.flex.componentes.filter
{
	import flash.events.EventDispatcher;
	import br.com.growupge.flex.valueobject.ConvertDTO;
	import br.com.growupge.flex.valueobject.ComandoDTO;
	import br.com.growupge.flex.event.ExecutarComandoEvent;
	import com.adobe.cairngorm.control.CairngormEventDispatcher;
	import mx.collections.ArrayCollection;
	import br.com.growupge.flex.valueobject.Parameter;
	import br.com.growupge.flex.event.BaseEvent;
	import br.com.growupge.flex.constantes.GCS;
	import mx.core.UIComponent;
	

	public class FiltroViewHelper extends EventDispatcher implements IFiltroViewHelper
	{
		protected var instanciaDTO : *;
		public var mapaGCS:String ;
		public var resultadoDisplayObject:UIComponent;
		
		public function FiltroViewHelper(dto:* = null){
            this.instanciaDTO = dto; 
        }
		public function aplicarFiltro(event:BaseEvent):*
		{
			var parameters:ArrayCollection = event.data as ArrayCollection;

			if (parameters.length > 0) {
				var dto:* = ConvertDTO.cloneObject(this.instanciaDTO);
				
				for each(var param:Parameter in parameters) {
					Parameter.setValor( dto , param);
				}
			} else {
				return null;
			}
			
			return dto;
		}
		
		public function consultar(event:BaseEvent):void {
			var dto:* = this.aplicarFiltro(event);
			
		    var comando :ComandoDTO = new ComandoDTO();
		    comando.mapaGCS = this.mapaGCS;
		    comando.permissao = GCS.BUSCAR_TODAS;
		    if (dto != null) {
				comando.parametrosLista.addItem( dto );
		    }
			
			var e:ExecutarComandoEvent = new ExecutarComandoEvent(comando);
			e.resultadoDisplayObject = resultadoDisplayObject;
			//e.eventoTargetResult = new FilterEvent(FilterEvent.RESUTADO_FILTRO);
			CairngormEventDispatcher.getInstance().dispatchEvent(e);
			
		}
		
	}
}
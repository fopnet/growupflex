package br.com.growupge.flex.componentes.filter.event
{
	import br.com.growupge.flex.event.BaseEvent;

	public class FilterEvent extends BaseEvent
	{
		public static const FILTRO_CRIADO : String = "FILTRO_CRIADO";
		public static const RESUTADO_FILTRO: String = "FILTRO_RESULTADO";

		public function FilterEvent(event:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(event, bubbles, cancelable);
		}
		
	}
}
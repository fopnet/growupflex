package br.com.growupge.flex.componentes.filter
{
    import flash.events.Event;
    import mx.collections.ArrayCollection;
    
    public interface IFiltro {
        function getParameters():ArrayCollection;
    }
}


package br.com.growupge.flex.componentes.filter
{
    import flash.events.Event;
    import br.com.growupge.flex.event.BaseEvent;
    
    public interface IFiltroViewHelper
    {
        function aplicarFiltro(event:BaseEvent):*;
        function consultar(event:BaseEvent):void ;
    }
}


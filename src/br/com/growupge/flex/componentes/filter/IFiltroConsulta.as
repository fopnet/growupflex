package br.com.growupge.flex.componentes.filter
{
    import flash.events.Event;
    
    public interface IFiltroConsulta
    {
        function consultar(event:Event):void ;
        function novaConsulta(event:Event):void ;
    }
}
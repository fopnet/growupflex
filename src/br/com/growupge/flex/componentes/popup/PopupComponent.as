package br.com.growupge.flex.componentes.popup
{
	import mx.containers.TitleWindow;
	import mx.managers.PopUpManager;
	import mx.core.UIComponent;
	import flash.net.getClassByAlias;
	import flash.events.Event;
	import mx.events.CloseEvent;
	import flash.display.DisplayObjectContainer;
	import flash.utils.setTimeout;
	import mx.core.Application;
	
	
	[Event(name="close")]
	
	[DefaultProperty("componente")]
	public class PopupComponent extends UIComponent {
		
		private var window:TitleWindow = new TitleWindow();
		public var title:String;
		
		[Bindable]
		public var componente:UIComponent;
		
		public function PopupComponent() {
			this.window.addEventListener(CloseEvent.CLOSE, closePopup, false, 1, true);
			//this.addEventListener(CloseEvent.CLOSE, closePopup, false, 1, true);
		}
				
		public function openPopup(owner:DisplayObjectContainer = null):void {
			this.setStyles();
			this.window.title = title;
			this.window.showCloseButton = true;
			this.window.addChildAt(componente, 0);
			
			PopUpManager.addPopUp(this.window, owner == null ? Application.application as DisplayObjectContainer : owner, true);
			PopUpManager.centerPopUp(this.window);
			this.componente.addEventListener(CloseEvent.CLOSE, closePopup);
			this.componente.setFocus();
		}
		
		private function setStyles():void{
		    
			this.window.setStyle('paddingTop',0);
			this.window.setStyle('paddingBottom',0);
			this.window.setStyle('paddingLeft',0);
			this.window.setStyle('paddingRight',0);
			this.window.setStyle('borderThicknessTop',0);
			this.window.setStyle('borderThicknessBottom',1);
			this.window.setStyle('borderThicknessLeft',1);
			this.window.setStyle('borderThicknessRight',1);

			this.componente.setStyle('borderColor',"#666666");
			this.componente.setStyle('color',"#000000");


			this.window.setStyle('shadowDistance',0);
			this.window.setStyle('dropShadowColor','#666666');
			this.window.setStyle('dropShadowColor','#666666');
			
 			this.window.setStyle('modalTransparency', .2);
			this.window.setStyle('modalTransparencyBlur', 0);
			this.window.setStyle('modalTransparencyColor','#000000');
			this.window.setStyle('modalTransparencyDuration',500); 
		}		
		
		public function closePopup(event:CloseEvent):void {
			PopUpManager.removePopUp(this.window);
			this.componente.removeEventListener(CloseEvent.CLOSE, closePopup);
			dispatchEvent(event);
			//this.window.removeEventListener(CloseEvent.CLOSE, closePopup);
		}	
		public function closePopupWithTimeout(delay:Number):void {
			setTimeout(close, delay);
		}		
		
		private function close():void {
			PopUpManager.removePopUp(this.window);
			this.window.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
		}	
	}
}
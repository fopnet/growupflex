package br.com.growupge.flex.componentes
{
        import mx.controls.Alert;
        import mx.managers.PopUpManager;
        import br.com.growupge.flex.componentes.debug.ErrorMessageWindow;
        import mx.core.Application;
        import flash.display.DisplayObject;
        import mx.rpc.events.FaultEvent;
        import mx.rpc.Fault;
        import mx.collections.errors.CursorError;
//        import mx.rpc.events.FaultEvent;
		

        public class MensagemErroSigem {        
			private static var instance:MensagemErroSigem;
	       	
	       	public function MensagemErroSigem() {
	        	if(instance != null) {
	        		throw new Error("Somente pode haver uma instância de MessagemErroSigem");
	        	}
	        }
	        
	        public static function getInstance():MensagemErroSigem {
	        	if(instance == null) {
	        		instance = new MensagemErroSigem();
	        	}
	        	
	        	return instance;
	        }
		        
                // tratar try catch para caso erro diferente de sigemException
                public function exibir(sigemException : Fault):void {
                     var exception : Object;
                     if (sigemException != null && sigemException.rootCause != null) {
                     	exception =  sigemException.rootCause;
                     } else {
                     	exception = sigemException;
                     }
/*
                        Alert.show( sigemException.mensagem + "\n\n\n"+
			            sigemException.acao + "\n\n\n" +
			            sigemException.mensagemOriginal + "\n\n\n", "Erro : " + sigemException.codigo );
 //                    Alert.show( sigemException.details, sigemException.level );
*/ 
					 var window:ErrorMessageWindow = new ErrorMessageWindow();
				     
					 if (!exception.hasOwnProperty('code')) {
					     window.title = sigemException.faultCode ;
					     window.mensagem = sigemException.faultString;
					     window.printStack = sigemException.faultDetail;
           			 } else { 	
				          window.title = exception.code ;
				          window.mensagem = exception.description;
				          window.printStack = exception.mensagemOriginal;
				          //window.printStack = sigemException.type == 'br.com.growupge.exception.GrowUpException' ? '' : exception.details;
               		}
               		
		            var  display : DisplayObject = Application.application.getChildByName('appView');
					if (display == null)
						display = Application.application.getChildByName('appHeader');// Vindo da tela de veiculos
					
		            PopUpManager.addPopUp(window, display , true);
                    PopUpManager.centerPopUp(window); 
                }
        }
}
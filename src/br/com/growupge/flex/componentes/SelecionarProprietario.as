package br.com.growupge.flex.componentes
{
    import flash.events.Event;
    import flash.events.MouseEvent;
    import br.com.growupge.flex.componentes.crud.SelecionarCRUD;
    import br.com.growupge.flex.valueobject.ProprietarioDTO;
    
    public class SelecionarProprietario extends SelecionarCRUD
    {
        public function SelecionarProprietario() {
            super();
        }
        
     	protected override function abrirSelecionar(event:MouseEvent):void {
            this.dto = new ProprietarioDTO();
            this.selecionarView = new SelecionarProprietarioView();
            super.abrirSelecionar(event);
        }        
        
    }
}
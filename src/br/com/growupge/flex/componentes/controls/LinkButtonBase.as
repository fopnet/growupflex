package br.com.growupge.flex.componentes.controls
{
    import flash.display.DisplayObject;
    
    import mx.controls.LinkButton;
    import mx.events.FlexEvent;
    import mx.controls.Button;
    import mx.core.UIComponent;
    import flash.filters.ColorMatrixFilter;

    public class LinkButtonBase extends LinkButton {
        public function LinkButtonBase() {
            super();
        }
        
        public override function set enabled(value:Boolean):void {
            if(!value){
                this.alpha = .4;
            }else {
                this.alpha = 1;
            }
            super.enabled = value;
            super.useHandCursor = value;
        }
    }
}
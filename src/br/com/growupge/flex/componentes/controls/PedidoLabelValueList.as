package br.com.growupge.flex.componentes.controls
{
	import br.com.growupge.flex.constantes.ImageLibrary;
	import br.com.growupge.flex.model.TradutorHelper;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.LinkButton;

	[Event(name="onTitleButtonClick", type="flash.events.Event")] 
	
	public class PedidoLabelValueList extends LabelValueList
	{
		private var myButton:LinkButton;
		
		public function PedidoLabelValueList()
		{
			super();
		}
		
		/**
		 * Creates the button and adds it to the titlebar
		 */
		override protected function createChildren():void 
		{
			super.createChildren();
			
			super.title = "";
			this.myButton = new LinkButton();
			this.myButton.label = tradutor.getTexto('IMPORTAR_ANEXOS');
			this.myButton.toolTip = tradutor.getTexto('IMPORTAR_ANEXOS');
			this.myButton.setStyle("icon", ImageLibrary.ADICIONAR_ICON);
			this.myButton.addEventListener(MouseEvent.CLICK, onButtonClick);
			this.titleBar.addChild(this.myButton);
		}
		
		/**
		 * The default handler for the <code>MouseEvent.CLICK</code> event.
		 */
		protected function onButtonClick(event:Event):void {
			var evt:Event = new MouseEvent('onTitleButtonClick', event.bubbles, event.cancelable);
			dispatchEvent(evt);		
		}
		
		/**
		 * Sizes and positions the button on the titlebar
		 */
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			this.myButton.setActualSize(myButton.getExplicitOrMeasuredWidth(),
				myButton.getExplicitOrMeasuredHeight());
			
			// Position the button 
			var y:int = 2;
			var x:int = super.x + getStyle("borderThicknessLeft"); /* (width / 2) - (myButton.width /2) ;//no meio do grid */
			this.myButton.move(x, y);
		}  
	}
}
package br.com.growupge.flex.componentes.controls.fileUploaderClasses
{
	import flash.net.URLRequest;
	
	public class UploadedFileStateHandler extends FileStateHandler
	{
		private var _name:String;
		private var _size:uint;
		
		public function UploadedFileStateHandler(name:String, size:Number) {
			super(null);
			_status = FileStateHandler.COMPLETED;
			this._name = name;
			this._size = size;
		}
		
		override public function cancel():void
		{
			// do nothing
		}
		
		override public function upload(request:URLRequest, overwriteIfExists:Boolean=false):void
		{
			// do nothing
		}
		
		override public function get name():String
		{
			return _name;
		}
		
		override public function get size():uint
		{
			return _size;
		}
		
		override public function get status():String
		{
			return super.status;
		}
		
		
	}
}
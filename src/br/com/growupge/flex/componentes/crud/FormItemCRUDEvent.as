package br.com.growupge.flex.componentes.crud
{
    import flash.events.Event;

    public class FormItemCRUDEvent extends Event
    {
        public static const CHANGED :String = 'formItemCRUDChanged';
        public var data:Object;
        
        public function FormItemCRUDEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false){
            super(type, bubbles, cancelable);
        }
        
    }
}

package br.com.growupge.flex.componentes.crud
{
    import br.com.growupge.flex.admin.model.AdminModelLocator;
    import br.com.growupge.flex.constantes.GCS;
    import br.com.growupge.flex.model.TradutorHelper;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    
    import com.adobe.cairngorm.control.CairngormEventDispatcher;
    
    import flash.utils.ByteArray;
    
    import mx.controls.DataGrid;
    import br.com.growupge.flex.admin.command.BuscarListaOrigemCommand;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    import mx.controls.Alert;
    import br.com.growupge.flex.admin.command.BuscarListaDestinoCommand;
    import mx.collections.ArrayCollection;
    import br.com.growupge.flex.valueobject.ConvertDTO;

    
    [Bindable]
    public class CRUDRelacionamentoViewHelper implements IComandosCRUD
    {    
        public var adminModel : AdminModelLocator = AdminModelLocator.getInstance();
        public var tradutor : TradutorHelper= TradutorHelper.getInstance();
        
        public var _dto : * ;
        private var instanciaItemDTO : *;
        private var instanciaDTO: * ;
       
        /* Permissoes Crud */
        private var _mapaListaOrigemGCS:String;
        public var mapaListaDestinoGCS:String;
        public var permissaoExcluir:String;
        public var permissaoAdicionar:String;
        public var permissaoAtualizar:String;
        public var permissaoBuscarDestino:String; 
                
        /* Campos DTO Relacionamento*/
        public var campoDTOGrupo:String;
        public var campoDTOGrupoItem:String;
        public var campoDTOOrdem:String = 'ordem';

        public var origemDataGrid:DataGrid;

        public function set mapaListaOrigemGCS (mapa:String) :void {
            this._mapaListaOrigemGCS = mapa;
        }
        public function get mapaListaOrigemGCS () :String{
            return this._mapaListaOrigemGCS;
        }
        
        public function CRUDRelacionamentoViewHelper(dto:* = null, itemdto:* = null){
            this.instanciaItemDTO = itemdto; 
            this.instanciaDTO = dto; 
        }
	    public function set dto(dto :Object) : void {
	    	//AMF0 this._dto = ConvertDTO.clone(dto, ConvertDTO.cloneObject( this.instanciaItemDTO ));
	    	this._dto = ConvertDTO.cloneObject(dto);
	   	}
	   	public function get dto() : Object {
	   		return this._dto;
	   	}        
        
        public function consultar(event:ComandosCRUDEvent):void {
            var comando :ComandoDTO = new ComandoDTO();
            comando.mapaGCS = mapaListaDestinoGCS;
            comando.permissao = permissaoBuscarDestino;
            
            // Instancia do ItemDTO
            this._dto = ConvertDTO.cloneObject(this.instanciaItemDTO);
            //AMF0 this._dto[campoDTOGrupo] = ConvertDTO.clone( DataGrid(event.resultadoDisplayObject).selectedItem, ConvertDTO.cloneObject(this.instanciaDTO) );
            this._dto[campoDTOGrupo] = ConvertDTO.cloneObject( DataGrid(event.resultadoDisplayObject).selectedItem );
            
            comando.parametrosLista.addItem(_dto);

             // Ordem inicial para quantidade de registros retornados
           // comando.parametrosLista.addItem(0);
            
            var e : ExecutarComandoEvent = new ExecutarComandoEvent(comando, ExecutarComandoEvent.BUSCAR_LISTA_DESTINO);
            e.resultadoDisplayObject = CRUDRelacionamentoComponente(event.target).destino;
            CairngormEventDispatcher.getInstance().dispatchEvent( e );
        }
        
        public function consultarOrigem(event:ComandosCRUDEvent):void {
           var comando :ComandoDTO = new ComandoDTO();
           comando.mapaGCS = mapaListaOrigemGCS;
           comando.permissao = GCS.BUSCAR_TODAS;
           
           var e : ExecutarComandoEvent = new ExecutarComandoEvent(comando, ExecutarComandoEvent.BUSCAR_LISTA_ORIGEM);
           e.resultadoDisplayObject = event.resultadoDisplayObject;
           CairngormEventDispatcher.getInstance().dispatchEvent( e ) 
        }
         
        public function salvar(event:ComandosCRUDEvent):void {
            var comando :ComandoDTO = new ComandoDTO();
            comando.mapaGCS = mapaListaDestinoGCS;
            //data formulario
            if (event.data is FormCRUD){
                FormCRUD(event.data).preencheDTO();                
                this.dto = FormCRUD(event.data).dto ;
            }
            
            if (event.currentCRUDAction == ComandosCRUDEvent.EDITAR) {
                comando.permissao = permissaoAtualizar;
                comando.parametrosLista.removeAll();               
                comando.parametrosLista.addItem(dto);
                var e : ExecutarComandoEvent = new ExecutarComandoEvent(comando,ExecutarComandoEvent.ATUALIZAR_RELACIONAMENTO);
                e.resultadoDisplayObject = event.resultadoDisplayObject;
                e.indice = DataGrid(event.resultadoDisplayObject).selectedIndex;
                CairngormEventDispatcher.getInstance().dispatchEvent( e );
            }else {
                comando.permissao = permissaoAdicionar;
                comando.parametrosLista.removeAll();               
                comando.parametrosLista.addItem(dto);
                var e3 : ExecutarComandoEvent = new ExecutarComandoEvent(comando,ExecutarComandoEvent.ADICIONAR_RELACIONAMENTO);
                e3.resultadoDisplayObject = event.resultadoDisplayObject;
                CairngormEventDispatcher.getInstance().dispatchEvent( e3 );
            }
            
        } 
        
        public function excluir(event:ComandosCRUDEvent):void {
            var comando:ComandoDTO = new ComandoDTO();
            comando.mapaGCS = mapaListaDestinoGCS;
            comando.permissao = permissaoExcluir;

            var datagridItem:Object = DataGrid(event.target.destino).selectedItem;
            this.dto = DataGrid(event.target.destino).selectedItem;

            comando.parametrosLista.removeAll();
            comando.parametrosLista.addItem(_dto);
            //adminModel.indiceExcluirItemDataProvider = event.target.relacionamento.destino.dataProvider.getItemIndex(datagridItem);       
            var e : ExecutarComandoEvent= new ExecutarComandoEvent(comando, ExecutarComandoEvent.EXCLUIR_RELACIONAMENTO);
            e.indice = DataGrid(event.target.destino).dataProvider.getItemIndex(datagridItem);
            e.resultadoDisplayObject = event.resultadoDisplayObject;
            CairngormEventDispatcher.getInstance().dispatchEvent( e );  
            
        }
        
        public function novo(event:ComandosCRUDEvent):void{
//            var obj:* = ConvertDTO.cloneObject(this.instanciaItemDTO);
//            obj[campoDTOGrupo] = DataGrid(event.resultadoDisplayObject).selectedItem;
//            obj[campoDTOGrupoItem] = CRUDRelacionamentoComponente(event.target).origem.selectedItem;
			this._dto = ConvertDTO.cloneObject(this.instanciaItemDTO);
			//AMF0 this._dto[campoDTOGrupo] = ConvertDTO.clone(DataGrid(event.resultadoDisplayObject).selectedItem, _dto[campoDTOGrupo]);
			//AMF0 this._dto[campoDTOGrupoItem] = ConvertDTO.clone(CRUDRelacionamentoComponente(event.target).origem.selectedItem, _dto[campoDTOGrupoItem] ) ;
			this._dto[campoDTOGrupo] = DataGrid(event.resultadoDisplayObject).selectedItem;
			this._dto[campoDTOGrupoItem] = CRUDRelacionamentoComponente(event.target).origem.selectedItem ;
            var ordem : Number;
            
            if (event.target.destino.selectedItem == null){
                ordem = ArrayCollection(event.target.destino.dataProvider).length + 1;
            } else {
                ordem = DataGrid(event.target.destino).selectedIndex + 2;
            }
            
           	if (this._dto.hasOwnProperty(campoDTOOrdem)) {
                this._dto[campoDTOOrdem] = ordem;
            }
            // this.dto  = obj;
        }
        
        public function moverParaCima(event:ComandosCRUDEvent):void{
            this.dto = null;
            this.dto = ConvertDTO.cloneObject(event.data);
            var ordem : Number = event.data.ordem - 1 ;
            
            try {
                this.dto[campoDTOOrdem] = ordem;
            }catch (e:Error){}
            this.salvar(event);
        }
        public function moverParaBaixo(event:ComandosCRUDEvent):void{
            this.dto = null;
            this.dto = ConvertDTO.cloneObject(event.data);
            var ordem : Number = event.data.ordem + 1 ;
            
            try {
                this.dto[campoDTOOrdem] = ordem;
            }catch (e:Error){}
            this.salvar(event);
        }
        
        public function editar(event:ComandosCRUDEvent):void{
            this.dto = null;
            try {
                this.dto = event.data;
            } catch (e:Error){
                this.dto = event.data;
            }
        }
                
    }
}
package br.com.growupge.flex.componentes.crud
{
    import flash.display.DisplayObject;
    import flash.events.Event;
    
    import mx.containers.FormItem;
    import mx.controls.Spacer;
    import mx.core.Container;
    import mx.events.FlexEvent;

    public class FormItemCRUD extends FormItem implements IHabilitarCRUD
    {
        public var isVisibleOnEdit:Boolean = true;
        public var isEnabledOnEdit:Boolean = true;
        public var isEnabledOnInsert:Boolean = true;
        public static const VERTICAL_GAP:int = 3;
        
        private var verticalGapSpacer:Spacer = new Spacer() ;
        
        public function FormItemCRUD() {
            super();
            this.verticalGapSpacer.height = VERTICAL_GAP;
            this.addEventListener( FlexEvent.CREATION_COMPLETE, init );
        }
            
        private function propagarEventoChange(event:Event):void{
           var e:FormItemCRUDEvent = new FormItemCRUDEvent(FormItemCRUDEvent.CHANGED,true);
           e.data = event.target;
           dispatchEvent(e);
        }

        private function init(event:FlexEvent):void {
            //addSpacer
            event.currentTarget.addChild(this.verticalGapSpacer as DisplayObject);
            
            //add Event Change
			addChildrenListeners( this.getChildren() ) ;
        }
		
		private function addChildrenListeners(filhos:Array):void {
			for each ( var f:DisplayObject in filhos ) {
				try {
					if (f is Container)
						addChildrenListeners ( Container(f).getChildren() );
					else 
						f.addEventListener(Event.CHANGE, propagarEventoChange);
				} catch (e:Error) {}
			}
		}

        public override function set visible( visible:Boolean ):void {
            super.visible = visible;
            if ( !visible ){
                height = 0;
            } else {
                height = undefined;
            }
        }
   		
   		public function set habilitado ( value:Boolean ):void {
        	var filhos:Array = this.getChildren();
            for each ( var f:DisplayObject in filhos ) {
            	if ( f is IHabilitarCRUD) {
            		IFormItemCRUD(f).habilitado = value;
            	}
            }
        }
/*
        public function get isEnabledOnEdit():Boolean {
        	return _isEnabledOnEdit;
        }
         public function set isEnabledOnEdit(value:Boolean ):void {
        	this._isEnabledOnEdit = value;
        	var filhos:Array = this.getChildren();
            for each ( var f:DisplayObject in filhos ) {
            	if ( f is IHabilitarCRUD) {
            		IFormItemCRUD(f).habilitado = value;
            	}
            }
        }   */      
    }
}
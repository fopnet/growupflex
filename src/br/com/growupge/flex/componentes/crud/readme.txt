Roteiro para Utiliza��o do Componente CRUD-Sigem

1 - Criar um Componente mxml(panel) na pasta 
/Sigem4FlexNG/src/br/com/petrobras/sigem/flex/admin/view
seguindo um modelo existente, por exemplo, TemaCRUDView.mxml
Nomenclatura OBJ+FUNCAO+VIEW

2 - Instanciar um viewHelper, baseado em CRUDViewHelper
3 - Definir uma funcao init() no evento creationComplete do Componente
incluindo a iniciacao dos comandos do viewhelper, definindo mapaGCS e permissao
 * comandoConsultar
 * comandoAdicionar
 * comandoAtualizar
 * comandoExcluir

4 - Incluir o CrudComponente
 * consultaFiltro - crud:FormConsultarCRUD - definindo a propriedade colunas como colunas="{datagrid.columns}"
 * resultados - criar CompoenteDatagrid para o DTO, definir o dataProvider="{adminModel.dataProvider}"
 * formulario - criar o Form usando os componentes criados para o Sigem
 	 * crud:FormCRUD
 	 * crud:FormItemCRUD
 	 * crud:TextCRUD
 	 * crud:TextInputCRUD
 	 * etc

5 - O Componente de DataGrid dever� estar em 
/Sigem4FlexNG/src/br/com/petrobras/sigem/flex/componentes
Nomenclatura DataGrid+OBJ, DataGridTema

6- Para visualizarem no sistema, incluam uma linha no arquivo MenuAdministracaoView
em /Sigem4FlexNG/src/br/com/petrobras/sigem/flex/admin/view
	package br.com.growupge.flex.componentes.crud
{
    import com.adobe.cairngorm.control.CairngormEventDispatcher;
    
    import mx.collections.ArrayCollection;
    import mx.controls.DataGrid;
    
    import br.com.growupge.flex.admin.event.AdicionarCRUDEvent;
    import br.com.growupge.flex.admin.event.AtualizarCRUDEvent;
    import br.com.growupge.flex.admin.event.BuscarTotalEvent;
    import br.com.growupge.flex.admin.event.CopiarCRUDEvent;
    import br.com.growupge.flex.admin.event.ExcluirCRUDEvent;
    import br.com.growupge.flex.constantes.GCS;
    import br.com.growupge.flex.event.ExecutarComandoEvent;
    import br.com.growupge.flex.model.TradutorHelper;
    import br.com.growupge.flex.valueobject.ComandoDTO;
    import br.com.growupge.flex.valueobject.ConvertDTO;
    import br.com.growupge.flex.valueobject.Parameter;

    
    [Bindable]
    public class CRUDViewHelper implements IComandosCRUD
    {    
        
        public var tradutor : TradutorHelper= TradutorHelper.getInstance();
        
        public var _dto : * ;
		internal var dtoAntesAlteracao : * ;
        protected var instanciaDTO : *;
        protected var comando :ComandoDTO ;
        
        /* Permissoes Crud */
        public var permissaoExcluir:String;
        public var permissaoAdicionar:String;
        public var permissaoAtualizar:String;
        public var permissaoBuscar:String;
        public var permissaoCopiar:String;
        
        
        public function CRUDViewHelper(dto:* = null){
        	this.comando = new ComandoDTO();
            this.instanciaDTO = dto; 
        }
        
        public function set mapaGCS(value :String):void {
        	this.comando.mapaGCS = value;
        }
        public function get mapaGCS():String {
        	return this.comando.mapaGCS;
        }
        
	    public function set dto(dto :Object) : void {
	    	//AMF0 this._dto = ConvertDTO.clone(dto, ConvertDTO.cloneObject( this.instanciaDTO ));
	    	this._dto = ConvertDTO.cloneObject(dto);
	   	}
	   	public function get dto() : Object {
	   		return this._dto;
	   	}
        
        public function consultar(event:ComandosCRUDEvent):void {
            this.comando.permissao = permissaoBuscar;
            comando.parametrosLista.removeAll();     

            this._dto = ConvertDTO.cloneObject(this.instanciaDTO);

            var isParametrized:Boolean = false;
            for each(var param:Parameter in event.data as ArrayCollection) {
	            if (this._dto.hasOwnProperty( param.atributo )) {
	            	isParametrized = true;
	            	Parameter.setValor( _dto, param ) ;
	            }
            }
            
            if (isParametrized) {          
                comando.parametrosLista.addItem(this._dto);
            }

            if (event.pagina == 0){
                var eTotal : BuscarTotalEvent = new BuscarTotalEvent(mapaGCS);
				eTotal.comando.parametrosLista.addAll( comando.parametrosLista );
                eTotal.resultadoDisplayObject = event.resultadoDisplayObject;
                CairngormEventDispatcher.getInstance().dispatchEvent( eTotal );
            }
            
            // Ordem inicial para quantidade de registros retornados
            this.comando.parametrosLista.addItem( event.pagina );
            
			var e : ExecutarComandoEvent= new ExecutarComandoEvent(comando);
            e.resultadoDisplayObject = event.resultadoDisplayObject;
            CairngormEventDispatcher.getInstance().dispatchEvent( e );  
        }
         
        public function salvar(event:ComandosCRUDEvent):void {
            FormCRUD(event.data).preencheDTO();   
            this.dto = FormCRUD(event.data).dto ;
            
            switch (event.currentCRUDAction) {
                case ComandosCRUDEvent.EDITAR :
                    this.comando.permissao = permissaoAtualizar;
                    this.comando.parametrosLista.removeAll();               
                    this.comando.parametrosLista.addItem(dto);
                    var e : AtualizarCRUDEvent = new AtualizarCRUDEvent(this.comando);
                    e.resultadoDisplayObject = event.resultadoDisplayObject;
                    CairngormEventDispatcher.getInstance().dispatchEvent( e );
                    break;    
                case ComandosCRUDEvent.NOVO_ITEM :
                    this.comando.permissao = permissaoAdicionar;
                    this.comando.parametrosLista.removeAll();               
                    this.comando.parametrosLista.addItem(dto);
                    var e2 : AdicionarCRUDEvent = new AdicionarCRUDEvent(this.comando);
                    e2.resultadoDisplayObject = event.resultadoDisplayObject;
                    CairngormEventDispatcher.getInstance().dispatchEvent( e2 );
                    break;    
                case ComandosCRUDEvent.COPIAR :
                    this.comando.permissao = permissaoCopiar;
                    this.comando.parametrosLista.removeAll();               
                    this.comando.parametrosLista.addItem(dto);
                    var e3 : CopiarCRUDEvent = new CopiarCRUDEvent(this.comando);
                    e3.resultadoDisplayObject = event.resultadoDisplayObject;
                    CairngormEventDispatcher.getInstance().dispatchEvent( e3 );
                    break;    
            }
        } 
        
        public function excluir(event:ComandosCRUDEvent):void {
            comando.permissao = permissaoExcluir;
            
            var datagridItem:Object = event.data;
            this.dto = datagridItem;

            comando.parametrosLista.removeAll();
            comando.parametrosLista.addItem(dto);
            var e : ExcluirCRUDEvent= new ExcluirCRUDEvent(comando);
            e.resultadoDisplayObject = event.resultadoDisplayObject;
            e.indice = DataGrid(event.resultadoDisplayObject).selectedIndex;   
            CairngormEventDispatcher.getInstance().dispatchEvent( e );  
        }
        
        public function novo(event:ComandosCRUDEvent):void{
			this.dtoAntesAlteracao = null;
            this.dto = ConvertDTO.cloneObject(this.instanciaDTO);
			FormCRUD(event.data).preencheForm();
        }

        public function copiar(event:ComandosCRUDEvent):void{
            this.dto = ConvertDTO.cloneObject(event.data);
        }

        public function cancelar(event:ComandosCRUDEvent):void{
			if (dtoAntesAlteracao == null)
				return;
            try {
				var dtGrid:DataGridCRUD = event.resultadoDisplayObject as DataGridCRUD;  
				//var idx:int = dtGrid.dataProvider.getItemIndex(this.dto);
				dtGrid.dataProvider.setItemAt(this.dtoAntesAlteracao, dtGrid.selectedIndex);
				//dtGrid.selectedIndex = idx;
				dtGrid.reordenarDataGrid();
				
            } catch (e:Error) {}
			finally {
	            this.dto = this.dtoAntesAlteracao;
			}
        }

        public function editar(event:ComandosCRUDEvent):void{
            this.dtoAntesAlteracao = event.data;
            //this.dto = ConvertDTO.cloneObject( FormCRUD(event.data).dto );
			//FormCRUD(event.data).preencheForm();
			
            var paramDTO:* = ConvertDTO.cloneObject(dtoAntesAlteracao);
            //paramDTO.codigo = this.dtoAntesAlteracao.codigo;            
            
            this.comando.permissao = GCS.PROCURAR;
            this.comando.parametrosLista.removeAll();
            this.comando.parametrosLista.addItem(paramDTO);
            
			var e : ExecutarComandoEvent = new ExecutarComandoEvent(comando, ExecutarComandoEvent.PROCURAR);
            e.resultadoDisplayObject = event.resultadoDisplayObject;
            e.eventTargetResult = new ComandosCRUDEvent(ComandosCRUDEvent.RESULTADO_PROCURAR);
            CairngormEventDispatcher.getInstance().dispatchEvent( e );
           
        }

        public function novaConsulta(event:ComandosCRUDEvent):void{
            DataGrid(event.resultadoDisplayObject).dataProvider = new ArrayCollection();
            DataGridCRUD(event.resultadoDisplayObject).totalDeRegistros = 0;
        }
        
    }
}
package br.com.growupge.flex.componentes.crud
{
    import mx.controls.dataGridClasses.DataGridColumn;
    
    import br.com.growupge.flex.valueobject.Parameter;

    public class DataGridColumnCRUD extends DataGridColumn  {
        
        /*Define se coluna será permitida consulta ou busca */
        public var searchable:Boolean = true;
        /* Define o campo do DTO que deverá ser utilizado para consulta */
        public var dataSubField:String = null;
        /* Instancia do DTO que a coluna representa*/
        public var dto:Object = null;
        
        public function DataGridColumnCRUD(columnName:String=null) {
            super(columnName);
        }
		
		
		public function getLabel(item :Object, column: DataGridColumn): String {
			var dtField:String = dataField; 
			if (dataSubField)
				dtField += "." + dataSubField;
			
			var p:Parameter   = new Parameter(item, dtField);
			var o:Object = Parameter.getValor(p); 
			return o != null ? o.toString() : '';
		}
        
        
    }
}
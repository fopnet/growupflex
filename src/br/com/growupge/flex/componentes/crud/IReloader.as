package br.com.growupge.flex.componentes.crud
{
	import mx.events.CloseEvent;
	import br.com.growupge.flex.valueobject.Parameter;
	
    public interface IReloader {
        function reloader(parameter:Parameter):void;
    }
}
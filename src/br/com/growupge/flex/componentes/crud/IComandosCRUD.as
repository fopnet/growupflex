package br.com.growupge.flex.componentes.crud
{
    public interface IComandosCRUD
    {
        function salvar(event:ComandosCRUDEvent):void;
        function excluir(event:ComandosCRUDEvent):void;
        function consultar(event:ComandosCRUDEvent):void;
    }
}
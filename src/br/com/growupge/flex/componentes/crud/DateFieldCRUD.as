package br.com.growupge.flex.componentes.crud
{
    import br.com.growupge.flex.componentes.crud.IFormItemCRUD;
    import mx.controls.DateField;
    import mx.formatters.DateFormatter;

    public class DateFieldCRUD extends DateField implements IFormItemCRUD
    {
        
        public function DateFieldCRUD() {
            super();
        }
/* 	        
		protected var _isEnabledOnEdit:Boolean = true;
        public function set isEnabledOnEdit(value:Boolean ):void {
        	this._isEnabledOnEdit = value;
        }
        public function get isEnabledOnEdit():Boolean {
        	return this._isEnabledOnEdit;
        }        
 */        
        public function set valor(valor:*):void {
            this.selectedDate = valor as Date;
        }
        
        public function get valor():* {
            return this.selectedDate;
        }
        
        public override function set formatString(value:String):void {
            super.formatString = value;
            this.selectedDate = (this.selectedDate)? new Date(this.selectedDate) : this.selectedDate;
        }

		public function set habilitado(value:Boolean):void {
			this.enabled = value;	
		}       
		
		private var _campoDTO:String;
		public function get campoDTO():String {
			return this._campoDTO = (this._campoDTO == null)? this.id : this._campoDTO;
		}
		
		public function set campoDTO(value:String):void {
			this._campoDTO = value;
		}  
    }
}
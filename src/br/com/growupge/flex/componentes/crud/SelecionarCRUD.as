package br.com.growupge.flex.componentes.crud
{
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Button;
    import mx.core.Application;
    import mx.core.EdgeMetrics;
    import mx.core.IFlexDisplayObject;
    import mx.core.UIComponent;
    import mx.core.mx_internal;
    import mx.events.Event;
    import mx.managers.PopUpManager;
    import mx.styles.ISimpleStyleClient;
    import mx.utils.StringUtil;
    
    import br.com.growupge.flex.componentes.controls.LinkButtonBase;
    import br.com.growupge.flex.constantes.ImageLibrary;
    import br.com.growupge.flex.valueobject.Parameter;

    use namespace mx_internal;    
    
	[Event(name="valorChanged",type="mx.events.Event")]
	[Event(name="change")]
	
    public class SelecionarCRUD extends UIComponent implements IFormItemCRUD,IReloader
    {
        protected var _campoDTO:String;
        public var campoChave:String;
        public var displayObject:DisplayObject;
        public var selecionarView:ISelecionarView;
        public var selecionarViewHelper:ISelecionarViewHelper;
        public var byPassChangeEvent:Boolean = false;
        [Bindable]
        public var editable:Boolean = false;
        [Bindable]
        public var maxChars:int = 0;
        
        mx_internal var border:IFlexDisplayObject;
        
        public var dto:* ;
        protected var labelTxt:TextInputCRUD;
        protected var btn:Button;
        protected var btnCancel:LinkButtonBase;
        protected var _contextParameter:Parameter;
        
        [Bindable]
        public function get contextParameter():Parameter {
        	return this._contextParameter;
        }
        public function set contextParameter(value:Parameter):void {
        	this._contextParameter = value;
        }
        
        [Bindable]
        private var _habilitado:Boolean = true;
        [Bindable]
        public var dataField:String = 'nome';

        private var _labelFunction:Function;
        [Bindable("labelFunctionChanged")]        
        public function get labelFunction():Function {
            return _labelFunction;
        }
        public function set labelFunction(value:Function):void {
            _labelFunction = value;
            dispatchEvent(new Event("labelFunctionChanged"));
        }
        
        public function itemToLabel(data:Object):String {
            if (!data)
                return " ";
    
            if (labelFunction != null)
                return labelFunction(data, this);
    
            if (typeof(data) == "object" || typeof(data) == "xml")
            {
                try
                {
                    data = data[dataField];
                }
                catch(e:Error)
                {
                    data = null;
                }
            }
    
            if (data is String)
                return String(data);
    
            try
            {
                return data.toString();
            }
            catch(e:Error)
            {
            }
    
            return " ";
        }
        public function SelecionarCRUD() {
            super();
           
           this.focusEnabled = true;

            setStyle('borderStyle','solid');
            setStyle('borderThickness',1);
            setStyle("fontWeight","normal");
//			  setStyle("borderColor","#000000");
//            setStyle("color","#000000");
            
        }
        
        public function reloader(contextParameter:Parameter):void {
        	this.contextParameter = contextParameter;
        	
        	if(selecionarView !=null && selecionarView is IReloader) {
        	    IReloader(selecionarView).reloader(contextParameter);
        	}
        	if (this.selecionarViewHelper != null && Object(this.selecionarView).hasOwnProperty('viewHelper')) {
                this.selecionarView['viewHelper'] = this.selecionarViewHelper;
            }
        }
        override protected function createChildren():void {
            super.createChildren();
            
            if (!labelTxt) {
                labelTxt = new TextInputCRUD();
                labelTxt.editable = this.editable;
                labelTxt.text= '';
                labelTxt.height = 22;
                labelTxt.maxChars = this.maxChars;
                //this.labelTxt.addEventListener(MouseEvent.CLICK, abrirSelecionar);
                addChild(labelTxt);
                labelTxt.move(0,0);
            }
            if (!btn) {
                btn = new Button(); //MyModules.graphics.getIcon('SELECIONAR_ICON');
                this.btn.setStyle('icon', ImageLibrary.SELECIONAR_ICON);
                this.btn.height = this.labelTxt.height;   
                this.btn.width = this.labelTxt.width;   
            
                this.btn.addEventListener(MouseEvent.CLICK, abrirSelecionar);
                addChild(btn);
            }
            if (!btnCancel) {
                btnCancel = new LinkButtonBase();
                this.btnCancel.setStyle('icon', ImageLibrary.CANCELAR_ICON);
                this.btnCancel.setStyle('paddingLeft', 0);
                this.btnCancel.setStyle('paddingRight', 0);
                this.btnCancel.setStyle('paddingTop', 0);
                this.btnCancel.setStyle('paddingBottom', 0);
                
                //this.btnCancel.alpha = .7;
                this.btnCancel.height = this.labelTxt.height;   
                this.btnCancel.width = this.labelTxt.width;   
                this.btnCancel.visible = false;
                this.btnCancel.addEventListener(MouseEvent.CLICK, cancelarSelecionado);
                addChild(btnCancel);
            }
            if (!border) {
                var borderClass:Class = getStyle("borderSkin");
                if (borderClass) {
                    border = new borderClass();    
                    if (border is ISimpleStyleClient)
                        ISimpleStyleClient(border).styleName = this;
                    addChild(DisplayObject(border));
                }
            }
        }
        override protected function measure():void {
            super.measure();

            var buttonWidth:Number = labelTxt.getExplicitOrMeasuredHeight();
            var buttonHeight:Number = labelTxt.getExplicitOrMeasuredHeight();

            measuredWidth = measuredMinWidth = labelTxt.measuredWidth + buttonWidth;
            measuredHeight = measuredMinHeight = Math.max(labelTxt.measuredHeight,buttonHeight) + 10 ;
            //measuredHeight = measuredMinHeight = 22;
        }
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
            super.updateDisplayList(unscaledWidth, unscaledHeight);         

            var usableWidth:Number = unscaledWidth -10;
            var usableHeight:Number = unscaledHeight -10 ;
            var bm:EdgeMetrics;
            
            var textWidth:Number = usableWidth ;
            var textHeight:Number = usableHeight ;
            labelTxt.setActualSize(textWidth + textHeight, textHeight);
            
            var buttonWidth:Number = textHeight;
            var buttonHeight:Number = textHeight;

            btn.setActualSize(buttonWidth, buttonHeight);       
            btnCancel.setActualSize(16, 16);       
            
            if (border) {
                border.setActualSize(textWidth + textHeight, usableHeight);
            }
            
            btn.move(textWidth,0 );
            btnCancel.move(textWidth - buttonWidth +4, textHeight - btnCancel.height /2 - 11 );
        }
        /**
         *  @private
         */
        override public function styleChanged(styleProp:String):void {
            if (labelTxt)
                labelTxt.styleChanged(styleProp); 
            
            if (border && border is ISimpleStyleClient){
                ISimpleStyleClient(border).styleChanged(styleProp);
            }
    
            super.styleChanged(styleProp);
        }
        /**
         *  @private
         */
        override public function notifyStyleChangeInChildren(
                                    styleProp:String, recursive:Boolean):void {
            super.notifyStyleChangeInChildren(styleProp, recursive);
    
            if (labelTxt)
                labelTxt.notifyStyleChangeInChildren(styleProp, recursive); 
        }
        /**
        * Implementacao 
        * **/
/* 
        protected var _isEnabledOnEdit:Boolean = true;
        public function set isEnabledOnEdit(value:Boolean):void {            
            this._isEnabledOnEdit = value;
        }
        public function get isEnabledOnEdit():Boolean {
       		return _isEnabledOnEdit;
        }
 */    
        public function get text():String {
        	return this.labelTxt.text;
        }
        public function set text(value:String ):void {
       		this.labelTxt.valor = value;
        }
       
        public function set valor(valor:*):void {
            this.dto = valor;
            //this.text = (this.dto)? dto.nome : '';
            var str:String = itemToLabel(this.dto);
            this.text = null;
            if (str!=null){
                this.text = StringUtil.trim(str);
            }
           // if (!byPassChangeEvent){
                this.dispatchEvent(new Event(VALOR_CHANGED));
                this.dispatchEvent(new Event(Event.CHANGE));
           // }
            byPassChangeEvent = false;
            btnCancel.visible = (this.text!= '' || this.dto!=null)? true : false;
        } 
        [Bindable(event="change")]
        public function get valor():*{
            return this.dto;
        }
       
        protected function cancelarSelecionado(event:MouseEvent):void {
            this.valor = null;
        }
        protected function abrirSelecionar(event:MouseEvent):void {
            var selecionar:ISelecionarView = selecionarView;
            selecionar.target = this;
            
            var display:DisplayObject = (this.displayObject)? this.displayObject : (Application.application) as DisplayObject;
            PopUpManager.addPopUp(selecionar as IFlexDisplayObject, display  , true);
            PopUpManager.centerPopUp(selecionar as IFlexDisplayObject);

            if (selecionar is IReloader) {
            	IReloader(selecionar).reloader(this.contextParameter);
            }
            if (this.selecionarViewHelper != null && Object(this.selecionarView).hasOwnProperty('viewHelper')) {
                this.selecionarView['viewHelper'] = this.selecionarViewHelper;
            }
        }
        public function dispararEvento(event:String):void {
           	this.dispatchEvent(new Event(event));
        }
        public function set habilitado ( value:Boolean ):void {
       		//super.enabled = value;
       		//this.enabled = value;
       		this._habilitado = value;
       		
       		this.btn.enabled = value;
       		this.btnCancel.enabled = value;
       		invalidateDisplayList();
       		validateNow();
       		
       		this.dispatchEvent(new Event("habilitarChanged"));
        }
        public function get habilitado ():Boolean {
        	return this._habilitado;
        }
        [Bindable(event="habilitarChanged")]
        public function get habilitar():Boolean{
   		    return this._habilitado;
        }
		public function get campoDTO():String {
			return this._campoDTO = (this._campoDTO == null)? this.id : this._campoDTO;
		}
		public function set campoDTO(value:String):void {
			this._campoDTO = value;
		}    
        public function getParameters():ArrayCollection {
        	var lst:ArrayCollection = new ArrayCollection();
        	
        	if (this.valor != null && this.habilitado) {
        		lst.addItem( new Parameter( this.valor, this.campoDTO ));
        	}
        	return lst;
        }
		                 
        
    }
}
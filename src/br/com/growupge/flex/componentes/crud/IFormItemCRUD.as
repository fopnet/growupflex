package br.com.growupge.flex.componentes.crud
{
    public interface IFormItemCRUD extends IHabilitarCRUD
    {
        function set valor(valor:*):void;
        function get valor():*;
		
		function get campoDTO():String;
		function set campoDTO(value:String):void;
    }
}
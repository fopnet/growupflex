package br.com.growupge.flex.componentes.crud
{
	import br.com.growupge.flex.event.BaseEvent;

	public class ComandosCRUDEvent extends BaseEvent	{
		/* Comandos do CRUD */		    
		public static const CONSULTA:String = 'consulta';
  		public static const NOVA_CONSULTA:String = 'novaConsulta';

		public static const NOVO_ITEM:String = 'adicionarItem';
		public static const EDITAR:String = 'editarItem';
		public static const VISUALIZAR:String = 'visualizarItem';
		public static const VISUALIZAR_HISTORICO:String = 'visualizarHistoricoItem';
		public static const EXCLUIR:String = 'excluirItem';
		public static const SALVAR:String = 'salvarItem';
		public static const COPIAR:String = 'copiarItem';
    	public static const CANCELAR:String = 'cancelar';
    	public static const FECHAR_VISUALIZAR:String = 'fecharVisualizar';

		/* Eventos do Form */
        public static const VALOR_CHANGED:String = "valorChanged";
        public static const FECHAR_MENU:String = "fecharMenu";

    	
    	/*Relacionamento GRUPO*/
    	public static const NOVO_ITEM_RELACIONAMENTO:String = 'crudComandoNovoRelacionamentoEvent';
		public static const EDITAR_RELACIONAMENTO:String = 'crudComandoEditarRelacionamentoEvent';
		public static const VISUALIZAR_RELACIONAMENTO:String = 'crudComandoVisualizarRelacionamentoEvent';
		public static const EXCLUIR_RELACIONAMENTO:String = 'crudComandoExcluirRelacionamentoEvent';
		public static const SALVAR_RELACIONAMENTO:String = 'crudComandoSalvarRelacionamentoEvent';
    	public static const CANCELAR_RELACIONAMENTO:String = 'crudComandoCancelarRelacionamentoEvent';
    	public static const CONSULTA_RELACIONAMENTO:String = 'crudComandoConsultarRelacionamentoEvent';
    	public static const CONSULTA_ORIGEM_RELACIONAMENTO:String = 'crudComandoConsultarOrigemRelacionamentoEvent';
    	public static const MOVER_ITEM_PARA_CIMA_RELACIONAMENTO:String = 'moverItemParaCima';
    	public static const MOVER_ITEM_PARA_BAIXO_RELACIONAMENTO:String = 'moverItemParaBaixo';
    	
    	public static const RESULTADO_RELACIONAMENTO:String = 'crudResultadoRelacionamentoEvent';
    	public static const RESULTADO:String = 'crudResultadoEvent';
    	public static const RESULTADO_PROCURAR:String = 'procurarEvent';
    	public static const EMAIL_ENVIADO:String = 'resultadoEmailEvent';
        
        /*Paginacao*/
    	public static const CONSULTA_PAGINA:String = 'consultaPagina';
        public static const ITEM_SELECIONADO:String = 'itemSelecionado';				        
		

		public var currentCRUDAction:String;
		/*Indice do primeiro item a busca da pagina*/
		public var pagina:int = 0; 
		
		public function ComandosCRUDEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
		
	}
}
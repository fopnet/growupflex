package br.com.growupge.flex.componentes.crud
{
    import br.com.growupge.flex.valueobject.Parameter;
    
    public interface ISelecionarViewHelper
    {
        function consultar(value:*):void;
        function get contextParameter():Parameter ;
        function set contextParameter(value:Parameter):void ;
    }
}
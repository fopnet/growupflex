package br.com.growupge.flex.componentes.crud
{
    import mx.core.IFlexDisplayObject;
    
    public interface ISelecionarView extends IFlexDisplayObject
    {
        function set target(target:IFormItemCRUD):void;
        function get target():IFormItemCRUD;
    }
}
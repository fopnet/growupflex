package br.com.growupge.flex.componentes.crud
{
    import flash.events.Event;
    
    import br.com.growupge.flex.formatters.DataFormatter;
    
    public class DateTextCrud extends TextCRUD
    {
        //public var formatString:String;
        
        public function DateTextCrud() {
            super();
            this.minWidth = 70;
        }
        
        public override function set text(value:String):void {
            var format:DataFormatter = new DataFormatter();
            //format.formatString = this.formatString;
            super.text = (value!='')?format.format(value):value;
        }
        
        public override function set valor(value:*):void {
               if (value is Date || value == null) {
                   this.text = (value != null)? (value as Date).toLocaleString(): '';
               } else {
                   this.text = (campoTarget != null)? (value[campoTarget] as Date).toLocaleString() : valor;
               }
               this._valor = value;
               this.dispatchEvent(new Event(ComandosCRUDEvent.VALOR_CHANGED));
         }
         
         public override function get valor():*{
            return this._valor;
         }
    }
}
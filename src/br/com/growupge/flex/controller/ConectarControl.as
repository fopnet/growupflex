package br.com.growupge.flex.controller 
{
	import com.adobe.cairngorm.control.FrontController;
	
	import br.com.growupge.flex.admin.command.TrocarSenhaCommand;
	import br.com.growupge.flex.command.ConectarCommand;
	import br.com.growupge.flex.command.DesconectarCommand;
	import br.com.growupge.flex.command.ProcurarSessionCommand;
	import br.com.growupge.flex.event.ConectarEvent;
	
	public class ConectarControl extends FrontController {
		public function ConectarControl() {
			addCommand( ConectarEvent.CONECTAR, ConectarCommand );
            addCommand( ConectarEvent.DESCONECTAR, DesconectarCommand );
            addCommand( ConectarEvent.TROCAR_SENHA, TrocarSenhaCommand );
            addCommand( ConectarEvent.PROCURAR_SESSION, ProcurarSessionCommand );
		}
		
	}
}
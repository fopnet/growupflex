package br.com.growupge.flex.controller 
{
	
	import com.adobe.cairngorm.control.FrontController;
	
	import br.com.growupge.flex.admin.command.AdicionarCRUDCommand;
	import br.com.growupge.flex.admin.command.AdicionarCRUDRelacionamentoCommand;
	import br.com.growupge.flex.admin.command.AtualizarCRUDCommand;
	import br.com.growupge.flex.admin.command.AtualizarCRUDRelacionamentoCommand;
	import br.com.growupge.flex.admin.command.BuscarListaDestinoCommand;
	import br.com.growupge.flex.admin.command.BuscarListaOrigemCommand;
	import br.com.growupge.flex.admin.command.BuscarTodasCommand;
	import br.com.growupge.flex.admin.command.BuscarTotalCommand;
	import br.com.growupge.flex.admin.command.ExcluirCRUDCommand;
	import br.com.growupge.flex.admin.command.ExcluirCRUDRelacionamentoCommand;
	import br.com.growupge.flex.command.BaseCommand;
	import br.com.growupge.flex.command.EmailCommand;
	import br.com.growupge.flex.event.EmailEvent;
	import br.com.growupge.flex.event.ExecutarComandoEvent;
	
	public class ExecutarComandoControl extends FrontController {
		public function ExecutarComandoControl() {
            addCommand( ExecutarComandoEvent.BUSCAR_TODAS, BuscarTodasCommand );
            addCommand( ExecutarComandoEvent.BUSCAR_TOTAL, BuscarTotalCommand );
            addCommand( ExecutarComandoEvent.PROCURAR, BaseCommand );
		    addCommand( ExecutarComandoEvent.EVENT_ADICIONAR, AdicionarCRUDCommand );
            addCommand( ExecutarComandoEvent.EVENT_ATUALIZAR, AtualizarCRUDCommand ); 
            addCommand( ExecutarComandoEvent.EVENT_EXCLUIR, ExcluirCRUDCommand );            
            addCommand( ExecutarComandoEvent.EXPORTAR_FATURA, BaseCommand );            
            addCommand( EmailEvent.EMAIL_ENVIANDO, EmailCommand);     
            addCommand( ExecutarComandoEvent.COMANDO_GENERICO, BaseCommand );            
            
          	/* CRUD Relacionamento*/
            addCommand( ExecutarComandoEvent.BUSCAR_LISTA_ORIGEM, BuscarListaOrigemCommand );
            addCommand( ExecutarComandoEvent.BUSCAR_LISTA_DESTINO, BuscarListaDestinoCommand );
            addCommand( ExecutarComandoEvent.ADICIONAR_RELACIONAMENTO, AdicionarCRUDRelacionamentoCommand );
            addCommand( ExecutarComandoEvent.ATUALIZAR_RELACIONAMENTO, AtualizarCRUDRelacionamentoCommand);
            addCommand( ExecutarComandoEvent.EXCLUIR_RELACIONAMENTO, ExcluirCRUDRelacionamentoCommand );
 
 			 /* Nova Conta */
             addCommand( ExecutarComandoEvent.CRIAR_USUARIO, BaseCommand );  
             addCommand( ExecutarComandoEvent.RECRIAR_SENHA, BaseCommand );         
		}
	}
}
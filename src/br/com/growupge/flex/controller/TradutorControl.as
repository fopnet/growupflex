package br.com.growupge.flex.controller 
{
	import br.com.growupge.flex.command.GetTradutorCommand;
	
	import com.adobe.cairngorm.control.FrontController;
	
	public class TradutorControl extends FrontController {
		public function TradutorControl() {
			addCommand( TradutorControl.EVENT_GETTRADUTOR, GetTradutorCommand );
		}
		
		public static const EVENT_GETTRADUTOR : String = "gettradutor";
	}
}
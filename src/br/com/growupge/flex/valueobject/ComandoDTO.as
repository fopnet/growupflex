package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	import mx.collections.ArrayCollection;
	
	[RemoteClass(alias="br.com.growupge.dto.ComandoDTO")]
	[Bindable]
	public class ComandoDTO implements IValueObject {
        public var mapaGCS : String;
        public var permissao : String;
        public var SID : String;
        public var parametrosLista : ArrayCollection;
        //public var _parametros : Array;

        public function getSID() : String {	return this.SID; }
        public function setSID(value:String) :void { this.SID = value;  }
        public function getMapaGCS() : String { return this.mapaGCS; }
        public function setMapaGCS(value:String) :void { this.mapaGCS = value; }
        public function getPermissao() : String { return this.permissao;  }
        public function setPermissao(value:String) :void {	this.permissao = value; }
        
        public function ComandoDTO() {
        	this.parametrosLista = new ArrayCollection();
        }
        
        //public function get_Parametros() : Array { return this._parametros;  }        
        //public function set_Parametros(value:Array) : void { this._parametros = value;  }     
        
	}
}
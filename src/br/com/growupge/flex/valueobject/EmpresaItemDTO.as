package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	[RemoteClass(alias="br.com.growupge.dto.EmpresaItemDTO")]
	[Bindable]
	public class EmpresaItemDTO extends MensagemRetornoDTO implements IValueObject	{
    	
        public var empresa: EmpresaDTO;
        public var cliente: ClienteDTO;

		public function EmpresaItemDTO() :void {
			this.empresa = new EmpresaDTO();
			this.cliente = new ClienteDTO();
		}
	
        public function getEmpresa():EmpresaDTO { return this.empresa; } 
        public function setEmpresa(value:EmpresaDTO): void { this.empresa = value; } 
        public function getCliente():ClienteDTO { return this.cliente; }
        public function setCliente(value :ClienteDTO):void { this.cliente = value; }
    
	}
}
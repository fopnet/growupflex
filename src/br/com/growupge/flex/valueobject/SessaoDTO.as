package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;

	[Bindable]
	[RemoteClass(alias="br.com.growupge.dto.SessaoDTO")]
	
	public class SessaoDTO extends MensagemRetornoDTO implements IValueObject {
		public var sid : String;
        public var data : Date;
        public var host : String;
        public var usuario : UsuarioDTO;
        
        public function getSid():String { return this.sid ; }
        public function setSid(value:String): void { this.sid = value; }
        public function getData():Date { return this.data; }
        public function setData(value:Date):void { this.data = value ; }
        public function getHost():String { return this.host; }
        public function setHost(value:String):void { this.host = value; }
        public function getUsuario():UsuarioDTO { return this.usuario; }
        public function setUsuario(value:UsuarioDTO):void { this.usuario = value; }
 	}
}
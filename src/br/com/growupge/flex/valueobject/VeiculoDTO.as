package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados do proprietário dos veículos */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.VeiculoDTO")]

  public class VeiculoDTO extends MensagemRetornoDTO implements IValueObject
  {
 	public var codigo : String;
    public var renavam : String;
    public var chassi : String;
    public var anoModelo : int;
    public var anoFabric : int;
    public var dataCadastro : Date;
    public var usuarioCadastro : UsuarioDTO;
    public var dataAlteracao : Date;
    public var usuarioAlteracao : UsuarioDTO;

    public var proprietario : ProprietarioDTO;
    public var modelo : ModeloDTO;
	public var combustivel : CombustivelDTO;
	public var marca : MarcaDTO;
	
	/// Novos dados da Versao 2.1
	public var motor:String;
	public var tipo:String; //Automovel, Moto, Caminhao
	public var categoria:String; //Particular, aluguel
	public var cor:String;//Listagem das cores basicas
	public var ultimoAnoLicenciamento:int;//Último licensiamento
	public var ultimaTransacao:Date;//Último transacao
	public var srf:String;//Sistema roubo e furto, recuperacao, liberacao
	public var obs:String;//Obs gerais
	
//	public function get cpfcnpj():String {
//		 return proprietario.cpfcnpj; 
//	}
//	public function set cpfcnpj(value:String):void {
//		 proprietario.cpfcnpj = value; 
//	}
//
//	public function get nomeProprietario():String {
//		 return proprietario.nome; 
//	}
//	public function set nomeProprietario(value:String):void {
//		 proprietario.nome = value; 
//	}
//	
//	public function get proprietario():ProprietarioDTO {
//		if (_proprietario == null) 
//			_proprietario = new ProprietarioDTO();
//		return _proprietario;
//	}
	

	//public function get marca():String { return _marca.nome; }
	//public function set marca(value:String):void { _marca.nome = value; }
	
	public function get isCadastroCompleto():Boolean {
		//TODO completar este metodo
		return (modelo != null);
	}

  }
}

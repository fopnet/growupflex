package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	[RemoteClass(alias="br.com.growupge.dto.UserDTO")]
	[Bindable]
	public class UserDTO extends MensagemRetornoDTO implements IValueObject	{
    	
        public var userID : String;
    
        public var name : String;
    
        public var email : String;
        
        public var tipo : String;
        
        public function getTipo() :String {
        	return this.tipo;
        }
        public function setTipo(value:String) :void {
        	this.tipo = value;
        }
        public function getEmail() :String {
        	return this.email;
        }
        public function setEmail(value:String) :void {
        	this.email = value;
        }
        public function getName() :String {
        	return this.name;
        }
        public function setName(value:String) :void {
        	this.name = value;
        }
        public function getUserID() :String {
        	return this.userID;
        }
        public function setUserID(value:String) :void {
        	this.userID = value;
        }
        
    
	}
}
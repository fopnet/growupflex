package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados da marca*/

  [RemoteClass(alias="br.com.growupge.dto.MarcaDTO")]

  [Bindable]
  public class MarcaDTO extends MensagemRetornoDTO implements IValueObject
  {
    public var codigo : Number;
    public var nome : String;

  }
}

package br.com.growupge.flex.valueobject
{
    import com.adobe.cairngorm.vo.IValueObject;
    
    import flash.utils.describeType;
    import flash.utils.getDefinitionByName;
    
 
  /** Data transfer object para manipular dados de Área de Atividade. */

  public class Parameter implements IValueObject
  {
	  
		 // Variaveis somente para efeito de importar o pacote para o compilador usar. 
		private var unusedProprietario:ProprietarioDTO;
		private var unusedCliente:ClienteDTO;

	    public var atributo : String;
	    
	    public var valor : Object;
	    
	    public var label:String;
	    
	    public function Parameter(valor:Object, att:String = null) {
	    	this.valor = valor;
	    	this.atributo = att;
	    }
        public static function getValor(param:Parameter):Object {
			if (param == null || param.valor == null)
				return null;

			var atributos:Array = param.atributo.split(".");
        	var tmpParam :Parameter = null;
        	
        	if (atributos.length == 1) {
        		if (param.valor.hasOwnProperty(atributos[0]) ){
        			return param.valor[atributos[0]] ;
        		} else  if (atributos[0] == 'this') {
					return param.valor;
				} else { 
					throw new Error('Não foi encontrado atributo com o nome especificado:' + atributos[0]);
					//trace( "Não foi encontrado atributo com o nome especificado:" + atributos[0] );
				}
        	} else {
	       		tmpParam = new Parameter(param.valor[atributos[0]], atributos.slice(1, atributos.length).join("."));
        		return Parameter.getValor(tmpParam);
        	}
        }
        
        public static function setValor(dto:Object, param:Parameter):void {
			
			if (dto != null && param != null) {
	        	var atributos:Array =  param.atributo.split(".");
				var tmpParam:Parameter = null;
	        	
				if ( atributos.length == 1 ) {
					dto[atributos[0]] = param.valor;
				} else {
					// avaliar se pode ainda ter um objeto até chegar ao atributo
					if (atributos.length > 1) {
						// tentativa de chamar o construtor do objeto intermediario que está nulo.
						tmpParam = new Parameter( dto , atributos[0] );
						var obj:* = Parameter.getValor(tmpParam);
						if (obj == null) {
							var aType:String = getType(dto, atributos[0] as String);
							if (aType != null) {
								var nestedObj:Object = createInstance(aType);
								tmpParam = new Parameter(nestedObj, atributos[0] as String );
								Parameter.setValor( dto, tmpParam);
							}
						}
					}
					
					tmpParam = new Parameter( param.valor , atributos.slice(1, atributos.length).join("."));
	        		Parameter.setValor(dto[atributos[0]], tmpParam);
				}
			}
        }
		
		public static function getType(obj:*, attName:String):String { 
			// Get the Button control's E4X XML object description.
			var classInfo:XML = describeType(obj);
			
			// Dump the entire E4X XML object into ta2.
			//trace( classInfo.toString() );
			
			// List the class name.
			//trace( "Class " + classInfo.@name.toString() + "\n" );
			
			// List the object's variables, their values, and their types.
			/*
			for each (var v:XML in classInfo..variable) {
				trace( "Variable " + v.@name + "=" + obj[v.@name] + " (" + v.@type + ")\n" );
				if (v.@name == attName)
					return v.@type;
			}
			*/
			
			// List accessors as properties.
			for each (var a:XML in classInfo..accessor) {
				// Do not get the property value if it is write only.
				if (a.@access == 'writeonly') {
					//trace( "Property " + a.@name + " (" + a.@type +")\n" );
				}
				else {
					//trace( "Property " + a.@name + "=" + obj[a.@name] +  " (" + a.@type +")\n" );
				}
				
				if (a.@name == attName)
					return a.@type;
			} 
			
			/*
			// List the object's methods.
			for each (var m:XML in classInfo..method) {
				ta1.text += "Method " + m.@name + "():" + m.@returnType + "\n";
			}
			*/
			
			return null;
		}
		
		//private var forCompiler:FlagFrance; //REQUIRED! (but otherwise not used)
		public static function createInstance(className:String):Object {
			var myClass:Class = getDefinitionByName(className) as Class;
			var instance:Object = new myClass();
			return instance;
		}
  }
}

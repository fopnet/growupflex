package br.com.growupge.flex.valueobject
{
  import br.com.growupge.flex.constantes.Constantes;
  
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados de
permiss�es. */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.ServicoDTO")]

  public class ServicoDTO extends MensagemRetornoDTO implements IValueObject
  {
    public var codigo : Number;
    public var nome : String;
    public var descricao : String;
    public var isTramitado:String;
	
	public static var PESQUISA_SIMPLES: ServicoDTO = new ServicoDTO(8, "PESQUISA SIMPLES"); 
	
	public function ServicoDTO (id:Number=NaN, nome:String=null) {
		this.codigo = id;
		this.nome = nome;
	}
	
  }
}

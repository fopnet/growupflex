package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	
	import mx.controls.ToolTip;
/* AMF0
 	import mx.messaging.management.ObjectInstance;
	import mx.messaging.management.ObjectName; */
	import mx.utils.ObjectProxy;
	import mx.utils.ObjectUtil;
	import br.com.growupge.flex.valueobject.*;
	import flash.utils.ByteArray;
	
	//[Deprecated('AMF0')]
	public class ConvertDTO implements IValueObject {

		public static function getNewInstance(typedObj:DTOPadrao): Object {
			if (typedObj != null) {
				var typeName:String = describeType(typedObj).@name.toString();
				var ClassReference:Class = getDefinitionByName(typeName) as Class;
        	    var instance:Object = new ClassReference();
            	return instance;
   			} else {
   				return typedObj;
   			}
  		}
  		
  		public static function clone2(rawObj: Object, typedObj :Object ) :Object {
			
			if (rawObj != null && typedObj != null) {
		
				// Get the Button control's E4X XML object description.
	            var classInfo:XML = describeType(typedObj);
	    
	            // Dump the entire E4X XML object into ta2.
	            var info :String = classInfo.toString();
	
	            // List the object's variables, their values, and their types.
	            for each (var a:XML in classInfo..accessor) {
	            	/*
	            	 TODO: Implementar o metodo analogo ao getDeclaredFileds.
	                 Solução: verficando de do declaredType é igual ao tipo da mesma classe.
	                 ou de MensagemRetornoDTO
	                */
	                
	                 // Do not get the property value if it is write only.
			 		if (a.@access != 'writeonly') {
						// List the class name.
			            trace("Class " + classInfo.@name.toString());
			            // List the accessor´s sign
	   					trace('Signing... ' + a.@name);
	   					
	   					if (ConvertDTO.isPrimitive(a.@type)) {
	                		typedObj[a.@name] = rawObj[a.@name];
	   					} else {
	   						var typeName:String = a.@type;
	   						var ClassReference:Class = getDefinitionByName(typeName) as Class;
				            var instance:DTOPadrao = new ClassReference() as DTOPadrao;
				            typedObj[a.@name] = ConvertDTO.clone2(rawObj[a.@name], instance);
	   					}
	   					   					   					
	   				}
	
	            }
		  	} else if ( rawObj == null ) {
		  		return rawObj;
		  	}
            
            return typedObj;
		}
		
		
		private static function isPrimitive(typeName:String) : Boolean {
			switch (typeName) {
				case "String":
				case "Date":
				case "Number":
				case "Boolean":
				case "Object":
				return true;
				default:
				return false;
			}
		}
			
		public static function cloneObject(o:Object):Object{
           var bytes:ByteArray = new ByteArray();
           bytes.writeObject(o);
           bytes.position = 0;
           return bytes.readObject();
        }
        
        
        /* Os metodos abaixo precisam existir
        *  por causa do compilador se não verificar que
        *  existe uma referencia ao objeto clonado, dispara
        *  o erro de variavel não definida.
        * AMF0
         
		public static function toSessaoDTO(obj:Object):SessaoDTO {
			return  ConvertDTO.clone(obj, new SessaoDTO) as SessaoDTO;
		}
		public static function toMensagemDTO(obj:Object):MensagemDTO {
			return  ConvertDTO.clone(obj, new MensagemDTO) as MensagemDTO ;
		}
		public static function toClienteDTO(obj:Object):ClienteDTO {
			return  ConvertDTO.clone(obj, new ClienteDTO) as ClienteDTO ;
		}
		public static function toEmpresaDTO(obj:Object):EmpresaDTO {
			return  ConvertDTO.clone(obj, new EmpresaDTO) as EmpresaDTO ;
		}
		public static function toUsuarioDTO(obj:Object):UsuarioDTO {
			return  ConvertDTO.clone(obj, new UsuarioDTO) as UsuarioDTO;
		}
		public static function toPerfilDTO(obj:Object):PerfilDTO {
			return  ConvertDTO.clone(obj, new PerfilDTO) as PerfilDTO;
		}
		public static function toPermissaoDTO(obj:Object):PermissaoDTO {
			return  ConvertDTO.clone(obj, new PermissaoDTO) as PermissaoDTO;
		}
		public static function toSenhaDTO(obj:Object):SenhaDTO {
			return ConvertDTO.clone(obj, new SenhaDTO) as SenhaDTO;
		}
		public static function toPedidoVeiculoDTO(obj:Object):PedidoVeiculoDTO {
			return ConvertDTO.clone(obj, new PedidoVeiculoDTO) as PedidoVeiculoDTO;
		}
		*/
		
	}
}
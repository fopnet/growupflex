package br.com.growupge.flex.valueobject
{
	public class Abstract
	{
		public function Abstract(access:Private) 
		{
			if ( access == null )
			{
				throw new Error("AbstractException...");
			}
		}
		
		protected static function getAccess():Private
		{
			return new Private();
		}
		
	}
}

// inner class which restricts constructor 
// access to private
final class Private{}


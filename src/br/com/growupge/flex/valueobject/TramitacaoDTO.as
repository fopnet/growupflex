package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados da Tramitacao dos pedidos */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.TramitacaoDTO")]

  public class TramitacaoDTO extends MensagemRetornoDTO implements IValueObject
  {
    public var codigo : Number;
    public var pedido : PedidoVeiculoDTO;
    public var isPendente : String;
    public var obs:String;

    public var dataAlteracao:Date;
    public var usuarioAlteracao:UsuarioDTO;
    public var dataCadastro:Date;
    public var usuarioCadastro:UsuarioDTO;
    
    public var filtroDataFinal:Date;
    public var filtroDataInicial:Date;    

  }
}

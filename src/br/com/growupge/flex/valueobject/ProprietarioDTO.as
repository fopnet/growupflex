package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados do proprietário dos veículos */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.ProprietarioDTO")]

  public class ProprietarioDTO extends MensagemRetornoDTO implements IValueObject
  {
    public var codigo : Number;
    public var nome : String;
    public var cpfcnpj : String;
    public var dataCadastro:Date;
    public var dataAlteracao:Date;
    public var endereco : String;
    public var bairro : String;
    public var cidade : String;
    public var uf : String;
    public var cep : String;
    public var tel : String;
    public var cel : String;
    public var obs : String;
    
    public var filtroDataInicial:Date;
    public var filtroDataFinal:Date;
	

  }
}

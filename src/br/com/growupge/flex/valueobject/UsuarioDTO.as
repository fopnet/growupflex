package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	[RemoteClass(alias="br.com.growupge.dto.UsuarioDTO")]
	[Bindable]
	public class UsuarioDTO extends MensagemRetornoDTO implements IValueObject	{
    	
        public var codigo : String;
        public var habilitado:String;
        public var senha : String;
        public var email : String;
        public var idioma : String;
        public var nome : String;
        public var telefone:String
        public var obs:String
        public var dataUltimoAcesso : Date;
        public var dataUltimaAlteracaoSenha : Date;
        public var dataExpiracaoSenha: Date ;
        public var cliente :ClienteDTO;
        public var empresa: EmpresaDTO;
        public var perfil : PerfilDTO;
        // Preferencias
        public var qtdRegistros:Number;
        
		/*
        public function getCodigo():String { return this.codigo; } 
        public function setCodigo(value:String): void { this.codigo = value; } 
        public function getSenha():String { return this.senha; }
        public function setSenha(value:String):void { this.senha = value; }
        public function getEmail():String { return this.email; }
        public function setEmail(value :String):void { this.email = value; }
        public function getTelefone():String { return this.telefone; }
        public function setTelefone(value :String):void { this.telefone= value; }
        public function getObs():String { return this.obs; }
        public function setObs(value :String):void { this.obs = value; }
        public function getIdioma():String { return this.idioma; }
        public function setIdioma(value :String):void{ this.idioma = value }
        public function getNome():String { return this.nome; }
        public function setNome(value :String):void { this.nome = value; }
        public function getDataUltimoAcesso():Date { return this.dataUltimoAcesso; };
        public function setDataUltimoAcesso(value :Date):void { this.dataUltimoAcesso = value };
        public function getDataExpiracaoSenha():Date { return this.dataExpiracaoSenha; };
        public function setDataExpiracaoSenha(value :Date):void { this.dataExpiracaoSenha = value };
        public function getPerfil():PerfilDTO { return this.perfil; } 
        public function setPerfil(value:PerfilDTO): void { this.perfil = value; } 
        public function getCliente():ClienteDTO { return this.cliente; } 
        public function setCliente(value:ClienteDTO): void { this.cliente = value; } 
        public function getHabilitado():String { return this.habilitado; } 
        public function setHabilitado(value:String): void { this.habilitado = value; } 
        public function getQtdRegistros():int { return this.qtdRegistros; } 
        public function setQtdRegistros(value:int): void { this.qtdRegistros = value; }
		*/
    
	}
}
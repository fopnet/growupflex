package br.com.growupge.flex.valueobject
{

  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados de login do usuario. */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.SenhaDTO")]

  public class SenhaDTO extends MensagemRetornoDTO implements IValueObject
  {
	
	public var codigo : Number;
	public var usuario : UsuarioDTO;
	public var valor : String;

    public function getCodigo():Number { return this.codigo; } 
    public function setCodigo(value:Number): void { this.codigo = value; }
    public function getValor():String { return this.valor; } 
    public function setValor(value:String): void { this.valor = value; } 
    public function getUsuario():UsuarioDTO { return this.usuario; } 
    public function setUsuario(value:UsuarioDTO): void { this.usuario = value; } 
    
  }
}

package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;
  
  import mx.collections.ArrayCollection;

  /** Data Transfer Object para manipular dados do modelo */

  [RemoteClass(alias="br.com.growupge.dto.ModeloDTO")]

  [Bindable]
  public class ModeloDTO extends MensagemRetornoDTO implements IValueObject
  {
    public var codigo : Number;
    public var marca : MarcaDTO;
    public var nome : String;
    public var codigoFipe:String;
    public var tipo:String;
    public var anos:ArrayCollection;

  }
}

package br.com.growupge.flex.valueobject
{
	import br.com.growupge.flex.util.StringUtilExt;
	
	import com.adobe.cairngorm.vo.IValueObject;
	
	import mx.collections.ArrayCollection;
	import mx.formatters.CurrencyFormatter;
	import mx.utils.StringUtil;

	[Bindable]
	[RemoteClass(alias="br.com.growupge.dto.RestricaoDTO")]

	public class RestricaoDTO extends MensagemRetornoDTO implements IValueObject 
	{
		public static const VALOR_UFIR_2013:Number = 2.5473;
		public static const MOEDA_REAL:String = "R$ ";
		public static const MOEDA_UFIR:String = "UFIR ";
		public static const tiposMoedas:ArrayCollection = new ArrayCollection([MOEDA_REAL, MOEDA_UFIR]);
		
		private var myFormatter:CurrencyFormatter =new CurrencyFormatter();
		public var codigo : Number;
		public var nome:String; 
		public var moeda:String; 
		private var _valor:Object;
		public var pedido:PedidoVeiculoDTO;
		
		public function get valor() :Object {
			return _valor;
		}
		public function set valor(valor:Object) :void {
			this._valor = valor == null ? "" : valor;
		}
		
		public function RestricaoDTO(nome:String=null,valor:Object=null) {
			this.nome = nome;
			this.valor = valor;

			myFormatter.currencySymbol = MOEDA_REAL;
			myFormatter.precision=2;
			myFormatter.decimalSeparatorTo = ",";
			myFormatter.thousandsSeparatorTo = ".";
			myFormatter.useThousandsSeparator = true;
		}			
		
		
		public function get getValorReal():Number {
			if ( StringUtilExt.isNumber( String(valor) ) && moeda != null ) {
				switch (moeda) {
					case MOEDA_UFIR:
						return Number(valor) * VALOR_UFIR_2013;
					case MOEDA_REAL:
						return Number(valor);
						break;
				}
			}
			
			return 0;
		}
		
		public function get getValorFormatado():String {
			if (StringUtilExt.isNumber( String(valor) ) && moeda != null) {
				var r:Number = parseFloat(valor.toString());
				var result:String = null;
				
				if(r == 0) {
					result = r.toString();
					
				} else {
					myFormatter.currencySymbol  = this.moeda;
					result = myFormatter.format(r.toString());
				}
				
				if(result.charAt(0) == ",") {
					result = "0" + result;
				}
				return myFormatter.format(valor);
			}
			
			return valor is String ? valor.toString() : null;
		}
		
	}
}
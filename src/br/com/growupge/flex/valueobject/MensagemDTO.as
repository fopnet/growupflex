package br.com.growupge.flex.valueobject
{

	
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular mensagens internacionalizadas. */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.MensagemDTO")]

  public class MensagemDTO extends MensagemRetornoDTO implements IValueObject
  {

    public var codigo : String;
    public var descricaoBR : String;
    public var descricaoEN : String;

    public function getCodigo() :String { return this.codigo; }
    public function setCodigo(value:String) :void { this.codigo = value; }

    public function getDescricaoBR() :String { return this.descricaoBR; }
    public function setDescricaoBR(value:String) :void { this.descricaoBR = value; }

    public function getDescricaoEN() :String { return this.descricaoEN; }
    public function setDescricaoEN(value:String) :void { this.descricaoEN = value; }


  }
}

package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados do proprietário dos veículos */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.CombustivelDTO")]

  public class CombustivelDTO extends MensagemRetornoDTO implements IValueObject
  {
 	public var codigo : String;
    public var descricao:String;
    
    public function CombustivelDTO(codigo:String = null, descricao:String= null) {
    	this.codigo = codigo;
    	this.descricao = descricao;
    }

  }
}

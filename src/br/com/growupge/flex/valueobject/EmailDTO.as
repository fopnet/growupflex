package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	[RemoteClass(alias="br.com.growupge.dto.EmailDTO")]
	[Bindable]
	public class EmailDTO extends DTOPadrao implements IValueObject {
        public var assunto : String;
        public var conteudo : String;
        public var usuario:UsuarioDTO;
        public var empresa :EmpresaDTO;

		public function EmailDTO() {
			this.usuario = new UsuarioDTO();
			this.empresa = new EmpresaDTO();
		}
			
        public function getAssunto() : String {	return this.assunto; }
        public function setAssunto(value:String) :void { this.assunto = value;  }
        public function getConteudo() : String { return this.conteudo; }
        public function setConteudo(value:String) :void { this.conteudo = value; }
        public function getUsuario() : UsuarioDTO { return this.usuario;  }
        public function setUsuario(value:UsuarioDTO) :void {	this.usuario  = value; }
        public function getEmpresa() : EmpresaDTO { return this.empresa;  }
        public function setEmpresa(value:EmpresaDTO) :void { this.empresa  = value; }
        
        
	}
}
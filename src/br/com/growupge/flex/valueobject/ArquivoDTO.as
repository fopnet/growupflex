package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados de
permiss�es. */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.ArquivoDTO")]

  public class ArquivoDTO extends MensagemRetornoDTO implements IValueObject
  {
    public var codigo : String;
    public var nome : String;

  }
}

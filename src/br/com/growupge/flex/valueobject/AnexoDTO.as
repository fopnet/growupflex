package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados dos anexos submetidos */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.AnexoDTO")]

  public class AnexoDTO implements IValueObject
  {
    public var nome : String;
    public var tamanho : uint;
	public var pedido:PedidoVeiculoDTO;
	
	public function AnexoDTO(nome:String= null, tamanho:uint=0, pedido:PedidoVeiculoDTO= null) {
		this.nome = nome;
		this.tamanho = tamanho;
		this.pedido = pedido;
	}

  }
}

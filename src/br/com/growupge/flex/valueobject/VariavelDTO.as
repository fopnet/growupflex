package br.com.growupge.flex.valueobject
{
    import com.adobe.cairngorm.vo.IValueObject;
    
 
  /** Data transfer object para manipular dados
de vari�veis do sistema. */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.VariavelDTO")]

  public class VariavelDTO extends MensagemRetornoDTO implements IValueObject
  {


    public var codigo : String;

    public var descricaoBR : String;

    public var descricaoEN : String;

    public var descricaoES : String;

    public var descricaoOT : String;

    public var valor : String;
    
  }
}

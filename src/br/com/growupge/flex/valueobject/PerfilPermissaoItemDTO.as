package br.com.growupge.flex.valueobject
{

  	
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados da lista de
perfil x permiss�es. */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.PerfilPermissaoItemDTO")]

  public class PerfilPermissaoItemDTO extends MensagemRetornoDTO implements IValueObject
  {


    public var perfil : PerfilDTO;
    public var permissao : PermissaoDTO;

	public function PerfilPermissaoItemDTO() :void {
		this.perfil = new PerfilDTO();
		this.permissao = new PermissaoDTO();
	}
    
    public function getPerfil():PerfilDTO { return this.perfil; } 
    public function setPerfil(value:PerfilDTO): void { this.perfil = value; } 
    public function getPermissao():PermissaoDTO { return this.permissao; } 
    public function setPermissao(value:PermissaoDTO): void { this.permissao = value; } 

  }
}

package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	[RemoteClass(alias="br.com.growupge.dto.EmpresaDTO")]
	[Bindable]
	public class EmpresaDTO extends MensagemRetornoDTO implements IValueObject	{
    	
		public static var GROWUP_VEICULOS: EmpresaDTO = new EmpresaDTO(3, "GROW UP VEÍCULOS S/C LTDA", "veiculos@growupge.com.br"); 
		
		public function EmpresaDTO(codigo:Number=NaN, nome:String=null, email:String= null) {
			this.codigo = codigo;
			this.nome = nome;
			this.email = email;
		}
		
        public var codigo : Number;
        public var nome : String;
        public var cnpj : String;
        public var email: String;
        
        public function getCodigo():Number { return this.codigo; } 
        public function setCodigo(value:Number): void { this.codigo = value; } 
        public function getNome():String { return this.nome; }
        public function setNome(value:String):void { this.nome = value; }
        public function getCnpj():String { return this.cnpj; }
        public function setCnpj(value :String):void { this.cnpj = value; }
        public function getEmail():String { return this.email; }
        public function setEmail(value :String):void { this.email = value; }
 
	}
}
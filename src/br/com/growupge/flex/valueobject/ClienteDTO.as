package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	[RemoteClass(alias="br.com.growupge.dto.ClienteDTO")]
	[Bindable]
	public class ClienteDTO extends MensagemRetornoDTO implements IValueObject	{
    	
        public var codigo : Number;
        public var nome : String;
        public var descricao : String;
        public var arquivo : String;
        
        public function getCodigo():Number { return this.codigo; } 
        public function setCodigo(value:Number): void { this.codigo = value; } 
        public function getNome():String { return this.nome; }
        public function setNome(value:String):void { this.nome = value; }
        public function getDescricao():String { return this.descricao; }
        public function setDescricao(value :String):void { this.descricao= value; }
        public function getArquivo():String { return this.arquivo; }
        public function setArquivo(value :String):void { this.arquivo= value; }
    
	}
}
package br.com.growupge.flex.valueobject
{

  import mx.collections.ArrayCollection;
	
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados de
permiss�es. */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.PermissaoDTO")]

  public class PermissaoDTO extends MensagemRetornoDTO implements IValueObject
  {


    public var codigo : String;
    public var descricao : String;

    public function getCodigo():String { return this.codigo; } 
    public function setCodigo(value:String): void { this.codigo = value; }
    public function getDescricao():String { return this.descricao; } 
    public function setDescricao(value:String): void { this.descricao = value; } 
    
  }
}

package br.com.growupge.flex.valueobject
{
  import com.adobe.cairngorm.vo.IValueObject;

  /** Data Transfer Object para manipular dados do modelo */

  [RemoteClass(alias="br.com.growupge.dto.AnoModeloDTO")]

  [Bindable]
  public class AnoModeloDTO extends MensagemRetornoDTO implements IValueObject
  {
    public var codigo : Number;
	public var ano:Number;
	public var combustivel:String ;
	public var valorFipe:String ;
	public var modelo:ModeloDTO ;

  }
}

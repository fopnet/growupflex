package br.com.growupge.flex.valueobject
{

  import com.adobe.cairngorm.vo.IValueObject;
  
  import flash.utils.IDataInput;
  import flash.utils.IDataOutput;
  import flash.utils.IExternalizable;
  
  import mx.collections.ArrayCollection;
  import br.com.growupge.flex.enums.StatusPedido;

  /** Data Transfer Object para manipular dados do pedido */

  [Bindable]
  [RemoteClass(alias="br.com.growupge.dto.PedidoVeiculoDTO")]

  public class PedidoVeiculoDTO extends MensagemRetornoDTO implements IValueObject /*, IExternalizable */ 
  {

    public var codigo : Number;
	public var usuarioCadastro:UsuarioDTO;
	public var dataCadastro:Date;
	public var usuarioAlteracao:UsuarioDTO;
	public var dataAlteracao:Date;
	public var veiculo:VeiculoDTO = new VeiculoDTO();
	public var status:StatusPedido;
	public var descricao:String;
	public var servico:ServicoDTO;
	public var cliente:ClienteDTO;
	public var nrGaveta:int;
	public var arquivo:ArquivoDTO;
    
	public var filtroDataFinal:Date;
	public var filtroDataInicial:Date;
	
	public var restricoes:ArrayCollection;
	public var anexos:ArrayCollection;

	public var pagamentos:ArrayCollection;
	public var dataPagamento:Date;
	public var valor:Number;
	
	
	public function PedidoVeiculoDTO() {
		//status = StatusPedido.PENDENTE;
	}
	
	public function readExternal(input:IDataInput):void { 
		codigo = input.readDouble();
		usuarioCadastro = input.readObject() as UsuarioDTO;
		dataCadastro = input.readObject() as Date;
		usuarioAlteracao =  input.readObject() as UsuarioDTO;
		dataAlteracao = input.readObject() as Date;
		veiculo = input.readObject() as VeiculoDTO;
		status = input.readObject() as StatusPedido;
		descricao = input.readObject() as String;
		servico =  input.readObject() as ServicoDTO;
		cliente = input.readObject() as ClienteDTO;
		nrGaveta = input.readInt();
		arquivo = input.readObject() as ArquivoDTO;
		filtroDataInicial = input.readObject() as Date;
		filtroDataFinal = input.readObject() as Date;
		
/*		restricoes =  input.readObject() as ArrayCollection;
		anexos = input.readObject() as ArrayCollection;
		pagamentos = input.readObject() as ArrayCollection;
*/		
		dataPagamento = input.readObject() as Date;
		//valor = input.readDouble() as Number; 
	} 
	
	public function writeExternal(output:IDataOutput):void { 
		output.writeDouble( codigo );
		output.writeObject( usuarioCadastro );
		output.writeObject( dataCadastro );
		output.writeObject( usuarioAlteracao );
		output.writeObject( dataAlteracao );
		output.writeObject( veiculo );
		output.writeObject( status );
		output.writeObject( descricao );
		output.writeObject( servico );
		output.writeObject( cliente );
		output.writeInt( nrGaveta );
		output.writeObject( arquivo );
		output.writeObject( filtroDataInicial );
		output.writeObject( filtroDataFinal );
		
/*		output.writeObject( restricoes );
		output.writeObject( anexos);
		
		output.writeObject( pagamentos);
*/		output.writeObject( dataPagamento );
		//output.writeDouble( valor ); 
	} 

	
	public function get placa():String { return veiculo.codigo; }
	public function set placa(value:String):void { veiculo.codigo = value; }
	public function get renavam():String { return veiculo.renavam; }
	public function set renavam(value:String):void { veiculo.renavam = value; }
	public function get chassi():String { return veiculo.chassi; }
	public function set chassi(value:String):void { veiculo.chassi = value; }
	
	public function get marca():String { return veiculo.marca ? veiculo.marca.nome : null; }
	//chamado pelo tela de varejo
	public function set marca(value:String):void { 
		if (!veiculo.marca)
			veiculo.marca = new MarcaDTO();
		
		veiculo.marca.nome = value;  
	}
    
  }
}

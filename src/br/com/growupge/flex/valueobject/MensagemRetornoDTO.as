package br.com.growupge.flex.valueobject
{
	import com.adobe.cairngorm.vo.IValueObject;
	
	[RemoteClass(alias="br.com.growupge.dto.MensagemRetornoDTO")]
	[Bindable]
	public class MensagemRetornoDTO  extends DTOPadrao implements IValueObject {
        public var codMensagem:String;
    	public var mensagemRetorno:String;
    	public var parametros : Object;
    	
    	public function getCodMensagem() :String { return this.codMensagem ;}
    	public function setCodMensagem(value:String):void { this.codMensagem = value ;}
    	public function getMensagemRetorno() :String { return this.mensagemRetorno;}
    	public function setMensagemRetorno(value:String):void { this.mensagemRetorno = value ;}
    	public function getParametros() :Object { return this.parametros ;}
    	public function setParametros(value:Object):void { this.parametros = value ;}
	}
}